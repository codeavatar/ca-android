package com.yuntoyun.fwcore.model;
/*
 * +----------------------------------------------------------------------
 * | @Author: codeavatar   @Year：2021
 * +----------------------------------------------------------------------
 * | @Email: codeavatar@aliyun.com
 * +----------------------------------------------------------------------
 */
import java.io.Serializable;

import androidx.annotation.NonNull;

/**
 * WebView活动数据型数
 */
public class FwWebViewModel implements Serializable {

    private String title;
    private String url;
    private String htmlcode;
    private WebType type;


    public FwWebViewModel() {
    }

    public FwWebViewModel(String title, String data, @NonNull WebType type) {
        this.title = title;
        switch (type) {
            case Url:
                this.url = data;
                break;
            case Html:
                this.htmlcode = data;
                break;
        }
    }


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getHtmlcode() {
        return htmlcode;
    }

    public void setHtmlcode(String htmlcode) {
        this.htmlcode = htmlcode;
    }

    public WebType getType() {
        if (null == type) {
            return WebType.Url;
        }
        return type;
    }

    public void setType(WebType type) {
        this.type = type;
    }

    //显示类型
    public enum WebType {
        Url, Html
    }
}
