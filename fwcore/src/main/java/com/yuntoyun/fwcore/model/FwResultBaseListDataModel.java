package com.yuntoyun.fwcore.model;
/*
 * +----------------------------------------------------------------------
 * | @Author: codeavatar   @Year：2021
 * +----------------------------------------------------------------------
 * | @Email: codeavatar@aliyun.com
 * +----------------------------------------------------------------------
 */

/**
 * data + list 数据模型 基类
 */
public class FwResultBaseListDataModel<T, D> {

    public int getFwStatus() {
        return fwStatus;
    }

    public void setFwStatus(int fwStatus) {
        this.fwStatus = fwStatus;
    }

    public String getFwMessage() {
        return fwMessage;
    }

    public void setFwMessage(String fwMessage) {
        this.fwMessage = fwMessage;
    }

    public D getFwData() {
        return fwData;
    }

    public void setFwData(D fwData) {
        this.fwData = fwData;
    }

    public T getFwList() {
        return fwList;
    }

    public void setFwList(T fwList) {
        this.fwList = fwList;
    }

    public int getFwCurrentPage() {
        return fwCurrentPage;
    }

    public void setFwCurrentPage(int fwCurrentPage) {
        this.fwCurrentPage = fwCurrentPage;
    }

    public int getFwTotalPage() {
        return fwTotalPage;
    }

    public void setFwTotalPage(int fwTotalPage) {
        this.fwTotalPage = fwTotalPage;
    }

    public int getFwTotalCount() {
        return fwTotalCount;
    }

    public void setFwTotalCount(int fwTotalCount) {
        this.fwTotalCount = fwTotalCount;
    }

    private int fwStatus = -1;
    private String fwMessage = "";
    private D fwData;
    private T fwList;
    private int fwCurrentPage;
    private int fwTotalPage;
    private int fwTotalCount;
}
