package com.yuntoyun.fwcore.model;
/*
 * +----------------------------------------------------------------------
 * | @Author: codeavatar   @Year：2021
 * +----------------------------------------------------------------------
 * | @Email: codeavatar@aliyun.com
 * +----------------------------------------------------------------------
 */
import org.json.JSONArray;

/**
 * FwBaseSpinnerAdapter 数据模型
 */
public class FwSpinnerAdapterModel {

    private int valueId;
    private String name, value;
    private JSONArray childArr;

    public JSONArray getChildArr() {
        return childArr;
    }

    public void setChildArr(JSONArray childArr) {
        this.childArr = childArr;
    }

    public int getValueId() {
        return valueId;
    }

    public void setValueId(int valueId) {
        this.valueId = valueId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public FwSpinnerAdapterModel() {

    }

    public FwSpinnerAdapterModel(String name, int valueId) {
        this.name = name;
        this.valueId = valueId;
    }

    public FwSpinnerAdapterModel(String name, String value) {
        this.name = name;
        this.value = value;
    }

    public FwSpinnerAdapterModel(String name, String value, JSONArray childArr) {
        this.name = name;
        this.value = value;
        this.childArr = childArr;
    }
}
