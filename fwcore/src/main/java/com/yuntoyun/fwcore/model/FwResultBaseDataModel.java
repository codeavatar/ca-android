package com.yuntoyun.fwcore.model;
/*
 * +----------------------------------------------------------------------
 * | @Author: codeavatar   @Year：2021
 * +----------------------------------------------------------------------
 * | @Email: codeavatar@aliyun.com
 * +----------------------------------------------------------------------
 */

/**
 * data 数据模型 基类
 */
public class FwResultBaseDataModel<T> {

    private int fwStatus = -1;
    private String fwMessage = "";
    private T fwData;

    public int getFwStatus() {
        return fwStatus;
    }

    public void setFwStatus(int fwStatus) {
        this.fwStatus = fwStatus;
    }

    public String getFwMessage() {
        return fwMessage;
    }

    public void setFwMessage(String fwMessage) {
        this.fwMessage = fwMessage;
    }

    public T getFwData() {
        return fwData;
    }

    public void setFwData(T fwData) {
        this.fwData = fwData;
    }

}
