package com.yuntoyun.fwcore.model;
/*
 * +----------------------------------------------------------------------
 * | @Author: codeavatar   @Year：2021
 * +----------------------------------------------------------------------
 * | @Email: codeavatar@aliyun.com
 * +----------------------------------------------------------------------
 */
import android.view.View;

import com.yuntoyun.fwcore.adapter.FwBaseRecyclerViewAdapter;

/**
 * RecyclerView组装Adapter必要数据模型
 * （继承 FwBaseRecyclerViewActivity 或 FwBaseRecyclerViewFragment 后推荐使用）
 */
public class FwRecyclerViewAdapterModel {

    private int itemResId;
    private int emptyResId;
    private View emptyView;
    private FwBaseRecyclerViewAdapter.IFwViewHolder viewHolder;

    public FwRecyclerViewAdapterModel() {
    }

    public FwRecyclerViewAdapterModel(int itemResId, int emptyResId, FwBaseRecyclerViewAdapter
            .IFwViewHolder viewHolder) {
        this.itemResId = itemResId;
        this.emptyResId = emptyResId;
        this.viewHolder = viewHolder;
    }

    public FwRecyclerViewAdapterModel(int itemResId, View emptyView, FwBaseRecyclerViewAdapter
            .IFwViewHolder viewHolder) {
        this.itemResId = itemResId;
        this.emptyView = emptyView;
        this.viewHolder = viewHolder;
    }

    public int getItemResId() {
        return itemResId;
    }

    public void setItemResId(int itemResId) {
        this.itemResId = itemResId;
    }

    public int getEmptyResId() {
        return emptyResId;
    }

    public void setEmptyResId(int emptyResId) {
        this.emptyResId = emptyResId;
    }

    public View getEmptyView() {
        return emptyView;
    }

    public void setEmptyView(View emptyView) {
        this.emptyView = emptyView;
    }

    public FwBaseRecyclerViewAdapter.IFwViewHolder getViewHolder() {
        return viewHolder;
    }

    public void setViewHolder(FwBaseRecyclerViewAdapter.IFwViewHolder viewHolder) {
        this.viewHolder = viewHolder;
    }
}
