package com.yuntoyun.fwcore.model;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

/**
 * data + list 数据模型
 */
public class FwResultJsonListDataModel extends FwResultBaseListDataModel<JsonArray, JsonObject> {
    private int index;


    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }
}
