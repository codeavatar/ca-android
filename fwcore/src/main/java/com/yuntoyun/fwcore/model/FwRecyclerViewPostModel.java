package com.yuntoyun.fwcore.model;

import java.util.HashMap;

import io.reactivex.Observable;

/**
 * RecyclerView组装Post必要数据模型
 * （继承 FwBaseRecyclerViewActivity 或 FwBaseRecyclerViewFragment 后推荐使用）
 */
public class FwRecyclerViewPostModel {

    private HashMap<String, Object> map;
    private Observable<FwResultJsonListDataModel> observable;
    private boolean isShowLoading;

    public FwRecyclerViewPostModel() {
    }

    public FwRecyclerViewPostModel(HashMap<String, Object> map,
                                   Observable<FwResultJsonListDataModel> observable) {
        this.map = map;
        this.observable = observable;
    }

    public FwRecyclerViewPostModel(HashMap<String, Object> map,
                                   Observable<FwResultJsonListDataModel> observable,
                                   boolean isShowLoading) {
        this.map = map;
        this.observable = observable;
        this.isShowLoading = isShowLoading;
    }

    public HashMap<String, Object> getMap() {
        return map;
    }

    public void setMap(HashMap<String, Object> map) {
        this.map = map;
    }

    public Observable<FwResultJsonListDataModel> getObservable() {
        return observable;
    }

    public void setObservable(Observable<FwResultJsonListDataModel> observable) {
        this.observable = observable;
    }

    public boolean isShowLoading() {
        return isShowLoading;
    }

    public void setShowLoading(boolean showLoading) {
        isShowLoading = showLoading;
    }
}
