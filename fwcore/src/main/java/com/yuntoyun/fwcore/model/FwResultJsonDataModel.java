package com.yuntoyun.fwcore.model;
/*
 * +----------------------------------------------------------------------
 * | @Author: codeavatar   @Year：2021
 * +----------------------------------------------------------------------
 * | @Email: codeavatar@aliyun.com
 * +----------------------------------------------------------------------
 */
import com.google.gson.JsonObject;

/**
 * data数据模型
 */
public class FwResultJsonDataModel extends FwResultBaseDataModel<JsonObject> {

    private int index;


    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }
}
