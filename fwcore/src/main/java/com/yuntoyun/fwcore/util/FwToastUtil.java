package com.yuntoyun.fwcore.util;
/*
 * +----------------------------------------------------------------------
 * | @Author: codeavatar   @Year：2021
 * +----------------------------------------------------------------------
 * | @Email: codeavatar@aliyun.com
 * +----------------------------------------------------------------------
 */
import android.content.Context;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.yuntoyun.fwcore.R;

/**
 * 消息提示工具类
 */
public class FwToastUtil {

    private static FwToastUtil init = null;
    private Toast toast = null;

    public static synchronized FwToastUtil builder() {
        if (null == init) {
            init = new FwToastUtil();
        }
        return init;
    }

    /**
     * 默认系统消息提示
     *
     * @param message
     */
    public void makeText(String message) {
        if (null != toast) {
            toast.cancel(); //取消未释放的提示
        }
        toast = Toast.makeText(FwApplicationUtil.appContext(), message, Toast
                .LENGTH_SHORT);
        toast.show();
    }

    /**
     * 默认系统消息提示
     *
     * @param message
     * @param duration 设置显示时长
     */
    public void makeText(String message, int duration) {
        if (null != toast) {
            toast.cancel(); //取消未释放的提示
        }
        toast = Toast.makeText(FwApplicationUtil.appContext(), message, duration);
        toast.show();
    }

    /**
     * 消息提示
     *
     * @param message
     * @param msgColor   默认-1不设置
     * @param msgBgColor 默认-1不设置
     */
    public void makeText(String message, int msgColor, int msgBgColor) {
        if (null != toast) {
            toast.cancel(); //取消未释放的提示
        }
        toast = Toast.makeText(FwApplicationUtil.appContext(), message, Toast.LENGTH_SHORT);
        View toastView = toast.getView();
        if (-1 != msgColor) {
            TextView tvw = toastView.findViewById(android.R.id.message);
            tvw.setTextColor(msgColor);
        }
        if (-1 != msgBgColor) {
            Drawable background = toastView.getBackground();
            if (background != null) {
                background.setColorFilter(
                        new PorterDuffColorFilter(msgBgColor, PorterDuff.Mode.SRC_IN)
                );
            } else {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    toastView.setBackground(new ColorDrawable(msgBgColor));
                } else {
                    toastView.setBackgroundDrawable(new ColorDrawable(msgBgColor));
                }
            }
        }
        toast.show();
    }

    /**
     * 可设置提示显示位置
     *
     * @param message
     * @param gravity 显示位置
     */
    public void makeText(String message, ToastGravity gravity) {
        if (null != toast) {
            toast.cancel(); //取消未释放的提示
        }
        toast = Toast.makeText(FwApplicationUtil.appContext(), message, Toast.LENGTH_SHORT);
        switch (gravity) {
            case CENTER:
                toast.setGravity(Gravity.CENTER, 0, 0);
                break;
            case TOP:
                toast.setGravity(Gravity.TOP, 0, 0);
                break;
            case BOTTOM:
            default:
                toast.setGravity(Gravity.BOTTOM, 0, 0);
                break;
        }
        toast.show();
    }

    /**
     * 自定义消息提示
     *
     * @param message
     * @param icon    默认-1不设置
     */
    public void makeCustomView(Context context, String message, int icon) {
        if (null != toast) {
            toast.cancel(); //取消未释放的提示
        }

//        Context context = FwApplicationUtil.appContext();

        View toastView = LayoutInflater.from(context).inflate(R.layout
                .fw_util_toast, null);
        TextView tvw_text = toastView.findViewById(R.id.tvw_text);
        if (-1 != icon) {
            ImageView ivw_icon = toastView.findViewById(R.id.ivw_icon);
            ivw_icon.setImageResource(icon);
            ivw_icon.setVisibility(View.VISIBLE);
        }
        tvw_text.setText(message);
        toast = new Toast(context);
        toast.setView(toastView);
        toast.setDuration(Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.show();
    }

    /**
     * 自定义消息提示
     *
     * @param view
     */
    public void makeCustomView(View view) {
        if (null != toast) {
            toast.cancel(); //取消未释放的提示
        }

        Context context = FwApplicationUtil.appContext();
        toast = new Toast(context);
        toast.setView(view);
        toast.setDuration(Toast.LENGTH_SHORT);
        toast.show();
    }

    /**
     * 显示多行消息
     *
     * @param str
     */
    public void makeTextMultiRow(String ... str) {
        if (null != toast) {
            toast.cancel(); //取消未释放的提示
        }
        StringBuilder sb = new StringBuilder("");
        for (String item : str) {
            sb.append(item).append("\n");
        }
        if (sb.length() > 0) {
            sb.deleteCharAt(sb.length() - 1);
        }
        toast = Toast.makeText(FwApplicationUtil.appContext(), sb.toString(), Toast
                .LENGTH_LONG);
        toast.show();
    }

    /**
     * Toast位置枚举
     */
    public enum ToastGravity {
        CENTER, BOTTOM, TOP
    }
}

