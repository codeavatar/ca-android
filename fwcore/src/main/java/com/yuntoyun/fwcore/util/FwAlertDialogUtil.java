package com.yuntoyun.fwcore.util;
/*
 * +----------------------------------------------------------------------
 * | @Author: codeavatar   @Year：2021
 * +----------------------------------------------------------------------
 * | @Email: codeavatar@aliyun.com
 * +----------------------------------------------------------------------
 */
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;

import com.yuntoyun.fwcore.R;

/**
 * 对话框工具类
 */
public class FwAlertDialogUtil {

    private String mNegativeTextColor = "#999999";
    private String mPositiveTextColor = "#EF5DB0";
    private String mNeutralTextColor = "#555555";

    private DialogType mDialogType = DialogType.Center;

    private static FwAlertDialogUtil init = null;

    public static synchronized FwAlertDialogUtil builder() {
        if (null == init) {
            init = new FwAlertDialogUtil();
        }
        return init;
    }

    /**
     * 设置弹框样式
     *
     * @param dialogType
     * @return
     */
    public FwAlertDialogUtil setDialogType(DialogType dialogType) {
        this.mDialogType = dialogType;
        return init;
    }

    /**
     * 设置确认按扭字体颜色
     *
     * @param txtColor
     * @return
     */
    public FwAlertDialogUtil setPositiveTextColor(String txtColor) {
        this.mPositiveTextColor = txtColor;
        return init;
    }

    /**
     * 设置取消按扭字体颜色
     *
     * @param txtColor
     * @return
     */
    public FwAlertDialogUtil setNegativeTextColor(String txtColor) {
        this.mNegativeTextColor = txtColor;
        return init;
    }

    /**
     * 设置中立按扭字体颜色
     *
     * @param txtColor
     * @return
     */
    public FwAlertDialogUtil setNeutralTextColor(String txtColor) {
        this.mNeutralTextColor = txtColor;
        return init;
    }

    public void showCustomDialog(Context context, String title, String message) {
        this.showCustomDialog(context, title, message, "确认", null);
    }

    public void showCustomDialog(Context context, String title, String message, String
            positiveText, final CustomOkClickListener clickListener) {
        View dialogView = null;
        if (DialogType.Center == mDialogType) {
            dialogView = LayoutInflater.from(context).inflate(R.layout.fw_util_alertdialog_ok2,
                    null);
        } else {
            dialogView = LayoutInflater.from(context).inflate(R.layout.fw_util_alertdialog_ok,
                    null);
        }
        final AlertDialog alertDialog = getCustomAlertDialog(context, dialogView);
        ((TextView) dialogView.findViewById(R.id.tvw_title)).setText(title);
        ((TextView) dialogView.findViewById(R.id.tvw_message)).setText(message);
        TextView tvw_submit = (TextView) dialogView.findViewById(R.id.tvw_submit);
        tvw_submit.setText(positiveText);
        tvw_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();

                if (null != clickListener)
                    clickListener.clickPositiveButton(alertDialog);
            }
        });
        tvw_submit.setTextColor(Color.parseColor(this.mPositiveTextColor));
        alertDialog.show();
    }

    public void showCustomConfirmDialog(Context context, String title, String message, final
    CustomClickListener customClickListener) {
        this.showCustomConfirmDialog(context, title, message, "取消", "确认", customClickListener);
    }

    public void showCustomConfirmDialog(Context context, String title, String message, String
            negativeText, String positiveText, final CustomClickListener customClickListener) {

        View dialogView = null;
        if (DialogType.Center == mDialogType) {
            dialogView = LayoutInflater.from(context).inflate(R.layout.fw_util_alertdialog_confirm2,
                    null);
        } else {
            dialogView = LayoutInflater.from(context).inflate(R.layout.fw_util_alertdialog_confirm,
                    null);
        }
        final AlertDialog alertDialog = getCustomAlertDialog(context, dialogView);
        ((TextView) dialogView.findViewById(R.id.tvw_title)).setText(title);
        ((TextView) dialogView.findViewById(R.id.tvw_message)).setText(message);
        TextView tvw_cancel = (TextView) dialogView.findViewById(R.id.tvw_cancel);
        tvw_cancel.setText(negativeText);
        tvw_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                if (null != customClickListener)
                    customClickListener.clickNegativeButton(alertDialog);
            }
        });
        tvw_cancel.setTextColor(Color.parseColor(this.mNegativeTextColor));

        TextView tvw_submit = (TextView) dialogView.findViewById(R.id.tvw_submit);
        tvw_submit.setText(positiveText);
        tvw_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                if (null != customClickListener)
                    customClickListener.clickPositiveButton(alertDialog);
            }
        });
        tvw_submit.setTextColor(Color.parseColor(this.mPositiveTextColor));
        alertDialog.show();
    }

    public AlertDialog getCustomAlertDialog(Context context, View dialogView) {
        return getCustomAlertDialog(context, dialogView, false, false);
    }

    public AlertDialog getCustomAlertDialog(Context context, View dialogView, boolean isTouchOutside) {
        return getCustomAlertDialog(context, dialogView, false, isTouchOutside);
    }

    public AlertDialog getCustomAlertDialog(Context context, View dialogView, boolean isCancelable, boolean isTouchOutside) {
//        AlertDialog alertDialog = new AlertDialog.Builder(context).create();
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.getContext().setTheme(R.style.fw_dialog_custom);
        AlertDialog alertDialog = builder.create();

        //清除AlertDialog白色背景
        alertDialog.getWindow().setBackgroundDrawable(new BitmapDrawable());
        alertDialog.setView(dialogView);
        //dialog弹出后会点击屏幕或物理返回键，dialog不消失
        alertDialog.setCancelable(isCancelable);
        //dialog弹出后会点击屏幕，dialog不消失；点击物理返回键dialog消失
        alertDialog.setCanceledOnTouchOutside(isTouchOutside);
        return alertDialog;
    }

    /**
     * 按扭点击事件
     */
    public interface CustomOkClickListener {
        void clickPositiveButton(DialogInterface dialog);
    }

    public interface CustomClickListener {
        void clickPositiveButton(DialogInterface dialog);

        void clickNegativeButton(DialogInterface dialog);

//        void clcikNeutralButton(DialogInterface dialog);
    }

    private enum DialogType {
        Default, Center
    }

    //++++++++++++++++++++++++++++++++++++++++++
    // 框架对话框
    //++++++++++++++++++++++++++++++++++++++++++


    //++++++++++++++++++++++++++++++++++++++++++
    // 系统原生
    //++++++++++++++++++++++++++++++++++++++++++

    /**
     * 确认按扭对话框
     *
     * @param context
     * @param title
     * @param message
     */
    public void showAlertDialog(Context context, String title, String message) {
        this.showAlertDialog(context, title, message, new
                int[]{DialogInterface.BUTTON_POSITIVE}, new String[]{"确认"}, new DialogInterface
                .OnClickListener[]{
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }
        });
    }

    /**
     * 确认对话框
     *
     * @param context
     * @param title
     * @param message
     * @param clickListener
     */
    public void showAlertDialogConfirm(Context context, String title, String message,
                                       DialogInterface.OnClickListener clickListener) {
        this.showAlertDialog(context, title, message,
                new int[]{
                        DialogInterface.BUTTON_NEGATIVE,
                        DialogInterface.BUTTON_POSITIVE},
                new String[]{"取消", "确认"}, new DialogInterface.OnClickListener[]{
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        }, clickListener
                });
    }

    /**
     * 多按扭对话框
     *
     * @param context
     * @param title
     * @param message
     * @param buttons
     * @param buttonTexts
     * @param clickListener
     */
    public void showAlertDialog(Context context, String title, String message,
                                int buttons[], String buttonTexts[],
                                DialogInterface.OnClickListener clickListener[]) {
        AlertDialog alertDialog = new AlertDialog.Builder(context)
                .setCancelable(false)
                .setTitle(title)
                .setMessage(message)
                .create();

        //setCancelable 点击屏幕或物理返回键，dialog不消失
        //setCanceledOnTouchOutside 点击屏幕dialog不消失；点击物理返回键dialog消失
//        alertDialog.setCanceledOnTouchOutside(false);

        for (int i = 0; i < buttons.length; i++) {
            alertDialog.setButton(buttons[i], buttonTexts[i], clickListener[i]);
        }
        alertDialog.show();

        this.setAlertButtonTextColor(alertDialog);
    }

    /**
     * 显示单选列表
     *
     * @param context
     * @param title
     * @param options
     * @param clickListener
     */
    public void showAlertDialogRadioBox(Context context, String title, String options[],
                                        DialogInterface.OnClickListener clickListener) {

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.getContext().setTheme(R.style.fw_dialog_custom);
        AlertDialog alertDialog = builder
//                .setCancelable(false)
                .setTitle(title)
                .setIcon(android.R.drawable.ic_dialog_info)
                .setSingleChoiceItems(options, 0, clickListener)
                .setNegativeButton("确认", null)
                .create();

        //dialog弹出后会点击屏幕或物理返回键，dialog不消失
        alertDialog.setCancelable(false);
//        //dialog弹出后会点击屏幕，dialog不消失；点击物理返回键dialog消失
//        alertDialog.setCanceledOnTouchOutside(true);
        alertDialog.show();

        this.setAlertButtonTextColor(alertDialog);
    }

    /**
     * 显示多选列表
     *
     * @param context
     * @param title
     * @param options
     * @param checkedItems
     * @param choiceClickListener
     * @param clickListener
     */
    public void showAlertDialogCheckBox(Context context, String title, String options[],
                                        boolean[] checkedItems,
                                        DialogInterface.OnMultiChoiceClickListener
                                                choiceClickListener, DialogInterface
                                                .OnClickListener clickListener) {
        //setMultiChoiceItems有三种方法，可以自由扩展
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.getContext().setTheme(R.style.fw_dialog_custom);
        AlertDialog alertDialog = builder
                .setTitle(title)
                .setIcon(android.R.drawable.ic_dialog_info)
                .setMultiChoiceItems(options, checkedItems, choiceClickListener)
                .setNegativeButton("取消", null)
                .setPositiveButton("确认", clickListener)
                .create();

        //dialog弹出后会点击屏幕或物理返回键，dialog不消失
        alertDialog.setCancelable(false);
        //dialog弹出后会点击屏幕，dialog不消失；点击物理返回键dialog消失
//        alertDialog.setCanceledOnTouchOutside(true);
        alertDialog.show();
        this.setAlertButtonTextColor(alertDialog);
    }

    /**
     * 显示列表
     *
     * @param context
     * @param title
     * @param options
     * @param clickListener
     */
    public void showAlertDialogList(Context context, String title, String options[],
                                    DialogInterface.OnClickListener clickListener) {
        AlertDialog alertDialog = new AlertDialog.Builder(context)
                .setTitle(title)
                .setIcon(android.R.drawable.ic_dialog_info)
                .setItems(options, clickListener)
                .setNegativeButton("取消", null)
                .create();
        //setItems有二种方法，可以自由扩展

//        //dialog弹出后会点击屏幕或物理返回键，dialog不消失
//        alertDialog.setCancelable(false);
//        //dialog弹出后会点击屏幕，dialog不消失；点击物理返回键dialog消失
//        alertDialog.setCanceledOnTouchOutside(true);
        alertDialog.show();

        this.setAlertButtonTextColor(alertDialog);
    }

    /**
     * 显示图文提示
     *
     * @param context
     * @param title
     * @param message
     * @param imgRid
     * @param clickListener
     */
    public void showAlertDialogImageText(Context context, String title, String message, int
            imgRid, DialogInterface.OnClickListener clickListener) {
        ImageView imageView = new ImageView(context);
        imageView.setImageResource((imgRid == 0) ? android.R.drawable.ic_dialog_info : imgRid);
        AlertDialog alertDialog = new AlertDialog.Builder(context)
                .setCancelable(false)
                .setTitle(title)
                .setView(imageView)
                .setMessage(message)
                .setIcon(android.R.drawable.ic_dialog_info)
                .setPositiveButton("确认", clickListener)
                .create();

//        alertDialog.setCancelable(false);
//        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.show();

        this.setAlertButtonTextColor(alertDialog);
    }

    /**
     * 设置按扭颜色
     *
     * @param alertDialog
     */
    private void setAlertButtonTextColor(AlertDialog alertDialog) {
        Button btnPositive = alertDialog.getButton(AlertDialog.BUTTON_POSITIVE);
        if (null != btnPositive) {
            btnPositive.setTextColor(Color.parseColor(mPositiveTextColor));
        }
        Button btnNegative = alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE);
        if (null != btnNegative) {
            btnNegative.setTextColor(Color.parseColor(mNegativeTextColor));
        }
        Button btnNeutral = alertDialog.getButton(AlertDialog.BUTTON_NEUTRAL);
        if (null != btnNeutral) {
            btnNeutral.setTextColor(Color.parseColor(mNeutralTextColor));
        }
    }
}
