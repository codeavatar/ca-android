package com.yuntoyun.fwcore.util.constant;
/*
 * +----------------------------------------------------------------------
 * | @Author: codeavatar   @Year：2021
 * +----------------------------------------------------------------------
 * | @Email: codeavatar@aliyun.com
 * +----------------------------------------------------------------------
 */
/**
 * 缓存单位常量
 */
public interface FwCacheConstants {
    int SEC  = 1;
    int MIN  = 60;
    int HOUR = 3600;
    int DAY  = 86400;
}
