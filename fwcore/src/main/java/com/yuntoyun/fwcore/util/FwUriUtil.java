package com.yuntoyun.fwcore.util;
/*
 * +----------------------------------------------------------------------
 * | @Author: codeavatar   @Year：2021
 * +----------------------------------------------------------------------
 * | @Email: codeavatar@aliyun.com
 * +----------------------------------------------------------------------
 */
import android.net.Uri;
import android.os.Build;

import java.io.File;

import androidx.core.content.FileProvider;

/**
 * 资源地址工具类
 */
public class FwUriUtil {

    private static FwUriUtil init = null;

    public static synchronized FwUriUtil builder() {
        if (null == init) {
            init = new FwUriUtil();
        }
        return init;
    }

    /**
     * 获取文件的地址
     *
     * @param file
     * @return
     */
    public Uri getFileUri(File file) {
        if (null == file) return null;

        Uri data = null;
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.N) {
            data = Uri.fromFile(file);
        } else {
            String authority = FwApplicationUtil.builder().getAppPackageName() + ".afcore" +
                    ".fileProvider";
            data = FileProvider.getUriForFile(FwApplicationUtil.appContext(), authority, file);
        }
        return data;
    }
}
