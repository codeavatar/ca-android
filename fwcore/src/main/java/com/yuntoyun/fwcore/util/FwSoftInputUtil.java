package com.yuntoyun.fwcore.util;
/*
 * +----------------------------------------------------------------------
 * | @Author: codeavatar   @Year：2021
 * +----------------------------------------------------------------------
 * | @Email: codeavatar@aliyun.com
 * +----------------------------------------------------------------------
 */
import android.content.Context;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;

/**
 * 软键盘工具类
 */
public class FwSoftInputUtil {

    private static FwSoftInputUtil init = null;

    public static FwSoftInputUtil builder() {
        if (null == init) {
            init = new FwSoftInputUtil();
        }
        return init;
    }

    /**
     * 打开软键盘
     *
     * @param editText
     * @param mContext
     */
    public void openkeybord(@NonNull EditText editText, @NonNull Context mContext) {
        editText.setFocusable(true);
        editText.setFocusableInTouchMode(true);
        editText.requestFocus();

        InputMethodManager imm = (InputMethodManager) mContext
                .getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(editText, InputMethodManager.RESULT_SHOWN);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED,
                InputMethodManager.HIDE_IMPLICIT_ONLY);
    }

    /**
     * 打开软键盘
     *
     * @param editText
     * @param activity
     */
    public void openkeybord(@NonNull EditText editText, @NonNull FragmentActivity activity) {
        editText.setFocusable(true);
        editText.setFocusableInTouchMode(true);
        editText.requestFocus();

        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(editText, InputMethodManager.RESULT_SHOWN);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED,
                InputMethodManager.HIDE_IMPLICIT_ONLY);
    }

    /**
     * 关闭软键盘
     *
     * @param editText
     * @param mContext
     */
    public void closeKeybord(@NonNull EditText editText, @NonNull Context mContext) {
        InputMethodManager imm = (InputMethodManager) mContext.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(editText.getWindowToken(), 0);
    }

    /**
     * 关闭软键盘
     *
     * @param activity
     */
    public void closeKeybord(@NonNull FragmentActivity activity) {
        View view = activity.getWindow().peekDecorView();
        if (null != view) {
            InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    /**
     * 关闭软键盘
     *
     * @param mContext
     * @param view
     */
    public void closeKeybord(@NonNull Context mContext, View view) {
        if (null != view) {
            InputMethodManager imm = (InputMethodManager) mContext.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    /**
     * 关闭软键盘
     *
     * @param editText
     * @param editInfoActionId (EditorInfo.IME_ACTION_SEARCH)
     * @param icallback
     */
    public void closeKeybord(EditText editText, final int editInfoActionId, final ICloseKeybordCallback icallback) {
        editText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView tvw, int actionId, KeyEvent event) {
                if (actionId == editInfoActionId) {
                    InputMethodManager imm = (InputMethodManager) tvw.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                    if (tvw == null) {
                        imm.toggleSoftInput(0, InputMethodManager.RESULT_HIDDEN);
                    } else {
                        imm.hideSoftInputFromWindow(tvw.getWindowToken(), 0); //强制隐藏键盘
                    }
                    icallback.callback();
                    return true;
                }
                return false;
            }
        });
    }

    //关闭回调事件
    public interface ICloseKeybordCallback {
        void callback();
    }

}