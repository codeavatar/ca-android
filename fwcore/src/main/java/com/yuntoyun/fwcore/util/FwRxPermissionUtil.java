package com.yuntoyun.fwcore.util;
/*
 * +----------------------------------------------------------------------
 * | @Author: codeavatar   @Year：2021
 * +----------------------------------------------------------------------
 * | @Email: codeavatar@aliyun.com
 * +----------------------------------------------------------------------
 */
import android.Manifest;
import android.app.Activity;
import android.view.View;

import com.yuntoyun.fwcore.config.FwAppConfig;
import com.tbruyelle.rxpermissions2.Permission;
import com.tbruyelle.rxpermissions2.RxPermissions;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import io.reactivex.functions.Consumer;
import rx.functions.Action1;

/**
 * 权限工具类
 */
public class FwRxPermissionUtil extends RxPermissions {

    public FwRxPermissionUtil(@NonNull FragmentActivity activity) {
        super(activity);
    }

    public FwRxPermissionUtil(@NonNull Fragment fragment) {
        super(fragment);
    }

    /**
     * 使用单个权限
     *
     * @param action1
     */
    public void usePermission(Consumer<Boolean> action1, String permissions) {
        this.setLogging(FwAppConfig.PRINT_LOG_SWITCH);
        this.request(permissions).subscribe(action1);
    }

    /**
     * 使用多个权限
     *
     * @param action1
     * @param permissions
     */
    public void useMultPermission(Consumer<Permission> action1, final String... permissions) {
        this.setLogging(FwAppConfig.PRINT_LOG_SWITCH);
        this.requestEach(permissions).subscribe(action1);
    }

    /**
     * 使用绑定权限
     *
     * @param activity
     * @param bindView
     * @param action1
     * @param permissions
     */
    public void useBindPermission(FragmentActivity activity, View bindView,
                                  Action1<Boolean> action1,
                                  final String... permissions) {
//        RxPermissions rxPermissions = new RxPermissions(activity);
//        rxPermissions.setLogging(FwAppConfig.PRINT_LOG_SWITCH);
//        RxView.clicks(bindView)
//                .compose(rxPermissions.ensure(permissions)).subscribe()
//                .subscribe(action1);

    }

    /**
     * 批量检测权限
     *
     * @param permissions
     */
    public void doCheckPermissions(String... permissions) {
        super.requestEach(permissions).subscribe(new Consumer<Permission>() {
            @Override
            public void accept(Permission permission) throws Exception {
                if (permission.granted) {
                    // `permission.name` is granted !
                } else if (permission.shouldShowRequestPermissionRationale) {
                    // Denied permission without ask never again
                } else {
                    // Denied permission with ask never again
                    // Need to go to the settings
                }
            }
        });
    }

    /**
     * 拔打电话（多个号码使用英式逗号隔开）
     *
     * @param telNum
     * @param activity
     */
    public void doDialPhone(final String telNum, final Activity activity) {
        super.request(Manifest.permission.CAMERA).subscribe(new Consumer<Boolean>() {
            @Override
            public void accept(Boolean aBoolean) throws Exception {
                if (aBoolean) {
                    if (null != activity) {
                        FwPhoneUtil.builder().dialMultiPhone(telNum, activity);
                    }
                } else {
                    FwAlertDialogUtil.builder().showAlertDialog(activity, "警告", "请开启打电话权限!");
                }
            }
        });
    }
}
