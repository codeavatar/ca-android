package com.yuntoyun.fwcore.util;
/*
 * +----------------------------------------------------------------------
 * | @Author: codeavatar   @Year：2021
 * +----------------------------------------------------------------------
 * | @Email: codeavatar@aliyun.com
 * +----------------------------------------------------------------------
 */
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.yuntoyun.fwcore.R;

/**
 * 对话框工具类
 */
public class FwDialogUtil {

    private static Dialog dialog = null;

    /**
     * 显示加载
     * @param context 上下文引用
     * @param message 提示信息
     * @param cancelable 是否可取消
     */
    public static void showLoading(Context context, String message, boolean cancelable) {

        if (null != dialog) {
            dialog.dismiss();
            dialog = null;
        }

        if (null == context) return;

        View view = LayoutInflater.from(context).inflate(R.layout.fw_util_dialog_loading, null);
        TextView loadingText = (TextView) view.findViewById(R.id.tvw_loading_text);
        loadingText.setText(message);
        dialog = new Dialog(context, R.style.fw_dialog_loading);
        dialog.setCancelable(cancelable);
        dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dg) {
                dialog = null;
            }
        });
        dialog.setCanceledOnTouchOutside(false);
        dialog.setContentView(view, new LinearLayout.LayoutParams(LinearLayout.LayoutParams
                .MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT));
        Activity activity = (Activity) context;
        if (activity.isFinishing()) return;
        dialog.show();
    }

    /**
     * 关闭加载
     */
    public static void dismissLoading(){
        if (null != dialog && dialog.isShowing()) {
            dialog.dismiss();
            dialog = null;
        }
    }


}
