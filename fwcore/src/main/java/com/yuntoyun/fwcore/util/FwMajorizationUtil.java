package com.yuntoyun.fwcore.util;
/*
 * +----------------------------------------------------------------------
 * | @Author: codeavatar   @Year：2021
 * +----------------------------------------------------------------------
 * | @Email: codeavatar@aliyun.com
 * +----------------------------------------------------------------------
 */
import android.view.View;

/**
 * 优化工具类
 */
public class FwMajorizationUtil {

    /**
     * 记录上次点击时间(毫秒)
     */
    private long lastClick = 0;

    private static FwMajorizationUtil init = null;

    public synchronized static FwMajorizationUtil builder() {
        if (null == init) {
            init = new FwMajorizationUtil();
        }
        return init;
    }

    /**
     * 判断是否快速点击
     *
     * @return true:是，false:否
     */
    public boolean isFastClick() {
        //单位为毫秒
        long now = System.currentTimeMillis();
        if (now - lastClick < 2000) {
            return true;
        } else {
            lastClick = now;
        }
        return false;
    }

    /**
     * 控制快速点击限制
     *
     * @param view
     * @param clickListener
     */
    public void setOnClickListener(View view, View.OnClickListener clickListener) {
        if (!view.hasOnClickListeners()) {
            view.setOnClickListener(clickListener);
        } else {
            if (!isFastClick()) {
                view.performClick();
            }
        }
    }

}
