package com.yuntoyun.fwcore.util;
/*
 * +----------------------------------------------------------------------
 * | @Author: codeavatar   @Year：2021
 * +----------------------------------------------------------------------
 * | @Email: codeavatar@aliyun.com
 * +----------------------------------------------------------------------
 */
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.GridLayout;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * 组件工具类
 */
public class FwComponentUtil {

    private final String TAG = FwComponentUtil.class.getSimpleName();

    private static FwComponentUtil instance = null;

    public static FwComponentUtil builder() {
        if (null == instance) {
            instance = new FwComponentUtil();
        }
        return instance;
    }

    /**
     * 设置子视图单击事件
     *
     * @param lltContainer
     * @param listener
     */
    public void setOnChildViewClick(LinearLayout lltContainer, View.OnClickListener listener) {
        if (null == lltContainer || null == listener) return;

        //第二层
        for (int i = 0; i < lltContainer.getChildCount(); i++) {
            View tmpView = lltContainer.getChildAt(i);
            if (tmpView.getId() != 0) {
                tmpView.setOnClickListener(listener);
            } else {
                if (tmpView instanceof LinearLayout) {
                    LinearLayout tmpLltContainer = (LinearLayout) tmpView;
                    //第三层
                    for (int m = 0; m < tmpLltContainer.getChildCount(); m++) {
                        View tmpChildView = tmpLltContainer.getChildAt(m);
                        if (tmpChildView.getId() != 0) {
                            tmpChildView.setOnClickListener(listener);
                        }
                    }
                }
            }
        }
    }

    /**
     * 设置容器中指定标识的单击事件
     *
     * @param container
     * @param resIds
     * @param listener
     */
    public void setOnChildViewClick(View container, int resIds[], View.OnClickListener listener) {
        if (null == container || null == listener || null == resIds) return;

        for (int resId : resIds) {
            View tmpView = container.findViewById(resId);
            if (null != tmpView) {
                tmpView.setOnClickListener(listener);
            }
        }
    }

    /**
     * 设置RadioGroup组件元素的点击事件
     *
     * @param rgp
     * @param clickListener
     */
    public void setRadioGroupItemClicklistener(RadioGroup rgp, View.OnClickListener clickListener) {
        if (null == rgp || null == clickListener) return;

        List<RadioButton> list = this.getRadioButtonsOfRadioGroup(rgp);
        for (RadioButton rbn : list) {
            rbn.setOnClickListener(clickListener);
        }
    }

    /**
     * 设置RadioGroup组件使用状态
     *
     * @param rgp
     * @param enabled
     */
    public void setRadioGroupDisabled(RadioGroup rgp, boolean enabled) {
        if (null == rgp) return;

        List<RadioButton> list = this.getRadioButtonsOfRadioGroup(rgp);
        for (RadioButton rbn : list) {
            rbn.setEnabled(enabled);
        }
    }

    /**
     * 设置RadioGroup指定元素选中
     *
     * @param rgp
     * @param resId
     */
    public void setRadioGroupCheckedByResid(RadioGroup rgp, int resId) {
        if (null == rgp) return;

        List<RadioButton> list = this.getRadioButtonsOfRadioGroup(rgp);
        for (RadioButton rbn : list) {
            if (rbn.getId() == resId) {
                rgp.check(resId);
//                //不推荐使用下面的(设置RadioButton的默认选中, 使用setChecked(true) 这样会使RadioButton一直处于选中状态. )
//                rbn.setChecked(true);
            }
        }
    }

    /**
     * 设置RadioGroup指定元素选中
     *
     * @param rgp
     * @param index
     */
    public void setRadioGroupCheckedByIndex(RadioGroup rgp, int index) {
        if (null == rgp) return;

        List<RadioButton> list = this.getRadioButtonsOfRadioGroup(rgp);
        for (int i = 0; i < list.size(); i++) {
            if (i == index) {
                rgp.check(list.get(i).getId());
                //不推荐使用下面的(设置RadioButton的默认选中, 使用setChecked(true) 这样会使RadioButton一直处于选中状态. )
//                rbn.setChecked(true);
            }
        }
    }

    /**
     * 设置RadioGroup为非选中状态
     *
     * @param rgp
     */
    public void setRadioGroupClearChecked(RadioGroup rgp) {
        if (null == rgp) return;

        rgp.clearCheck();
    }

    /**
     * 读取RadioGroup中的所有RadioButton组件
     *
     * @param rgp
     * @return
     */
    public List<RadioButton> getRadioButtonsOfRadioGroup(RadioGroup rgp) {
        if (null == rgp) return null;

        List<RadioButton> tmpList = new ArrayList<>();
        for (int i = 0; i < rgp.getChildCount(); i++) {
            View childView = rgp.getChildAt(i);
            if (childView instanceof RadioButton) {
                tmpList.add((RadioButton) childView);
            } else if (childView instanceof LinearLayout) {
                LinearLayout tmpLlt = (LinearLayout) childView;
                tmpList.addAll(this.getSelfRadioButtonList(tmpLlt));
            } else if (childView instanceof GridLayout) {
                GridLayout tmpGlt = (GridLayout) childView;
                tmpList.addAll(this.getSelfRadioButtonList(tmpGlt));
            }
        }
        return tmpList;
    }

    /**
     * 设置RadioGroup嵌套布局中RadioButton单选
     *
     * @param rgp
     */
    public void setRadioGroupSingleChoice(RadioGroup rgp, IRadioGroupSingleChoice iRadioGroupSingleChoice) {
        if (null == rgp) return;

        List<RadioButton> list = this.getRadioButtonsOfRadioGroup(rgp);
        if (null == list) return;

        //单选限制
        for (RadioButton item : list) {
            item.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (isChecked) {
                        for (RadioButton rbn : list) {
                            if (item.getId() != rbn.getId())
                                rbn.setChecked(false);
                        }
                        iRadioGroupSingleChoice.onCheckedChanged(item);
                    }
                }
            });
        }
    }

    //设置RadioGroup嵌套布局中RadioButton单选（回调事件）
    public interface IRadioGroupSingleChoice {
        void onCheckedChanged(RadioButton radioButton);
    }

    /**
     * 设置RadioGroup组中RadioButton的Tag值为索引；
     *
     * @param rgp
     */
    public void setRadioGroupValue(RadioGroup rgp) {
        if (null == rgp) return;

        List<RadioButton> tmpList = new ArrayList<>();
        for (int i = 0; i < rgp.getChildCount(); i++) {
            View childView = rgp.getChildAt(i);
            if (childView instanceof RadioButton) {
                tmpList.add((RadioButton) childView);
            } else if (childView instanceof LinearLayout) {
                LinearLayout tmpLlt = (LinearLayout) childView;
                tmpList.addAll(this.getSelfRadioButtonList(tmpLlt));
            } else if (childView instanceof GridLayout) {
                GridLayout tmpGlt = (GridLayout) childView;
                tmpList.addAll(this.getSelfRadioButtonList(tmpGlt));
            }
        }
        //按照顺序初始化RadioButton的Tag值。
        for (int n = 0; n < tmpList.size(); n++) {
            tmpList.get(n).setTag(n);
        }
    }

    /**
     * 读取RadioGroup中RadioButton选中项的Tag值；
     *
     * @param rgp
     * @return
     */
    public <T> T getRadioGroupValue(RadioGroup rgp) {
        RadioButton tmpRbn = this.getSelfRadioButton(rgp);
        if (null != tmpRbn) {
            return (T) tmpRbn.getTag();
        }
        return null;
    }

    /**
     * 读取RadioGroup中RadioButton选中项的Text值；
     *
     * @param rgp
     * @return
     */
    public String getRadioGroupText(RadioGroup rgp) {
        RadioButton tmpRbn = this.getSelfRadioButton(rgp);
        if (null != tmpRbn) {
            return tmpRbn.getText().toString().trim();
        }
        return null;
    }

    /**
     * 读取RadioGroup选中的RadioButton
     *
     * @param rgp
     * @return
     */
    public RadioButton getRadioGroupCheckedButton(RadioGroup rgp) {
        return this.getSelfRadioButton(rgp);
    }

    /**
     * 读取RadioGroup指定索引的RadioButton
     *
     * @param rgp
     * @param index
     * @return
     */
    public RadioButton getRadioGroupIndexButton(RadioGroup rgp, int index) {
        List<RadioButton> list = this.getSelfRadioButtonList(rgp);
        if (list.size() <= index) return null;
        return list.get(index);
    }

    /**
     * 读取组件值
     *
     * @param view
     * @return
     */
    public String getViewValue(View view) {
        if (null == view) return "";

        if (view instanceof TextView) {
            return ((TextView) view).getText().toString().trim();
        } else if (view instanceof EditText) {
            return ((EditText) view).getText().toString().trim();
        } else if (view instanceof CheckBox) {
            return ((CheckBox) view).getText().toString().trim();
        } else if (view instanceof RadioButton) {
            return ((RadioButton) view).getText().toString().trim();
        } else if (view instanceof Spinner) {
            return ((Spinner) view).getSelectedItem().toString().trim();
        }

        return "";
    }

    /**
     * 读取组件Hint值
     *
     * @param view
     * @return
     */
    public String getViewHint(View view) {
        if (null == view) return "";

        if (view instanceof TextView) {
            return ((TextView) view).getHint().toString().trim();
        } else if (view instanceof EditText) {
            return ((EditText) view).getHint().toString().trim();
        } else if (view instanceof CheckBox) {
            return ((CheckBox) view).getHint().toString().trim();
        } else if (view instanceof RadioButton) {
            return ((RadioButton) view).getHint().toString().trim();
        } else if (view instanceof Button) {
            return ((Button) view).getHint().toString().trim();
        }

        return "";
    }


    /**
     * 读取组件Tag值
     *
     * @param view
     * @return
     */
    public <T> T getViewTag(View view) {
        if (null == view) return null;

        return (T) view.getTag();
    }

    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    private RadioButton getSelfRadioButton(ViewGroup vgp) {
        RadioButton tmpRbn = null;
        for (int m = 0; m < vgp.getChildCount(); m++) {
            View childView = vgp.getChildAt(m);
            if (childView instanceof RadioButton) {
                RadioButton rbn = ((RadioButton) childView);
                if (rbn.isChecked()) {
                    tmpRbn = rbn;
                    break;
                }
            } else if (childView instanceof LinearLayout) {
                LinearLayout tmpLlt = (LinearLayout) childView;
                RadioButton rbn = this.getSelfRadioButton(tmpLlt);
                if (rbn.isChecked()) {
                    tmpRbn = rbn;
                    break;
                }
            } else if (childView instanceof GridLayout) {
                GridLayout tmpGlt = (GridLayout) childView;
                RadioButton rbn = this.getSelfRadioButton(tmpGlt);
                if (rbn.isChecked()) {
                    tmpRbn = rbn;
                    break;
                }
            }
        }
        return tmpRbn;
    }

    private List<RadioButton> getSelfRadioButtonList(ViewGroup vgp) {
        List<RadioButton> tmpList = new ArrayList<>();
        for (int m = 0; m < vgp.getChildCount(); m++) {
            View childView = vgp.getChildAt(m);
            if (childView instanceof RadioButton) {
                tmpList.add((RadioButton) childView);
            } else if (childView instanceof LinearLayout) {
                LinearLayout tmpLlt = (LinearLayout) childView;
                tmpList.addAll(this.getSelfRadioButtonList(tmpLlt));
            } else if (childView instanceof GridLayout) {
                GridLayout tmpGlt = (GridLayout) childView;
                tmpList.addAll(this.getSelfRadioButtonList(tmpGlt));
            }
        }
        return tmpList;
    }

}
