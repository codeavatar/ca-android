package com.yuntoyun.fwcore.util;
/*
 * +----------------------------------------------------------------------
 * | @Author: codeavatar   @Year：2021
 * +----------------------------------------------------------------------
 * | @Email: codeavatar@aliyun.com
 * +----------------------------------------------------------------------
 */
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

/**
 * 意图工具类
 */
public class FwIntentUtil {

    private static FwIntentUtil init = null;

    public static synchronized FwIntentUtil builder() {
        if (null == init) {
            init = new FwIntentUtil();
        }
        return init;
    }

    /**
     * 启动新活动
     *
     * @param activity
     * @param cls
     */
    public void startActivity(Activity activity, Class<?> cls) {
        Intent intent = new Intent(activity, cls);
        activity.startActivity(intent);
    }

    /**
     * 启动带参数新活动
     *
     * @param activity
     * @param cls
     * @param bundle
     */
    public void startActivity(Activity activity, Class<?> cls, Bundle bundle) {
        Intent intent = new Intent(activity, cls);
        FwLogUtil.info(bundle);
        if (null != bundle)
            intent.putExtras(bundle);
        activity.startActivity(intent);
    }

    /**
     * 启动带参数和启动类型的新活动
     *
     * @param activity
     * @param cls
     * @param bundle
     * @param flags
     */
    public void startActivity(Activity activity, Class<?> cls, Bundle bundle, int flags[]) {
        Intent intent = new Intent(activity, cls);
        FwLogUtil.info(bundle);
        if (null != flags) {
            for (int item : flags) {
                intent.setFlags(item);
            }
        }
        if (null != bundle)
            intent.putExtras(bundle);
        activity.startActivity(intent);
    }

    /**
     * 获取Bundle
     *
     * @param activity
     * @return
     */
    public Bundle getActivityBundle(Activity activity) {
        Bundle bundle = null;
        if (null != activity.getIntent()) {
            bundle = activity.getIntent().getExtras();
        }
        return bundle;
    }

    /**
     * 启动指定Bundle键名的新活动
     *
     * @param activity
     * @param cls
     * @param bundle
     * @param bundleKey
     */
    public void startActivity(Activity activity, Class<?> cls, Bundle bundle, String bundleKey) {
        Intent intent = new Intent(activity, cls);
        if (null != bundle)
            intent.putExtra(bundleKey, bundle);
        FwLogUtil.info(bundle);
        activity.startActivity(intent);
    }

    /**
     * 启动指定Bundle键名和启动类型的新活动
     *
     * @param activity
     * @param cls
     * @param bundle
     * @param bundleKey
     * @param flags
     */
    public void startActivity(Activity activity, Class<?> cls, Bundle bundle, String bundleKey, int
            flags[]) {
        Intent intent = new Intent(activity, cls);
        if (null != bundle) {
            intent.putExtra(bundleKey, bundle);
            FwLogUtil.info(bundle);
        }
        if (null != flags) {
            for (int item : flags) {
                intent.setFlags(item);
            }
        }
        activity.startActivity(intent);
    }

    /**
     * 获取指定键名的Bundle
     *
     * @param activity
     * @param key
     * @return
     */
    public Bundle getActivityBundle(Activity activity, String key) {
        Bundle bundle = null;
        if (null != activity.getIntent()) {
            bundle = activity.getIntent().getBundleExtra(key);
            FwLogUtil.info(bundle);
        }
        return bundle;
    }

    /**
     * 启动活动组
     *
     * @param activity
     * @param intents
     */
    public void startActivitys(Activity activity, Intent[] intents) {
        if (null != intents) {
            activity.startActivities(intents);
        }
    }

    /**
     * 启动带Bundle参数的活动组
     *
     * @param activity
     * @param intents
     */
    public void startActivitys(Activity activity, Intent[] intents, Bundle options) {
        if (null != intents && null != options) {
            FwLogUtil.info(options);
            activity.startActivities(intents, options);
        }
    }

    /**
     * 启动带有返回的活动
     *
     * @param activity
     * @param cls
     * @param requestCode
     */
    public void startActivityForResult(Activity activity, Class<?> cls, int requestCode) {
        Intent intent = new Intent(activity, cls);
        activity.startActivityForResult(intent, requestCode);
    }

    /**
     * 启动带有Bundle并且返回的活动
     *
     * @param activity
     * @param cls
     * @param requestCode
     * @param options
     */
    public void startActivityForResult(Activity activity, Class<?> cls, int requestCode, Bundle
            options) {
        Intent intent = new Intent(activity, cls);
        FwLogUtil.info(options);
        if (null != options)
            intent.putExtras(options);
        activity.startActivityForResult(intent, requestCode);
    }

    /**
     * 结束活动返回结果
     *
     * @param activity
     * @param resultCode
     */
    public void stopActivityBackResult(Activity activity, int resultCode) {
        activity.setResult(resultCode);
        activity.finish();
    }

    /**
     * 结束活动返回结果和Bundle数据
     *
     * @param activity
     * @param resultCode
     * @param options
     */
    public void stopActivityBackResult(Activity activity, int resultCode, Bundle options) {
        Intent intent = new Intent();
        FwLogUtil.info(options);
        if (null != options)
            intent.putExtra("ext_bundle", options);
        activity.setResult(resultCode, intent);
        activity.finish();
    }

    /**
     * 获取onActivityResult事件的Intent中的Bundle数据
     *
     * @param data
     * @return
     */
    public Bundle getActivityResultBundle(Intent data) {
        Bundle bundle = data.getBundleExtra("ext_bundle");
        FwLogUtil.info(bundle);
        return bundle;
    }
}
