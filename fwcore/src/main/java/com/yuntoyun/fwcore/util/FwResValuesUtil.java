package com.yuntoyun.fwcore.util;
/*
 * +----------------------------------------------------------------------
 * | @Author: codeavatar   @Year：2021
 * +----------------------------------------------------------------------
 * | @Email: codeavatar@aliyun.com
 * +----------------------------------------------------------------------
 */
import android.content.Context;
import android.util.TypedValue;

/**
 * 项目资源工具类
 */
public class FwResValuesUtil {

    private static FwResValuesUtil init = null;

    public static FwResValuesUtil builder(Context context) {
        if (null == init) {
            init = new FwResValuesUtil();
        }
        return init;
    }

    public float dpToPx(Context context, float dp) {
        return TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_DIP, dp, context.getResources().getDisplayMetrics());
    }
}
