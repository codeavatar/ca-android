package com.yuntoyun.fwcore.util;
/*
 * +----------------------------------------------------------------------
 * | @Author: codeavatar   @Year：2021
 * +----------------------------------------------------------------------
 * | @Email: codeavatar@aliyun.com
 * +----------------------------------------------------------------------
 */
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;

import com.yuntoyun.fwcore.base.FwApplication;
import com.yuntoyun.fwcore.config.FwConstConfig;

/**
 * 系统应用工具类
 */
public class FwApplicationUtil {

    private final String TAG = this.getClass().getSimpleName();

    private static FwApplicationUtil init = null;

    public static FwApplicationUtil builder() {
        if (null == init) {
            init = new FwApplicationUtil();
        }
        return init;
    }

    /**
     * Base 中的 FwApplication 上下文
     *
     * @return
     */
    public static Context appContext() {
        return FwApplication.getAppContext();
    }

    /**
     * 获取应用的包名
     *
     * @return
     */
    public String getAppPackageName() {
        String appPackageName = "";
        Context context = appContext();

        if (null == context) {
            context = FwApplication.getAppContext();
        }
        PackageManager packageManager = context.getPackageManager();
        try {
            PackageInfo packageInfo = packageManager.getPackageInfo(context.getPackageName(),
                    0);
            appPackageName = packageInfo.packageName;
        } catch (PackageManager.NameNotFoundException e) {
            appPackageName = FwConstConfig.CONST_PACKAGE_NAME;
            FwLogUtil.error(TAG, e);
        } catch (Exception e) {
            appPackageName = FwConstConfig.CONST_PACKAGE_NAME;
            FwLogUtil.error(TAG, e);
        }
        return appPackageName;
    }

    /**
     * 获取应用的版本号
     *
     * @return
     */
    public String getAppVersionName() {
        String appVersionName = "";
        Context context = appContext();

        if (null == context) {
            context = FwApplication.getAppContext();
        }
        PackageManager packageManager = context.getPackageManager();
        try {
            PackageInfo packageInfo = packageManager.getPackageInfo(context.getPackageName(),
                    0);
            appVersionName = packageInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            FwLogUtil.error(TAG, e);
        } catch (Exception e) {
            FwLogUtil.error(TAG, e);
        }
        return appVersionName;
    }

    /**
     * 获取应用的版本序号
     *
     * @return
     */
    public int getAppVersionCode() {
        int appVersionCode = 1;

        Context context = appContext();

        if (null == context) {
            context = FwApplication.getAppContext();
        }
        PackageManager packageManager = context.getPackageManager();
        try {
            PackageInfo packageInfo = packageManager.getPackageInfo(context.getPackageName(),
                    0);
            appVersionCode = packageInfo.versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            FwLogUtil.error(TAG, e);
        } catch (Exception e) {
            FwLogUtil.error(TAG, e);
        }
        return appVersionCode;
    }

    /**
     * 获取应用包对象(项目清单)
     *
     * @return
     */
    public PackageInfo getAppPackageInfo() {
        Context context = appContext();

        PackageManager packageManager = context.getPackageManager();
        try {
            return packageManager.getPackageInfo(context.getPackageName(),
                    0);
        } catch (PackageManager.NameNotFoundException e) {
            FwLogUtil.error(TAG, e);
        }
        return null;
    }


}
