package com.yuntoyun.fwcore.util.helper;
/*
 * +----------------------------------------------------------------------
 * | @Author: codeavatar   @Year：2021
 * +----------------------------------------------------------------------
 * | @Email: codeavatar@aliyun.com
 * +----------------------------------------------------------------------
 */
import com.yuntoyun.fwcore.util.FwLogUtil;
import com.yuntoyun.fwcore.util.FwPermissionUtil;
import com.yuntoyun.fwcore.util.constant.FwPermissionConstants;

import java.util.List;

/**
 * 权限助手
 */
public class FwPermissionHelper {

    public static void requestCamera(final OnPermissionGrantedListener listener) {
        request(listener, FwPermissionConstants.CAMERA);
    }

    public static void requestStorage(final OnPermissionGrantedListener listener) {
        request(listener, FwPermissionConstants.STORAGE);
    }

    public static void requestPhone(final OnPermissionGrantedListener listener) {
        request(listener, FwPermissionConstants.PHONE);
    }

    public static void requestPhone(final OnPermissionGrantedListener grantedListener,
                                    final OnPermissionDeniedListener deniedListener) {
        request(grantedListener, deniedListener, FwPermissionConstants.PHONE);
    }

    public static void requestSms(final OnPermissionGrantedListener listener) {
        request(listener, FwPermissionConstants.SMS);
    }

    public static void requestLocation(final OnPermissionGrantedListener listener) {
        request(listener, FwPermissionConstants.LOCATION);
    }

    private static void request(final OnPermissionGrantedListener grantedListener,
                                final @FwPermissionConstants.Permission String... permissions) {
        request(grantedListener, null, permissions);
    }

    private static void request(final OnPermissionGrantedListener grantedListener,
                                final OnPermissionDeniedListener deniedListener,
                                final @FwPermissionConstants.Permission String... permissions) {
        FwPermissionUtil.permission(permissions)
                .rationale(new FwPermissionUtil.OnRationaleListener() {
                    @Override
                    public void rationale(ShouldRequest shouldRequest) {
                        FwDialogHelper.showRationaleDialog(shouldRequest);
                    }
                })
                .callback(new FwPermissionUtil.FullCallback() {
                    @Override
                    public void onGranted(List<String> permissionsGranted) {
                        if (grantedListener != null) {
                            grantedListener.onPermissionGranted();
                        }
                        FwLogUtil.info(permissionsGranted);
                    }

                    @Override
                    public void onDenied(List<String> permissionsDeniedForever, List<String>
                            permissionsDenied) {
                        if (!permissionsDeniedForever.isEmpty()) {
                            FwDialogHelper.showOpenAppSettingDialog();
                        }
                        if (deniedListener != null) {
                            deniedListener.onPermissionDenied();
                        }

                        FwLogUtil.info(permissionsDeniedForever);
                        FwLogUtil.info(permissionsDenied);
                    }
                })
                .request();
    }

    public interface OnPermissionGrantedListener {
        void onPermissionGranted();
    }

    public interface OnPermissionDeniedListener {
        void onPermissionDenied();
    }
}
