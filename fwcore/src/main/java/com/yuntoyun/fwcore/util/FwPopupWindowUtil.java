package com.yuntoyun.fwcore.util;
/*
 * +----------------------------------------------------------------------
 * | @Author: codeavatar   @Year：2021
 * +----------------------------------------------------------------------
 * | @Email: codeavatar@aliyun.com
 * +----------------------------------------------------------------------
 */
import android.app.Activity;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.PopupWindow;
import android.widget.PopupWindow.OnDismissListener;
import android.widget.RelativeLayout.LayoutParams;

import com.yuntoyun.fwcore.R;

import androidx.fragment.app.FragmentActivity;

/**
 * 弹窗工具类
 */
public class FwPopupWindowUtil {

    /**
     * 不开放
     */
    private FwPopupWindowUtil() {
    }

    /////////
    ///设置弹窗
    /////////

    public static PopupWindow getPopupWindow(FragmentActivity activity, View contentView) {
        return getPopupWindow(activity, contentView, null, true, true, true);
    }

    public static PopupWindow getPopupWindow(FragmentActivity activity, View contentView,
                                             PopupDismissCallback dismissCallback) {
        return getPopupWindow(activity, contentView, dismissCallback, true, true, true);
    }

    public static PopupWindow getPopupWindow(FragmentActivity activity, View contentView,
                                             PopupDismissCallback dismissCallback,
                                             boolean isPopup) {
        return getPopupWindow(activity, contentView, dismissCallback, isPopup, true, true);
    }

    public static PopupWindow getPopupWindow(FragmentActivity activity, View contentView,
                                             PopupDismissCallback dismissCallback,
                                             boolean isPopup, boolean isMask) {
        return getPopupWindow(activity, contentView, dismissCallback, isPopup, isMask, true);
    }

    /**
     * @param activity
     * @param contentView
     * @param dismissCallback
     * @param isPopup         (true:上弹显示，false:下拉显示；)
     * @param isMask
     * @return
     */
    public static PopupWindow getPopupWindow(FragmentActivity activity, View contentView,
                                             PopupDismissCallback dismissCallback,
                                             boolean isPopup, boolean isMask,
                                             boolean outsideTouchable) {

        PopupWindow popupWindow = new PopupWindow(contentView,
                LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT, true);
        popupWindow.setAnimationStyle((isPopup) ? R.style.fw_popup_bottom :
                R.style.fw_popup_animation);
        ColorDrawable dw = new ColorDrawable(0x7DC0C0C0);
        popupWindow.setBackgroundDrawable(dw);
        popupWindow.setClippingEnabled(true);
        popupWindow.setTouchable(true);
        popupWindow.setOutsideTouchable(outsideTouchable);
        popupWindow.setOnDismissListener(() -> {
            popupWindow.dismiss();
            if (isMask) {
                setWinBgAlpha(activity, 1.0f);
            }
            if (null != dismissCallback)
                dismissCallback.callback();
        });
        return popupWindow;
    }

    /////////
    ///触发弹窗
    /////////

    public static void showPopupWindow(FragmentActivity activity, PopupWindow popupWindow,
                                       View parentView) {
        showPopupWindow(activity, popupWindow, parentView, true, Gravity.BOTTOM, true, 0, 0);
    }

    public static void showPopupWindow(FragmentActivity activity, PopupWindow popupWindow,
                                       View parentView, boolean isPopup, int gravity) {
        showPopupWindow(activity, popupWindow, parentView, isPopup, gravity, true, 0, 0);
    }

    public static void showPopupWindow(FragmentActivity activity, PopupWindow popupWindow,
                                       View parentView, boolean isPopup, int gravity,
                                       boolean isMask) {
        showPopupWindow(activity, popupWindow, parentView, isPopup, gravity, isMask, 0, 0);
    }

    public static void showPopupWindow(FragmentActivity activity, PopupWindow popupWindow,
                                       View parentView, boolean isPopup, int gravity,
                                       boolean isMask, int x, int y) {
        if (-1 == gravity)
            gravity = Gravity.NO_GRAVITY;

        if (Build.VERSION.SDK_INT < 24) {
            if (isPopup) {
                popupWindow.showAtLocation(parentView, gravity, x, y);
            } else {
                popupWindow.showAsDropDown(parentView, x, y, gravity);
            }
        } else {
            if (isPopup) {
                popupWindow.showAtLocation(parentView, gravity, x, y);
            } else {
                int[] location = new int[2];
                parentView.getLocationOnScreen(location);
                int tmpX = (x != 0) ? x : location[0];
                int tmpY = (y != 0) ? y : location[1];
                popupWindow.showAtLocation(parentView, gravity, tmpX, tmpY + parentView
                        .getHeight());
            }
        }
        if (isMask) {
            setWinBgAlpha(activity, 0.5f);
        }
    }

    public static void setWinBgAlpha(Activity activity, float bgAlpha) {
        WindowManager.LayoutParams lp = activity.getWindow().getAttributes();
        lp.alpha = bgAlpha; //0.0-1.0
        activity.getWindow().setAttributes(lp);
    }

    /**
     * 关闭回调
     */
    public interface PopupDismissCallback {
        void callback();
    }

    public static PopupWindow setPopupWindow(View contentView,
                                             OnDismissListener dismissListener, int style, int
                                                     color, int width, int height) {

        PopupWindow popupWindow = new PopupWindow(contentView);
        popupWindow.setWidth((0 == width) ? LayoutParams.WRAP_CONTENT : width);
        popupWindow.setHeight((0 == height) ? LayoutParams.WRAP_CONTENT : height);
        popupWindow.setFocusable(true);

        if (0 == style) {
            style = R.style.fw_popup_animation;
        }

        popupWindow.setAnimationStyle(style);

        if (color == 0) {
            color = 0x7DC0C0C0;
        }

        ColorDrawable dw = new ColorDrawable(color);
        popupWindow.setBackgroundDrawable(dw);
        popupWindow.setClippingEnabled(true);
        popupWindow.setTouchable(true);
        popupWindow.setOutsideTouchable(true);
        popupWindow.setOnDismissListener(dismissListener);
        return popupWindow;
    }

    public static PopupWindow showPopupWindow(View parent, View contentView,
                                              int gravity, int width, int height, boolean
                                                      touchable, int animationStyle) {
        final PopupWindow popupWindow_pic = new PopupWindow(contentView, width,
                height);
        ColorDrawable dw = new ColorDrawable(0x66000000);
        popupWindow_pic.setAnimationStyle(animationStyle);
        popupWindow_pic.setBackgroundDrawable(dw);
        popupWindow_pic.setFocusable(true);
        popupWindow_pic.setOutsideTouchable(touchable);
        popupWindow_pic
                .setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        if (gravity != -1) {
            popupWindow_pic.showAtLocation(parent, gravity, 0, 0);
        } else {
            if (Build.VERSION.SDK_INT < 24) {
                popupWindow_pic.showAsDropDown(parent);
            } else {
                int[] location = new int[2];
                parent.getLocationOnScreen(location);
                int x = location[0];
                int y = location[1];
                popupWindow_pic.showAtLocation(parent, Gravity.NO_GRAVITY, 0, y + parent
                        .getHeight());
            }
        }
        contentView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popupWindow_pic.dismiss();
            }
        });
        return popupWindow_pic;
    }

    public static PopupWindow showPopupWindow(View parent, View contentView,
                                              int gravity, int width, int height, boolean
                                                      touchable, int animationStyle, boolean
                                                      clickClose) {
        final PopupWindow popupWindow_pic = new PopupWindow(contentView, width,
                height);
        ColorDrawable dw = new ColorDrawable(0x66000000);
        popupWindow_pic.setAnimationStyle(animationStyle);
        popupWindow_pic.setBackgroundDrawable(dw);
        popupWindow_pic.setFocusable(true);
        popupWindow_pic.setOutsideTouchable(touchable);
        popupWindow_pic
                .setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        if (gravity != -1) {
            popupWindow_pic.showAtLocation(parent, gravity, 0, 0);
        } else {
            popupWindow_pic.showAsDropDown(parent);
        }
        if (clickClose) {
            contentView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    popupWindow_pic.dismiss();
                }
            });
        }
        return popupWindow_pic;
    }
}
