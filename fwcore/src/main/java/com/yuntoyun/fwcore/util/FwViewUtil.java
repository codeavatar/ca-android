package com.yuntoyun.fwcore.util;
/*
 * +----------------------------------------------------------------------
 * | @Author: codeavatar   @Year：2021
 * +----------------------------------------------------------------------
 * | @Email: codeavatar@aliyun.com
 * +----------------------------------------------------------------------
 */
import android.app.Activity;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.LinearLayout;

/**
 * View视图工具类
 */
public class FwViewUtil {

    private static FwViewUtil init = null;

    public static FwViewUtil builder() {
        if (null == init) {
            init = new FwViewUtil();
        }
        return init;
    }

    /**
     * 根据屏幕尺寸设置View宽与高
     *
     * @param activity
     * @param view
     * @param widthPercent  View宽占屏幕宽的比例
     * @param heightPercent View高占屏幕高的比例
     */
    public void setScreenView(Activity activity, final View view, double widthPercent, double
            heightPercent) {

        DisplayMetrics dm = activity.getResources().getDisplayMetrics();
        int screenWidth = dm.widthPixels;
        int screenHeight = dm.heightPixels;

        int viewWidth = (int) (screenWidth * widthPercent);
        int viewHeight = (int) (screenHeight * heightPercent);

        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                viewWidth, viewHeight);
        view.setLayoutParams(params);
    }

    /**
     * 根据屏幕宽度设置View宽与高
     *
     * @param activity
     * @param view
     * @param widthPercent   View宽占屏幕的比例
     * @param viewProportion View宽与高的比例
     */
    public void setViewByScreenWidth(Activity activity, View view, double widthPercent,
                                     double viewProportion) {
        DisplayMetrics dm = activity.getResources().getDisplayMetrics();
        int screenWidth = dm.widthPixels;

        int viewWidth = (int) (screenWidth * widthPercent);
        int viewHeight = (int) (viewWidth * viewProportion);

        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                viewWidth, viewHeight);
        view.setLayoutParams(params);
    }

    /**
     * 根据屏幕宽度设置View高
     *
     * @param activity
     * @param view
     * @param widthPercent View宽占屏幕的比例
     */
    public void setViewHeightByScreenWidth(Activity activity, View view, double widthPercent) {
        DisplayMetrics dm = activity.getResources().getDisplayMetrics();
        int screenWidth = dm.widthPixels;

        int viewHeight = (int) (screenWidth * widthPercent);

        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                screenWidth, viewHeight);
        view.setLayoutParams(params);
    }

    /**
     * 根据屏幕宽度设置View高
     *
     * @param activity
     * @param view
     * @param widthPercent View宽占屏幕的比例
     * @param viewPadding  View的填充尺寸
     */
    public void setViewHeightByScreenWidth(Activity activity, final View view, double
            widthPercent, int viewPadding) {
        DisplayMetrics dm = activity.getResources().getDisplayMetrics();
        int screenWidth = dm.widthPixels;
        int height = (int) ((screenWidth - viewPadding) * widthPercent);

        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                screenWidth, height);
        view.setLayoutParams(params);
    }
}
