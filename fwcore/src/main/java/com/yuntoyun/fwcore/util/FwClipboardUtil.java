package com.yuntoyun.fwcore.util;
/*
 * +----------------------------------------------------------------------
 * | @Author: codeavatar   @Year：2021
 * +----------------------------------------------------------------------
 * | @Email: codeavatar@aliyun.com
 * +----------------------------------------------------------------------
 */
import android.content.ClipData;
import android.content.ClipDescription;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;

/**
 * 剪贴板工具类
 */
public class FwClipboardUtil {

    private static FwClipboardUtil init = null;

    public synchronized static FwClipboardUtil builder() {
        if (null == init) {
            init = new FwClipboardUtil();
        }
        return init;
    }

    /**
     * 复制文本到剪贴板
     *
     * @param text 文本
     */
    public void copyText(final CharSequence text) {
        ClipboardManager clipboard = (ClipboardManager) FwApplicationUtil.appContext()
                .getSystemService(Context.CLIPBOARD_SERVICE);
        clipboard.setPrimaryClip(ClipData.newPlainText("text", text));
    }

    public void copyText(Context context, final CharSequence text) {
        ClipboardManager clipboard = (ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
        clipboard.setPrimaryClip(ClipData.newPlainText("text", text));
    }

    /**
     * 获取剪贴板的文本
     *
     * @return 剪贴板的文本
     */
    public CharSequence getText() {
        ClipboardManager clipboard = (ClipboardManager) FwApplicationUtil.appContext()
                .getSystemService(Context.CLIPBOARD_SERVICE);
        if (clipboard.hasPrimaryClip()) {
            ClipData clip = clipboard.getPrimaryClip();
            //限文本信息 clipboard.getPrimaryClipDescription().hasMimeType(ClipDescription.MIMETYPE_TEXT_PLAIN);
            if (clip != null && clip.getItemCount() > 0) {
//            return clip.getItemAt(0).coerceToText(FwApplicationUtil.appContext());
                return clip.getItemAt(0).getText().toString().trim();
            }
        }
        return null;
    }

    public CharSequence getText(Context context) {
        ClipboardManager clipboard = (ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
        if (clipboard.hasPrimaryClip()) {
            ClipData clip = clipboard.getPrimaryClip();
            //限文本信息 clipboard.getPrimaryClipDescription().hasMimeType(ClipDescription.MIMETYPE_TEXT_PLAIN);
            if (clip != null && clip.getItemCount() > 0) {
//            return clip.getItemAt(0).coerceToText(FwApplicationUtil.appContext());
                return clip.getItemAt(0).getText().toString().trim();
            }
        }
        return null;
    }

    /**
     * 删除剪贴板的当前文本
     */
    public void removeText() {
        ClipboardManager clipboard = (ClipboardManager) FwApplicationUtil.appContext()
                .getSystemService(Context.CLIPBOARD_SERVICE);
        clipboard.setPrimaryClip(ClipData.newPlainText(null, ""));
    }

    public void removeText(Context context) {
        ClipboardManager clipboard = (ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
        clipboard.setPrimaryClip(ClipData.newPlainText(null, ""));
    }

    /**
     * 复制uri到剪贴板
     *
     * @param uri uri
     */
    public void copyUri(final Uri uri) {
        ClipboardManager clipboard = (ClipboardManager) FwApplicationUtil.appContext()
                .getSystemService(Context.CLIPBOARD_SERVICE);
        clipboard.setPrimaryClip(ClipData.newUri(FwApplicationUtil.appContext()
                .getContentResolver(), "uri", uri));
    }

    /**
     * 获取剪贴板的uri
     *
     * @return 剪贴板的uri
     */
    public Uri getUri() {
        ClipboardManager clipboard = (ClipboardManager) FwApplicationUtil.appContext()
                .getSystemService(Context.CLIPBOARD_SERVICE);
        ClipData clip = clipboard.getPrimaryClip();
        if (clip != null && clip.getItemCount() > 0) {
            return clip.getItemAt(0).getUri();
        }
        return null;
    }

    /**
     * 复制意图到剪贴板
     *
     * @param intent 意图
     */
    public void copyIntent(final Intent intent) {
        ClipboardManager clipboard = (ClipboardManager) FwApplicationUtil.appContext().getSystemService(Context.CLIPBOARD_SERVICE);
        clipboard.setPrimaryClip(ClipData.newIntent("intent", intent));
    }

    /**
     * 获取剪贴板的意图
     *
     * @return 剪贴板的意图
     */
    public Intent getIntent() {
        ClipboardManager clipboard = (ClipboardManager) FwApplicationUtil.appContext().getSystemService(Context.CLIPBOARD_SERVICE);
        ClipData clip = clipboard.getPrimaryClip();
        if (clip != null && clip.getItemCount() > 0) {
            return clip.getItemAt(0).getIntent();
        }
        return null;
    }
}
