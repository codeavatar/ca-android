package com.yuntoyun.fwcore.util;
/*
 * +----------------------------------------------------------------------
 * | @Author: codeavatar   @Year：2021
 * +----------------------------------------------------------------------
 * | @Email: codeavatar@aliyun.com
 * +----------------------------------------------------------------------
 */
import android.text.TextUtils;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Gson工具类
 */
public class FwGsonUtil {

    private static FwGsonUtil init = null;

    public static FwGsonUtil builder() {
        if (null == init) {
            init = new FwGsonUtil();
        }
        return init;
    }

    /**
     * 检测是否为JsonObject类型
     *
     * @param jsonStr
     * @return
     */
    public boolean isJsonObject(String jsonStr) {
        JsonParser jsonParser = new JsonParser();
        JsonElement jsonElement = jsonParser.parse(jsonStr);
        return jsonElement.isJsonObject();
    }

    /**
     * 检测是否为JsonArray类型
     *
     * @param jsonStr
     * @return
     */
    public boolean isJsonArray(String jsonStr) {
        JsonParser jsonParser = new JsonParser();
        JsonElement jsonElement = jsonParser.parse(jsonStr);
        return jsonElement.isJsonArray();
    }

    /**
     * 检测是否为JsonNull类型
     *
     * @param jsonStr
     * @return
     */
    public boolean isJsonNull(String jsonStr) {
        JsonParser jsonParser = new JsonParser();
        JsonElement jsonElement = jsonParser.parse(jsonStr);
        return jsonElement.isJsonNull();
    }

    /**
     * 从JsonArray中获取JsonObject
     *
     * @param array
     * @param index
     * @return
     */
    public JsonObject getJsonObject(JsonArray array, int index) {
        try {
            if (array != null) {
                return array.get(index).getAsJsonObject();
            }
        } catch (Exception e) {
            FwLogUtil.error("[FwGsonUtil]", e);
            return new JsonObject();
        }
        return new JsonObject();
    }

    /**
     * 从JsonArray中获取String字符串
     *
     * @param array
     * @param index
     * @return
     */
    public String getString(JsonArray array, int index) {
        String value = "";
        try {
            if (array != null) {
                return array.get(index).getAsString();
            }
        } catch (Exception e) {
            FwLogUtil.error("[FwGsonUtil]", e);
            return value;
        }
        return value;
    }

    /**
     * 从JsonArray中获取int数据
     *
     * @param array
     * @param index
     * @return
     */
    public int getInt(JsonArray array, int index) {
        int value = -1;
        try {
            if (array != null) {
                return array.get(index).getAsInt();
            }
        } catch (Exception e) {
            FwLogUtil.error("[FwGsonUtil]", e);
            return value;
        }
        return value;
    }

    /**
     * 字符串转换成JsonObject对象
     *
     * @param json
     * @return
     */
    public JsonObject getJsonObject(String json) {
        try {
            if (!TextUtils.isEmpty(json)) {
                return new JsonParser().parse(json).getAsJsonObject();
            } else {
                return new JsonObject();
            }
        } catch (Exception e) {
            FwLogUtil.error("[FwGsonUtil]", e);
            return new JsonObject();
        }

    }

    /**
     * 从JsonObject中获取String值
     *
     * @param jsonObject
     * @param key
     * @return
     */
    public String getString(JsonObject jsonObject, String key) {
        String value = "";
        try {
            value = jsonObject.get(key).getAsString();
        } catch (Exception e) {
            FwLogUtil.error("[FwGsonUtil]", e);
            return value;
        }
        return value.trim();

    }

    /**
     * 从JsonObject中获取int值
     *
     * @param jsonObject
     * @param key
     * @return
     */
    public int getInt(JsonObject jsonObject, String key) {
        int value = -1;
        try {
            value = jsonObject.get(key).getAsInt();
        } catch (Exception e) {
            FwLogUtil.error("[FwGsonUtil]", e);
            return value;
        }
        return value;
    }

    /**
     * 从JsonObject中获取BigDecimal值
     *
     * @param jsonObject
     * @param key
     * @return
     */
    public BigDecimal getBigDecimal(JsonObject jsonObject, String key) {
        BigDecimal bigDecimal;
        try {
            bigDecimal = jsonObject.get(key).getAsBigDecimal();
        } catch (Exception e) {
            FwLogUtil.error("[FwGsonUtil]", e);
            return bigDecimal = new BigDecimal(0);
        }
        return bigDecimal;
    }

    /**
     * 从JsonObject中获取JsonArray对象
     *
     * @param jsonObject
     * @param key
     * @return
     */
    public JsonArray getJsonArray(JsonObject jsonObject, String key) {
        JsonArray array = new JsonArray();
        try {
            array = jsonObject.getAsJsonArray(key);
        } catch (Exception e) {
            FwLogUtil.error("[FwGsonUtil]", e);
            return array;
        }
        return array != null ? array : new JsonArray();

    }

    /**
     * 字符串转换成JsonArray对象
     *
     * @param json
     * @return
     */
    public JsonArray getAsJsonArray(String json) {
        JsonArray array;
        try {
            array = new JsonParser().parse(json).getAsJsonArray();
        } catch (Exception e) {
            FwLogUtil.error("[FwGsonUtil]", e);
            return array = new JsonArray();
        }
        return array;

    }

    /**
     * 从JsonObject中获取JsonObject对象
     *
     * @param jsonObject
     * @param key
     * @return
     */
    public JsonObject getJsonObject(JsonObject jsonObject, String key) {
        JsonObject Object = new JsonObject();
        try {
            Object = jsonObject.getAsJsonObject(key);
        } catch (Exception e) {
            FwLogUtil.error("[FwGsonUtil]", e);
            return Object;
        }
        return Object;

    }

    /**
     * 从JsonObject中获取boolean值
     *
     * @param jsonObject
     * @param key
     * @return
     */
    public boolean getBoolean(JsonObject jsonObject, String key) {
        boolean bl = false;
        try {
            bl = jsonObject.get(key).getAsBoolean();
        } catch (Exception e) {
            FwLogUtil.error("[FwGsonUtil]", e);
            return bl;
        }
        return bl;

    }

    /**
     * 从JsonObject中获取Double值
     *
     * @param jsonObject
     * @param key
     * @return
     */
    public Double getDouble(JsonObject jsonObject, String key) {
        double value = 0;
        try {
            value = jsonObject.get(key).getAsDouble();
        } catch (Exception e) {
            FwLogUtil.error("[FwGsonUtil]", e);
            return value;
        }
        return value;
    }

    /**
     * Remove the element of jsonarray.
     *
     * @param jsonArray
     * @param index
     * @return
     */
    public boolean removeElement(JsonArray jsonArray, int index) {
        if (null == jsonArray || jsonArray.size() < 1)
            return false;

        return jsonArray.remove(jsonArray.get(index));
    }

    /**
     * Remove the key of jsonobject.
     *
     * @param jsonObject
     * @param key
     * @return
     */
    public boolean removeElement(JsonObject jsonObject, String key) {
        if (null == jsonObject)
            return false;

        if (jsonObject.has(key)) {
            jsonObject.remove(key);
        }
        return (!jsonObject.has(key));
    }

    /**
     * Convert JsonElement to map.
     *
     * @param jsonElement
     * @return
     */
    public List<Map<String, Object>> toMapKV(JsonElement jsonElement) {
        List<Map<String, Object>> list = new ArrayList<>();
        if (jsonElement.isJsonObject()) {
            JsonObject jsonO = jsonElement.getAsJsonObject();

            Set<String> setKeys = jsonO.keySet();
            Map<String, Object> map = new HashMap<String, Object>();
            for (String key : setKeys) {
                map.put(key, jsonO.get(key));
            }
            list.add(map);
        } else if (jsonElement.isJsonArray()) {
            JsonArray jsonArr = jsonElement.getAsJsonArray();

            for (int i = 0; i < jsonArr.size(); i++) {
                JsonObject jo = jsonArr.get(i).getAsJsonObject();
                Set<String> setKeys = jo.keySet();
                Map<String, Object> map = new HashMap<String, Object>();
                for (String key : setKeys) {
                    map.put(key, jo.get(key));
                }
                list.add(map);
            }
        }
        return list;
    }


}
