package com.yuntoyun.fwcore.util;
/*
 * +----------------------------------------------------------------------
 * | @Author: codeavatar   @Year：2021
 * +----------------------------------------------------------------------
 * | @Email: codeavatar@aliyun.com
 * +----------------------------------------------------------------------
 */
import android.graphics.Color;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.style.AbsoluteSizeSpan;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;

/**
 * 文本工具类
 */
public class FwTextUtil {

    /**
     * 设置文本字体颜色
     *
     * @param originalStr
     * @param keys
     * @param color
     * @return
     */
    public static SpannableString setKeyFontColor(String originalStr, String[] keys, String color) {
        for (String key : keys) {
            FwLogUtil.info("[setKeyFontColor]", String.format("原始文本：%s,目标文本：%s,字体颜色：%s", originalStr,
                    key, color));
        }
        SpannableString spannableString = new SpannableString(originalStr);
        if (TextUtils.isEmpty(originalStr)) {
            return spannableString;
        }
        for (String key : keys) {
            int start = originalStr.indexOf(key);
            int end = start + key.length();
            FwLogUtil.info("[setKeyFontColor]", String.format("起始下标：%d,结束下标：%d", start, end));
            spannableString.setSpan(new ForegroundColorSpan(Color.parseColor(color)), start, end,
                    Spanned.SPAN_INCLUSIVE_INCLUSIVE);
        }
        return spannableString;
    }

    /**
     * 设置字体大小
     *
     * @param originalStr
     * @param key
     * @param dpSize
     * @param isBold
     * @return
     */
    public static SpannableString setKeyFontSize(String originalStr, String key, int dpSize,
                                                 boolean isBold) {
        FwLogUtil.info("[setKeyFontColor]", String.format("原始文本：%s,目标文本：%s,字体大小：%d", originalStr,
                key, dpSize));

        SpannableString spannableString = new SpannableString(originalStr);
        if (TextUtils.isEmpty(originalStr)) {
            return spannableString;
        }
        int start = originalStr.indexOf(key);
        int end = start + key.length();
        FwLogUtil.info("[setKeyFontColor]", String.format("起始下标：%d,结束下标：%d", start, end));
        spannableString.setSpan(new AbsoluteSizeSpan(dpSize, true), start, end, Spanned
                .SPAN_INCLUSIVE_INCLUSIVE);
        if (isBold) {
            spannableString.setSpan(new StyleSpan(android.graphics.Typeface.BOLD), start, end,
                    Spanned.SPAN_INCLUSIVE_INCLUSIVE);
        }
        return spannableString;
    }
}
