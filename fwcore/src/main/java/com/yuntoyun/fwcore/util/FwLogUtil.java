package com.yuntoyun.fwcore.util;
/*
 * +----------------------------------------------------------------------
 * | @Author: codeavatar   @Year：2021
 * +----------------------------------------------------------------------
 * | @Email: codeavatar@aliyun.com
 * +----------------------------------------------------------------------
 */
import android.app.Activity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;

import com.yuntoyun.fwcore.config.FwAppConfig;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * 日志工具类
 */
public class FwLogUtil {

//    private static final String FILE_SEP       = System.getProperty("file.separator");
//    private static final String LINE_SEP       = System.getProperty("line.separator");
//    private static final String TOP_CORNER     = "┌";
//    private static final String MIDDLE_CORNER  = "├";
//    private static final String LEFT_BORDER    = "│ ";
//    private static final String BOTTOM_CORNER  = "└";
//    private static final String SIDE_DIVIDER   =
//            "────────────────────────────────────────────────────────";
//    private static final String MIDDLE_DIVIDER =
//            "┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄";
//    private static final String TOP_BORDER     = TOP_CORNER + SIDE_DIVIDER + SIDE_DIVIDER;
//    private static final String MIDDLE_BORDER  = MIDDLE_CORNER + MIDDLE_DIVIDER + MIDDLE_DIVIDER;
//    private static final String BOTTOM_BORDER  = BOTTOM_CORNER + SIDE_DIVIDER + SIDE_DIVIDER;

    private static final String PREFIX = ":::";
    private static final String BEGIN_SPLIT_LINE = PREFIX +
            "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++";
    private static final String END_SPLIT_LINE = PREFIX +
            "======================================================================================================";

    private static final int MSG_MAX_LENGTH = (3 * 1024);

    public static void verbose(String tag, String msg) {
        printLog(Log.VERBOSE, tag, msg);
    }

    public static void debug(String tag, String msg) {
        printLog(Log.DEBUG, tag, msg);
    }

    public static void debug(String msg) {
        printLog(Log.DEBUG, "debug", msg);
    }

    public static void info(Activity cls, String msg) {
        printLog(Log.INFO, cls.getClass().getSimpleName(), msg);
    }

    public static void info(String tag, String msg) {
        printLog(Log.INFO, tag, msg);
    }

    public static void info(String msg) {
        printLog(Log.INFO, "info", msg);
    }

    public static void info(Bundle bundle) {
        if (null == bundle) return;

        if (FwAppConfig.PRINT_LOG_SWITCH) {
            StringBuilder sb = new StringBuilder("");
            sb.append(PREFIX + "=================[Bundle 结构输出 开始]=====================");
            sb.append(FwSpanUtil.LINE_SEPARATOR);
            for (String key : bundle.keySet()) {
                if (bundle.get(key) instanceof Serializable) {
                    sb.append(String.format("%s键[%s]=值[%s]", PREFIX, key, bundle.getSerializable(key)));
                } else {
                    sb.append(String.format("%s键[%s]=值[%s]", PREFIX, key, bundle.getString(key)));
                }
                sb.append(FwSpanUtil.LINE_SEPARATOR);
            }
            sb.append(PREFIX + "=================[Bundle 结构输出 结束]=====================");
            printLog2(Log.INFO, "info", sb.toString());
        }
    }

    public static void info(Map<String, Object> map) {
        if (null == map) return;

        if (FwAppConfig.PRINT_LOG_SWITCH) {
            StringBuilder sb = new StringBuilder("");
            sb.append(PREFIX + "=================[Map 结构输出 开始]=====================");
            sb.append(FwSpanUtil.LINE_SEPARATOR);
            for (String key : map.keySet()) {
                sb.append(String.format("%s键[%s]=值[%s]", PREFIX, key, map.get(key).toString()));
                sb.append(FwSpanUtil.LINE_SEPARATOR);
            }
            //迭待循环
//                Iterator ite = map.entrySet().iterator();
//                while (ite.hasNext()) {
//                    Map.Entry entry = (Map.Entry) ite.next();
//                    Object key = entry.getKey();
//                    Object value = entry.getValue();
//                }
            sb.append(PREFIX + "=================[Map 结构输出 结束]=====================");
            printLog2(Log.INFO, "info", sb.toString());
        }
    }

    public static void info(List<String> list) {
        if (null == list) return;

        if (FwAppConfig.PRINT_LOG_SWITCH) {
            StringBuilder sb = new StringBuilder("");
            sb.append(PREFIX + "=================[List<T> 结构输出 开始]=====================");
            sb.append(FwSpanUtil.LINE_SEPARATOR);
            for (String item : list) {
                sb.append(item);
                sb.append(FwSpanUtil.LINE_SEPARATOR);
            }
            sb.append(PREFIX + "=================[List<T> 结构输出 结束]=====================");
            printLog2(Log.INFO, "info", sb.toString());
        }
    }

    public static void warn(String tag, String msg) {
        printLog(Log.WARN, tag, msg);
    }

    public static void warn(String msg) {
        printLog(Log.WARN, "warn", msg);
    }

    public static void error(Activity cls, Exception e) {
        printLog(Log.ERROR, cls.getClass().getSimpleName(), e.getMessage());
        if (FwAppConfig.PRINT_LOG_SWITCH_SYSTEM) {
            e.printStackTrace();
        }
    }

    public static void error(String tag, Exception e) {
        printLog(Log.ERROR, tag, e.getMessage());
        if (FwAppConfig.PRINT_LOG_SWITCH_SYSTEM) {
            e.printStackTrace();
        }
    }

    public static void error(Activity cls, String msg) {
        printLog(Log.ERROR, cls.getClass().getSimpleName(), msg);
    }

    public static void error(String tag, String msg) {
        printLog(Log.ERROR, tag, msg);
    }

    public static void error(String msg) {
        printLog(Log.ERROR, "error", msg);
    }

    public static void asserts(String msg) {
        printLog(Log.ASSERT, "assert", msg);
    }

    public static void asserts(String tag, String msg) {
        printLog(Log.ASSERT, tag, msg);
    }

    /**
     * 统一输出格式
     *
     * @param priority
     * @param tag
     * @param msg
     */
    private static void printLog(int priority, String tag, String msg) {
        if (FwAppConfig.PRINT_LOG_SWITCH) {
            if (TextUtils.isEmpty(msg.trim())) {
                return;
            }

            if (msg.length() < MSG_MAX_LENGTH) {
                StringBuilder sb = new StringBuilder();
                sb.append("　");
                sb.append(FwSpanUtil.LINE_SEPARATOR);
                sb.append(BEGIN_SPLIT_LINE);
                sb.append(FwSpanUtil.LINE_SEPARATOR);
                sb.append(PREFIX + msg);
                sb.append(FwSpanUtil.LINE_SEPARATOR);
                sb.append(END_SPLIT_LINE);
                Log.println(priority, tag, sb.toString());
            } else {
                printLongLog(priority, tag, msg);
            }
        }
    }

    private static void printLog2(int priority, String tag, String msg) {
        if (FwAppConfig.PRINT_LOG_SWITCH) {
            if (TextUtils.isEmpty(msg.trim())) {
                return;
            }

            StringBuilder sb = new StringBuilder();
            sb.append("　");
            sb.append(FwSpanUtil.LINE_SEPARATOR);
            sb.append(BEGIN_SPLIT_LINE);
            sb.append(FwSpanUtil.LINE_SEPARATOR);
            sb.append(msg);
            sb.append(FwSpanUtil.LINE_SEPARATOR);
            sb.append(END_SPLIT_LINE);
            Log.println(priority, tag, sb.toString());
        }
    }

    private static void printLongLog(int priority, String tag, String msg) {
        StringBuffer sbMsg = new StringBuffer(msg);
        int pCounter = 1;
        while (sbMsg.length() > MSG_MAX_LENGTH) {
            StringBuilder sb = new StringBuilder();
            sb.append("　");
            sb.append(FwSpanUtil.LINE_SEPARATOR);
            sb.append(BEGIN_SPLIT_LINE);
            sb.append(FwSpanUtil.LINE_SEPARATOR);
            sb.append(PREFIX + String.format("【%d】》", pCounter) + sbMsg.substring(0, MSG_MAX_LENGTH));
            sb.append(FwSpanUtil.LINE_SEPARATOR);
            sb.append(END_SPLIT_LINE);
            Log.println(priority, tag, sb.toString());
            sbMsg.delete(0, MSG_MAX_LENGTH);
            //计数器
            pCounter++;
        }
        printLog(priority, tag, String.format("【%d】》", pCounter) + sbMsg.toString());
    }
}
