package com.yuntoyun.fwcore.util;
/*
 * +----------------------------------------------------------------------
 * | @Author: codeavatar   @Year：2021
 * +----------------------------------------------------------------------
 * | @Email: codeavatar@aliyun.com
 * +----------------------------------------------------------------------
 */
import android.content.Context;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

/**
 * 控件工具类
 */
public class FwWidgetUtil {

    private static FwWidgetUtil init = null;

    public static FwWidgetUtil builder() {
        if (null == init) {
            init = new FwWidgetUtil();
        }
        return init;
    }

    /**
     * 创建LinearLayout
     *
     * @param context
     * @param layoutParams
     * @param orientation
     * @return
     */
    public LinearLayout createLinearLayout(Context context, ViewGroup.LayoutParams layoutParams, int
            orientation, int gravity) {
        LinearLayout llt = new LinearLayout(context);
        llt.setLayoutParams(layoutParams);
        llt.setOrientation(orientation);
        llt.setGravity(gravity);
        return llt;
    }

    /**
     * 创建TextView
     *
     * @param context      上下文
     * @param layoutParams 布局尺寸
     * @param text         文本内容
     * @param fontSize     sp大小
     * @param gravity      Gravity.CENTER
     * @param background   默认null
     * @param padding
     * @return
     */
    public TextView createTextView(Context context, ViewGroup.LayoutParams layoutParams,
                                   String text, int fontSize, int gravity, Drawable background,
                                   int[] padding) {
        TextView textView = new TextView(context);
        if (null != layoutParams) {
            textView.setLayoutParams(layoutParams);
        } else {
            textView.setLayoutParams(new ViewGroup.LayoutParams(
                    ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        }
        textView.setText(text);
        textView.setTextSize(fontSize);
        if (null != background) {
            textView.setBackground(background);
        }
        textView.setTypeface(null, Typeface.NORMAL);
        if (null != padding) {
            textView.setPadding(padding[0], padding[1], padding[2], padding[3]);
        }
        textView.setGravity(gravity);
        return textView;
    }
}
