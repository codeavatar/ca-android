package com.yuntoyun.fwcore.tool.login;

import android.app.Activity;
import android.os.Bundle;
import android.text.TextUtils;


import androidx.annotation.NonNull;

import com.yuntoyun.fwcore.appext.data.AppData;
import com.yuntoyun.fwcore.appext.data.SPSetUtil;
import com.yuntoyun.fwcore.appext.model.UserModel;
import com.yuntoyun.fwcore.util.FwIntentUtil;

import java.io.Serializable;

/**
 * 权限工具类
 */
public class LoginTool {

    /**
     * 检测登录状态
     *
     * @return
     */
    public static boolean isLogin() {
        UserModel model = AppData.getUserData();
        if (null != model) {
            return (0 != model.getUserId() && (!TextUtils.isEmpty(model.getUserToken())));
        }
        return false;
    }

    /**
     * 检测登录状态
     *
     * @param activity
     * @return
     */
    public static boolean isLogin(@NonNull Activity activity,@NonNull Class<?> cls) {
        if (isLogin()) return true;

        //显示登录界面
        FwIntentUtil.builder().startActivityForResult(activity, cls, -1);
        return false;
    }

    /**
     * 检测登录状态
     *
     * @param activity
     * @param requestCode
     * @return
     */
    public static boolean isLogin(@NonNull Activity activity, @NonNull Class<?> cls , int requestCode) {
        if (isLogin()) return true;
        //显示登录界面
        FwIntentUtil.builder().startActivityForResult(activity, cls, requestCode);
        return false;
    }

    /**
     * 登录后自动回调事件
     *
     * @param activity
     * @param loginCallback
     */
    public static void isLogin(@NonNull Activity activity,@NonNull Class<?> cls, LoginCallback loginCallback) {
        if (isLogin()) {
            loginCallback.callback();
        } else {
            Bundle bundle = new Bundle();
            bundle.putSerializable("ext_callback", loginCallback);
            FwIntentUtil.builder().startActivity(activity, cls, bundle);
        }
    }

    /**
     * 登录回调
     */
    public interface LoginCallback extends Serializable {
        void callback();
    }

    /**
     * 注销登录
     */
    public static void loginOut() {
        SPSetUtil.clearLoginData();
    }
}
