package com.yuntoyun.fwcore.tool.sms;
/*
 * +----------------------------------------------------------------------
 * | @Author: codeavatar   @Year：2021
 * +----------------------------------------------------------------------
 * | @Email: codeavatar@aliyun.com
 * +----------------------------------------------------------------------
 */
public interface SmsToolCallback {

    void initTimer();

    void startTimer(int counter);

    void runTimer(int counter);

    void stopTimer();
}
