package com.yuntoyun.fwcore.plug.tool;
/*
 * +----------------------------------------------------------------------
 * | @Author: codeavatar   @Year：2021
 * +----------------------------------------------------------------------
 * | @Email: codeavatar@aliyun.com
 * +----------------------------------------------------------------------
 */
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.yuntoyun.fwcore.appext.data.FwAppPathData;
import com.yuntoyun.fwcore.plug.qrcode.zxing.ZXingUtils;
import com.yuntoyun.fwcore.util.FwBitmapUtil;
import com.yuntoyun.fwcore.util.FwLogUtil;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class FwQrcodeTool {

    private String filename;


    /////////
    ///属性
    /////////

    private static FwQrcodeTool init = null;

    synchronized public static FwQrcodeTool builder() {
        if (null == init) {
            init = new FwQrcodeTool();
        }
        return init;
    }

    public String getFilename() {
        return filename;
    }

    public FwQrcodeTool setFilename(String filename) {
        this.filename = filename;
        return init;
    }


    //======================================================================

    public void setQrcode(Activity activity, ImageView ivwQrcode, String qrcode, int resId) {

        if (null == activity || null == ivwQrcode) return;

        Bitmap iconBitmap = BitmapFactory.decodeResource(activity.getResources(), resId);
        Bitmap qrcodeBitmap = ZXingUtils.createQRImage(qrcode, ivwQrcode.getWidth(),
                ivwQrcode.getHeight(), iconBitmap);
        ivwQrcode.setImageBitmap(qrcodeBitmap);
    }

    public void setOnlyQrcode(Activity activity, ImageView ivwQrcode, String qrcode) {

        if (null == activity || null == ivwQrcode) return;

        Bitmap qrcodeBitmap = ZXingUtils.createQRImage(qrcode, ivwQrcode.getWidth(),
                ivwQrcode.getHeight());
        ivwQrcode.setImageBitmap(qrcodeBitmap);
    }

    public void saveImageToGallery(Context context, View view) {
        //保存控制效果图
        Bitmap bitmap = FwBitmapUtil.view2Bitmap(view);
        saveImageToGallery(context, bitmap);
    }

    public void saveImageToGallery(Context context, ImageView ivwQrcode) {

        ivwQrcode.setDrawingCacheEnabled(true);
        Bitmap bitmap = Bitmap.createBitmap(ivwQrcode.getDrawingCache());
        ivwQrcode.setDrawingCacheEnabled(false);
        saveImageToGallery(context, bitmap);
    }


    public void saveImageToGallery(Context context, Bitmap bitmap) {

        if (null == bitmap) return;

        // 首先保存图片
        File appDir = new File(FwAppPathData.init().getImgPath(), "qrcode");
        if (!appDir.exists()) {
            appDir.mkdir();
        }
        String fileName = System.currentTimeMillis() + ".jpg";
        if (!TextUtils.isEmpty(getFilename())) {
            fileName = getFilename();
        }

        File file = new File(appDir, fileName);
        try {

            if (!file.exists()) {
                file.createNewFile();
            }

            FileOutputStream fos = new FileOutputStream(file);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fos);
            fos.flush();
            fos.close();
        } catch (FileNotFoundException e) {
            FwLogUtil.error(e.getMessage());
        } catch (IOException e) {
            FwLogUtil.error(e.getMessage());
        }

        // 其次把文件插入到系统图库
        try {
            MediaStore.Images.Media.insertImage(context.getContentResolver(),
                    file.getAbsolutePath(), fileName, null);
        } catch (FileNotFoundException e) {
            FwLogUtil.error(e.getMessage());
        }
        //最后通知图库更新
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);//扫描单个文件
        intent.setData(Uri.fromFile(file));//给图片的绝对路径
        context.sendBroadcast(intent);

        Toast.makeText(context, "已保存到相册！", Toast.LENGTH_LONG).show();
    }
}
