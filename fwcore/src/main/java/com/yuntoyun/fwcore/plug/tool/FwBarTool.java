package com.yuntoyun.fwcore.plug.tool;
/*
 * +----------------------------------------------------------------------
 * | @Author: codeavatar   @Year：2021
 * +----------------------------------------------------------------------
 * | @Email: codeavatar@aliyun.com
 * +----------------------------------------------------------------------
 */
import com.yuntoyun.fwcore.plug.tool.model.FwNavigationModel;
import com.yuntoyun.fwcore.util.FwBarUtil;

import androidx.annotation.ColorInt;
import androidx.fragment.app.FragmentActivity;

/**
 * 状态栏工具类
 */
public class FwBarTool {

    private static FwBarTool init = null;

    public static synchronized FwBarTool builder() {
        if (null == init) {
            init = new FwBarTool();
        }
        return init;
    }

    /**
     * 设置状态栏样式
     *
     * @param activity
     * @param color    (0x000000)
     */
    public void setHeadBarColor(FragmentActivity activity, @ColorInt final int color) {
        this.setHeadBarColor(activity, color, false);
    }

    /**
     * 设置状态栏样式
     *
     * @param activity
     * @param color
     * @param marginTopBar 距状态栏BarHeight高
     */
    public void setHeadBarColor(FragmentActivity activity, @ColorInt final int color,
                                boolean marginTopBar) {
        //状态栏
        FwNavigationModel model = FwNavigationTool.getHeadBarModel(activity);
        if (null != model.getLltBarContainer()) {
            if (marginTopBar) {
                FwBarUtil.addPaddingTopEqualStatusBarHeight(model.getLltBarContainer());
            }
            FwBarUtil.setStatusBarColor(activity, color, 0);
            //白色设置高亮显示
            if (color == 0xFFFFFF) {
                FwBarUtil.setStatusBarLightMode(activity, true);
            }
        }
    }
}
