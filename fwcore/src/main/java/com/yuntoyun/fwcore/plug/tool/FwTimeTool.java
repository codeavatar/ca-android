package com.yuntoyun.fwcore.plug.tool;
/*
 * +----------------------------------------------------------------------
 * | @Author: codeavatar   @Year：2021
 * +----------------------------------------------------------------------
 * | @Email: codeavatar@aliyun.com
 * +----------------------------------------------------------------------
 */
import com.yuntoyun.fwcore.util.FwLogUtil;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 时间工具类
 */
public class FwTimeTool {

    private static final String TAG = "FwTimeTool";

    private static FwTimeTool init = null;

    public static synchronized FwTimeTool builder() {
        if (null == init) {
            init = new FwTimeTool();
        }
        return init;
    }

    /**
     * 获取当前时间(格式：yyyy-MM-dd HH:mm:ss)
     *
     * @return
     */
    public String getCurrentDateTime() {
        Date date = new Date();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return df.format(date);
    }

    /**
     * 获取自定义的时间 (yyyy-MM-dd HH:mm:ss)
     *
     * @param pattern
     * @return
     */
    public String getCurrentDateTime(String pattern) {
        Date date = new Date();
        SimpleDateFormat df = null;
        try {
            df = new SimpleDateFormat(pattern);
        } catch (Exception e) {
            FwLogUtil.error(TAG, e);
        }
        return df.format(date);
    }

    /**
     * 获取当前时间戳
     *
     * @return
     */
    public String getTimestamp() {
        Date date = new Date();
        return String.valueOf(date.getTime());
    }
}
