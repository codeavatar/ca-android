package com.yuntoyun.fwcore.plug.qrcode.zbar;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.util.TypedValue;

import com.yuntoyun.fwcore.R;

public class CustomViewFinderView extends ViewFinderView {
    public static final String TRADE_MARK_TEXT = "ZXing";
    public static final int TRADE_MARK_TEXT_SIZE_SP = 40;
    public final Paint PAINT = new Paint();

    public CustomViewFinderView(Context context) {
        super(context);
        init();
    }

    public CustomViewFinderView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    private void init() {
        setBorderColor(getResources().getColor(R.color.viewfinder_border));
        setLaserColor(getResources().getColor(R.color.viewfinder_laser));
        setLaserEnabled(true);
        setMaskColor(getResources().getColor(R.color.viewfinder_mask));

        PAINT.setColor(Color.WHITE);
        PAINT.setAntiAlias(true);
        float textPixelSize = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP,
                TRADE_MARK_TEXT_SIZE_SP, getResources().getDisplayMetrics());
        PAINT.setTextSize(textPixelSize);
        setSquareViewFinder(true); //正方二维码设置
    }

    @Override
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
//            drawTradeMark(canvas);
    }

    //绘画文本
    private void drawTradeMark(Canvas canvas) {
        Rect framingRect = getFramingRect();
        float tradeMarkTop;
        float tradeMarkLeft;
        if (framingRect != null) {
            tradeMarkTop = framingRect.bottom + PAINT.getTextSize() + 10;
            tradeMarkLeft = framingRect.left;
        } else {
            tradeMarkTop = 10;
            tradeMarkLeft = canvas.getHeight() - PAINT.getTextSize() - 10;
        }
        canvas.drawText(TRADE_MARK_TEXT, tradeMarkLeft, tradeMarkTop, PAINT);
    }
}
