package com.yuntoyun.fwcore.plug.tool;
/*
 * +----------------------------------------------------------------------
 * | @Author: codeavatar   @Year：2021
 * +----------------------------------------------------------------------
 * | @Email: codeavatar@aliyun.com
 * +----------------------------------------------------------------------
 */
import android.content.Context;

import com.yuntoyun.fwcore.R;

/**
 * values组件工具类
 */
public class FwValuesTool {

    private static final String TAG = "FwValuesTool";

    private static FwValuesTool init = null;

    public static synchronized FwValuesTool builder() {
        if (null == init) {
            init = new FwValuesTool();
        }
        return init;
    }

    public int getDimenOneDp(Context context) {
        float f = context.getResources().getDimension(R.dimen.fw_one_dp);
        return (int) f;
    }

    public int getDimenOneSp(Context context) {
        float f = context.getResources().getDimension(R.dimen.fw_one_sp);
        return (int) f;
    }

    public int getDimenVal(Context context, int dimenResId) {
        float f = context.getResources().getDimension(dimenResId);
        return (int) f;
    }

    public String getStringVal(Context context, int stringResId) {
        return context.getResources().getString(stringResId);
    }

    public int getColorVal(Context context, int colorResId) {
        return context.getResources().getColor(colorResId);
    }

}
