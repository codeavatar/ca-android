package com.yuntoyun.fwcore.plug.view.pickerview;
/*
 * +----------------------------------------------------------------------
 * | @Author: codeavatar   @Year：2021
 * +----------------------------------------------------------------------
 * | @Email: codeavatar@aliyun.com
 * +----------------------------------------------------------------------
 */
import android.app.Activity;
import android.graphics.Color;
import android.text.TextUtils;
import android.view.View;

import com.bigkoo.pickerview.builder.OptionsPickerBuilder;
import com.bigkoo.pickerview.builder.TimePickerBuilder;
import com.bigkoo.pickerview.listener.OnOptionsSelectListener;
import com.bigkoo.pickerview.listener.OnTimeSelectChangeListener;
import com.bigkoo.pickerview.listener.OnTimeSelectListener;
import com.bigkoo.pickerview.view.OptionsPickerView;
import com.bigkoo.pickerview.view.TimePickerView;

import java.util.Calendar;
import java.util.Date;

/**
 * PckerView工具类
 */
public class PlugPickerView {


    private static PlugPickerView init = null;

    synchronized public static PlugPickerView builder() {
        if (null == init) {
            init = new PlugPickerView();
        }
        return init;
    }

    /**
     * 初始化TimePickerView组件（变范围限制）
     *
     * @param activity
     * @param title
     * @param startCalendar
     * @param timeType
     * @param timeLength
     * @param listener
     * @return
     */
    public TimePickerView getTimePickerView(Activity activity, String title,
                                            Calendar startCalendar, TimeType timeType,
                                            int timeLength,
                                            OnTimeSelectListener listener) {
        //结束日历
        Calendar endCalendar = null;
        //开始日历
        if (null == startCalendar) {
            startCalendar = Calendar.getInstance();
            endCalendar = Calendar.getInstance();
        } else {
            endCalendar = startCalendar;
        }

        if (0 != timeLength) {
            switch (timeType) {
                case Year:
                    if (timeLength > 0)
                        endCalendar.add(Calendar.YEAR, timeLength);
                    else
                        startCalendar.add(Calendar.YEAR, timeLength);
                    break;
                case Month:
                    if (timeLength > 0)
                        endCalendar.add(Calendar.MONTH, timeLength);
                    else
                        startCalendar.add(Calendar.MONTH, timeLength);
                    break;
                case Day:
                    if (timeLength > 0)
                        endCalendar.add(Calendar.DAY_OF_YEAR, timeLength);
                    else
                        startCalendar.add(Calendar.DAY_OF_YEAR, timeLength);
                    break;
                case Hour:
                    if (timeLength > 0)
                        endCalendar.add(Calendar.HOUR, timeLength);
                    else
                        startCalendar.add(Calendar.HOUR, timeLength);
                    break;
                case Minute:
                    if (timeLength > 0)
                        endCalendar.add(Calendar.MINUTE, timeLength);
                    else
                        startCalendar.add(Calendar.MINUTE, timeLength);
                    break;
                case Second:
                    if (timeLength > 0)
                        endCalendar.add(Calendar.SECOND, timeLength);
                    else
                        startCalendar.add(Calendar.SECOND, timeLength);
                    break;
            }
        }
        return getTimePickerView(activity, title, null, null, startCalendar, endCalendar, listener);
    }


    /**
     * 初始化TimePickerView组件
     *
     * @param activity
     * @param title
     * @param listener
     * @return
     */
    public TimePickerView getTimePickerView(Activity activity, String title,
                                            OnTimeSelectListener listener) {
        return getTimePickerView(activity, title, null, null, null, null, listener);
    }

    /**
     * 初始化TimePickerView组件（有范围限制）
     *
     * @param activity
     * @param title
     * @param startCalendar
     * @param endCalendar
     * @param listener
     * @return
     */
    public TimePickerView getTimePickerView(Activity activity, String title, String oktext,
                                            String cancelText,
                                            Calendar startCalendar, Calendar endCalendar,
                                            OnTimeSelectListener listener) {
        if (TextUtils.isEmpty(oktext)) {
            oktext = "确定";
        }
        if (TextUtils.isEmpty(cancelText)) {
            cancelText = "取消";
        }

        TimePickerView mTimePickerView = null;
        if (null != startCalendar && null != endCalendar) {
            mTimePickerView = new TimePickerBuilder(activity, listener)
                    .setCancelText(cancelText)//取消按钮文字
                    .setSubmitText(oktext)//确认按钮文字
                    .setTitleText(title)//标题文字
                    .setSubCalSize(18)//确定和取消文字大小
                    .setContentTextSize(18)//滚轮文字大小
                    .setTitleSize(20)//标题文字大小
                    .setOutSideCancelable(false)//点击屏幕，点在控件外部范围时，是否取消显示
                    .isCyclic(true)//是否循环滚动
                    .setTitleColor(Color.DKGRAY)//标题文字颜色
                    .setSubmitColor(Color.DKGRAY)//确定按钮文字颜色
                    .setCancelColor(Color.RED)//取消按钮文字颜色
                    .setTextColorCenter(Color.BLACK)
                    .setTextColorOut(Color.LTGRAY)
                    .setTitleBgColor(0xFFEFEFEF)//标题背景颜色 Night mode
                    .setBgColor(0xFFFFFFFF)//滚轮背景颜色 Night mode
                    .setDate(null)// 如果不设置的话，默认是系统时间*/
                    .setLabel("-", "-", "-", ":", ":", "")//默认设置为年月日时分秒
                    .setType(new boolean[]{true, true, true, true, true, true})// 默认全部显示
                    .setRangDate(startCalendar, endCalendar)//起始终止年月日设定
                    .isCenterLabel(false) //是否只显示中间选中项的label文字，false则每项item全部都带有label。
                    .isDialog(false)//是否显示为对话框样式
                    .build();
        } else {
            mTimePickerView = new TimePickerBuilder(activity, listener)
                    .setCancelText(cancelText)//取消按钮文字
                    .setSubmitText(oktext)//确认按钮文字
                    .setTitleText(title)//标题文字
                    .setSubCalSize(18)//确定和取消文字大小
                    .setContentTextSize(18)//滚轮文字大小
                    .setTitleSize(20)//标题文字大小
                    .setOutSideCancelable(false)//点击屏幕，点在控件外部范围时，是否取消显示
                    .isCyclic(true)//是否循环滚动
                    .setTitleColor(Color.DKGRAY)//标题文字颜色
                    .setSubmitColor(Color.DKGRAY)//确定按钮文字颜色
                    .setCancelColor(Color.RED)//取消按钮文字颜色
                    .setTextColorCenter(Color.BLACK)
                    .setTextColorOut(Color.LTGRAY)
                    .setTitleBgColor(0xFFEFEFEF)//标题背景颜色 Night mode
                    .setBgColor(0xFFFFFFFF)//滚轮背景颜色 Night mode
                    .setDate(null)// 如果不设置的话，默认是系统时间*/
                    .setLabel("-", "-", "-", ":", ":", "")//默认设置为年月日时分秒
                    .setType(new boolean[]{true, true, true, true, true, true})// 默认全部显示
//                .setRangDate(startCalendar, endCalendar)//起始终止年月日设定
                    .isCenterLabel(false) //是否只显示中间选中项的label文字，false则每项item全部都带有label。
                    .isDialog(false)//是否显示为对话框样式
                    .build();
        }
        return mTimePickerView;
    }

    /**
     * 初始化OptionsPickerView组件
     *
     * @param activity
     * @param title
     * @param listener
     * @return
     */
    public OptionsPickerView getOptionsPickerView(Activity activity, String title,
                                                  OnOptionsSelectListener listener) {

        OptionsPickerView mOptionsPickerView =
                new OptionsPickerBuilder(activity, listener).setSubmitText("确定")//确定按钮文字
                        .setCancelText("取消")//取消按钮文字
                        .setTitleText(title)//标题
                        .setSubCalSize(18)//确定和取消文字大小
                        .setTitleSize(20)//标题文字大小
                        .setTextColorCenter(Color.BLACK)
                        .setTextColorOut(Color.LTGRAY)
                        .setTitleColor(Color.DKGRAY)//标题文字颜色
                        .setSubmitColor(Color.DKGRAY)//确定按钮文字颜色
                        .setCancelColor(Color.RED)//取消按钮文字颜色
                        .setTitleBgColor(0xFFEFEFEF)//标题背景颜色 Night mode
                        .setBgColor(0xFFFFFFFF)//滚轮背景颜色 Night mode
                        .setContentTextSize(18)//滚轮文字大小
                        .setLabels(" ", " ", " ")//设置选择的三级单位
                        .isCenterLabel(false) //是否只显示中间选中项的label文字，false则每项item全部都带有label。
                        .setCyclic(false, false, false)//循环与否
                        .setSelectOptions(0, 0, 0)  //设置默认选中项
                        .setOutSideCancelable(false)//点击外部dismiss default true
                        .isDialog(false)//是否显示为对话框样式
                        .isRestoreItem(false)//切换时是否还原，设置默认选中第一项。
                        .build();
        return mOptionsPickerView;
    }

    /**
     * 初始化OptionsPickerBuilder实例
     *
     * @param activity
     * @param title
     * @param listener
     * @return
     */
    public OptionsPickerBuilder getOptionsPickerBuilder(Activity activity, String title,
                                                        OnOptionsSelectListener listener) {
        return new OptionsPickerBuilder(activity, listener).setSubmitText("确定")//确定按钮文字
                .setCancelText("取消")//取消按钮文字
                .setTitleText(title)//标题
                .setSubCalSize(18)//确定和取消文字大小
                .setTitleSize(20)//标题文字大小
                .setTextColorCenter(Color.BLACK)
                .setTextColorOut(Color.LTGRAY)
                .setTitleColor(Color.DKGRAY)//标题文字颜色
                .setSubmitColor(Color.DKGRAY)//确定按钮文字颜色
                .setCancelColor(Color.RED)//取消按钮文字颜色
                .setTitleBgColor(0xFFEFEFEF)//标题背景颜色 Night mode
                .setBgColor(0xFFFFFFFF)//滚轮背景颜色 Night mode
                .setContentTextSize(18)//滚轮文字大小
                .setLabels(" ", " ", " ")//设置选择的三级单位
                .isCenterLabel(false) //是否只显示中间选中项的label文字，false则每项item全部都带有label。
                .setCyclic(false, false, false)//循环与否
                .setSelectOptions(0, 0, 0)  //设置默认选中项
                .setOutSideCancelable(false)//点击外部dismiss default true
                .isDialog(false)//是否显示为对话框样式
                .isRestoreItem(false);//切换时是否还原，设置默认选中第一项。
    }

    /**
     * 初始化TimePickerBuilder实例
     *
     * @param activity
     * @param title
     * @param listener
     * @return
     */
    public TimePickerBuilder getTimePickerBuilder(Activity activity, String title,
                                                  OnTimeSelectListener listener) {
        return new TimePickerBuilder(activity, listener)
                .setCancelText("取消")//取消按钮文字
                .setSubmitText("确定")//确认按钮文字
                .setTitleText(title)//标题文字
                .setSubCalSize(18)//确定和取消文字大小
                .setContentTextSize(18)//滚轮文字大小
                .setTitleSize(20)//标题文字大小
                .setOutSideCancelable(false)//点击屏幕，点在控件外部范围时，是否取消显示
                .isCyclic(true)//是否循环滚动
                .setTitleColor(Color.DKGRAY)//标题文字颜色
                .setSubmitColor(Color.DKGRAY)//确定按钮文字颜色
                .setCancelColor(Color.RED)//取消按钮文字颜色
                .setTextColorCenter(Color.BLACK)
                .setTextColorOut(Color.LTGRAY)
                .setTitleBgColor(0xFFEFEFEF)//标题背景颜色 Night mode
                .setBgColor(0xFFFFFFFF)//滚轮背景颜色 Night mode
                .setDate(null)// 如果不设置的话，默认是系统时间*/
                .setLabel("-", "-", "-", ":", ":", "")//默认设置为年月日时分秒
                .setType(new boolean[]{true, true, true, true, true, true})// 默认全部显示
//                .setRangDate(startCalendar, endCalendar)//起始终止年月日设定
                .isCenterLabel(false) //是否只显示中间选中项的label文字，false则每项item全部都带有label。
                .isDialog(false);//是否显示为对话框样式
    }

    /**
     * 时间类型 枚举
     */
    public enum TimeType {
        Year, Month, Day, Hour, Minute, Second
    }
}
