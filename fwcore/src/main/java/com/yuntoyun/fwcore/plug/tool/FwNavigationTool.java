package com.yuntoyun.fwcore.plug.tool;
/*
 * +----------------------------------------------------------------------
 * | @Author: codeavatar   @Year：2021
 * +----------------------------------------------------------------------
 * | @Email: codeavatar@aliyun.com
 * +----------------------------------------------------------------------
 */
import android.app.Activity;
import android.graphics.Color;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.yuntoyun.fwcore.R;
import com.yuntoyun.fwcore.plug.tool.model.FwNavigationModel;

/**
 * 导航工具类
 */
public class FwNavigationTool {

    /////////////////
    ///读取导航模型
    /////////////////


    public static FwNavigationModel getHeadBarModel(final Activity activity) {
        FwNavigationModel model = new FwNavigationModel();
        LinearLayout llt_head_bar_container = activity.findViewById(R.id.llt_head_bar_container);
        ImageView ivw_left_action = activity.findViewById(R.id.ivw_left_action);
        TextView tvw_center_title = activity.findViewById(R.id.tvw_center_title);
        ImageView ivw_right_action = activity.findViewById(R.id.ivw_right_action);
        TextView tvw_right_action = activity.findViewById(R.id.tvw_right_action);
        model.setLltBarContainer(llt_head_bar_container);
        model.setIvwLeftAction(ivw_left_action);
        model.setTvwCenterTitle(tvw_center_title);
        model.setIvwRightAction(ivw_right_action);
        model.setTvwRightAction(tvw_right_action);
        return model;
    }

    public static FwNavigationModel getHeadBarModel(final View fragmentView) {
        FwNavigationModel model = new FwNavigationModel();
        LinearLayout llt_head_bar_container =
                fragmentView.findViewById(R.id.llt_head_bar_container);
        ImageView ivw_left_action = fragmentView.findViewById(R.id.ivw_left_action);
        TextView tvw_center_title = fragmentView.findViewById(R.id.tvw_center_title);
        ImageView ivw_right_action = fragmentView.findViewById(R.id.ivw_right_action);
        TextView tvw_right_action = fragmentView.findViewById(R.id.tvw_right_action);
        model.setLltBarContainer(llt_head_bar_container);
        model.setIvwLeftAction(ivw_left_action);
        model.setTvwCenterTitle(tvw_center_title);
        model.setIvwRightAction(ivw_right_action);
        model.setTvwRightAction(tvw_right_action);
        return model;
    }


    /////////////////
    ///设置普通导航
    /////////////////

    /**
     * 设置public_header_bar的标题
     *
     * @param activity
     * @param title
     */
    public static void setHeadBar(final Activity activity, String title) {
        //左侧事件
        ImageView leftAction = (ImageView) activity
                .findViewById(R.id.ivw_left_action);
        leftAction.setImageResource(R.mipmap.fw_icon_action_goback);
        leftAction.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.finish();
            }
        });
        //中间标题
        TextView tvw_center_title = (TextView) activity
                .findViewById(R.id.tvw_center_title);
        tvw_center_title.setText(title);
        //右侧事件
        TextView tvw_right_action = (TextView) activity
                .findViewById(R.id.tvw_right_action);
        tvw_right_action.setVisibility(View.GONE);
        ImageView ivw_right_action = (ImageView) activity
                .findViewById(R.id.ivw_right_action);
        ivw_right_action.setVisibility(View.INVISIBLE);
    }

    /**
     * 设置public_header_bar的标题
     *
     * @param activity
     * @param title
     * @param backResId
     */
    public static void setHeadBar(final Activity activity, String title, int backResId) {
        //左侧事件
        ImageView leftAction = (ImageView) activity.findViewById(R.id.ivw_left_action);
        leftAction.setImageResource((0 == backResId) ? R.mipmap.fw_icon_action_goback : backResId);
        leftAction.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.finish();
            }
        });
        //中间标题
        TextView tvw_center_title = (TextView) activity
                .findViewById(R.id.tvw_center_title);
        tvw_center_title.setText(title);
        //右侧事件
        TextView tvw_right_action = (TextView) activity
                .findViewById(R.id.tvw_right_action);
        tvw_right_action.setVisibility(View.GONE);
        ImageView ivw_right_action = (ImageView) activity
                .findViewById(R.id.ivw_right_action);
        ivw_right_action.setVisibility(View.INVISIBLE);
    }

    /**
     * 设置public_header_bar的标题
     *
     * @param activity
     * @param title
     */
    public static void setHeadBar(final Activity activity, String title, boolean showNavigation,
                                  int backResId) {
        if (!showNavigation) {
            TextView tvw_center_title = (TextView) activity.findViewById(R.id.tvw_center_title);
            if (null != tvw_center_title) {
                tvw_center_title.setVisibility(View.GONE);
            }
            View v = activity.findViewById(backResId);
            v.setVisibility(View.VISIBLE);
            v.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    activity.finish();
                }
            });
            return;
        }

        setHeadBar(activity, title);
    }

    /**
     * 设置public_header_bar的标题
     *
     * @param activity
     * @param title
     * @param rightText
     * @param rightClickListener
     */
    public static void setHeadBar(final Activity activity, String title, String rightText,
                                  OnClickListener rightClickListener) {

        //左侧事件
        ImageView leftAction = (ImageView) activity
                .findViewById(R.id.ivw_left_action);
        leftAction.setImageResource(getBackImageResId(BackImgType.White));
        leftAction.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.finish();
            }
        });
        //中间标题
        TextView tvw_center_title = (TextView) activity
                .findViewById(R.id.tvw_center_title);
        tvw_center_title.setText(title);
        //右侧事件
        TextView tvw_right_action = (TextView) activity
                .findViewById(R.id.tvw_right_action);
        ImageView ivw_right_action = (ImageView) activity
                .findViewById(R.id.ivw_right_action);
        if ((!TextUtils.isEmpty(rightText)) && rightText.length() > 0) {
            tvw_right_action.setText(rightText);
            tvw_right_action.setOnClickListener(rightClickListener);
            tvw_right_action.setVisibility(View.VISIBLE);
            ivw_right_action.setVisibility(View.GONE);
        } else {
            tvw_right_action.setVisibility(View.GONE);
            ivw_right_action.setVisibility(View.INVISIBLE);
        }
    }

    /**
     * 设置public_header_bar的标题
     *
     * @param activity
     * @param title
     * @param rightImg
     * @param rightClickListener
     */
    public static void setHeadBar(final Activity activity, String title, int rightImg,
                                  OnClickListener rightClickListener) {
        //左侧事件
        ImageView leftAction = (ImageView) activity
                .findViewById(R.id.ivw_left_action);
        leftAction.setImageResource(getBackImageResId(BackImgType.White));
        leftAction.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.finish();
            }
        });
        //中间标题
        TextView tvw_center_title = (TextView) activity
                .findViewById(R.id.tvw_center_title);
        tvw_center_title.setText(title);
        //右侧事件
        TextView tvw_right_action = (TextView) activity
                .findViewById(R.id.tvw_right_action);
        ImageView ivw_right_action = (ImageView) activity
                .findViewById(R.id.ivw_right_action);
        if (rightImg > 0) {
            tvw_right_action.setVisibility(View.GONE);
            ivw_right_action.setImageResource(rightImg);
            ivw_right_action.setOnClickListener(rightClickListener);
            ivw_right_action.setVisibility(View.VISIBLE);
        } else {
            tvw_right_action.setVisibility(View.GONE);
            ivw_right_action.setVisibility(View.INVISIBLE);
        }
    }


    public static void setHeadBar(final Activity activity, String title, View fragmentView,
                                  BackImgType imgType) {
        //左侧事件
        ImageView leftAction = (ImageView) fragmentView
                .findViewById(R.id.ivw_left_action);
        leftAction.setImageResource(getBackImageResId(imgType));
        leftAction.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.finish();
            }
        });
        //中间标题
        TextView tvw_center_title = (TextView) fragmentView
                .findViewById(R.id.tvw_center_title);
        tvw_center_title.setTextColor(getTextColor(imgType));
        tvw_center_title.setText(title);
        //右侧事件
        TextView tvw_right_action = (TextView) fragmentView
                .findViewById(R.id.tvw_right_action);
        tvw_right_action.setVisibility(View.GONE);
        ImageView ivw_right_action = (ImageView) fragmentView
                .findViewById(R.id.ivw_right_action);
        ivw_right_action.setVisibility(View.INVISIBLE);
    }

    /////////////////
    ///设置无返回导航
    /////////////////

    public static void setHeadBarNoBack(Activity activity, String title) {
        //中间标题
        TextView tvw_center_title = (TextView) activity
                .findViewById(R.id.tvw_center_title);
        tvw_center_title.setText(title);
        //右侧事件
        TextView tvw_right_action = (TextView) activity
                .findViewById(R.id.tvw_right_action);
        tvw_right_action.setVisibility(View.GONE);
        ImageView ivw_right_action = (ImageView) activity
                .findViewById(R.id.ivw_right_action);
        ivw_right_action.setVisibility(View.INVISIBLE);
    }

    public static void setHeadBarNoBack(View fragmentView, String title) {
        //中间标题
        TextView tvw_center_title = (TextView) fragmentView
                .findViewById(R.id.tvw_center_title);
        tvw_center_title.setText(title);
        //右侧事件
        TextView tvw_right_action = (TextView) fragmentView
                .findViewById(R.id.tvw_right_action);
        tvw_right_action.setVisibility(View.GONE);
        ImageView ivw_right_action = (ImageView) fragmentView
                .findViewById(R.id.ivw_right_action);
        ivw_right_action.setVisibility(View.INVISIBLE);
    }

    /////////////////
    ///设置可变背景色导航
    /////////////////

    public static void setHeadBarBgcolor(final Activity activity, String title, String bgcolor,
                                         BackImgType imgType) {
        LinearLayout llt_head_bar_container = activity.findViewById(R.id.llt_head_bar_container);
        if (null != llt_head_bar_container) {
            llt_head_bar_container.setBackgroundColor(Color.parseColor(bgcolor));

            //左侧事件
            ImageView leftAction = (ImageView) activity
                    .findViewById(R.id.ivw_left_action);
            leftAction.setImageResource(getBackImageResId(imgType));
            leftAction.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    activity.finish();
                }
            });
            //中间标题
            TextView tvw_center_title = (TextView) activity
                    .findViewById(R.id.tvw_center_title);
            tvw_center_title.setText(title);
            tvw_center_title.setTextColor(getTextColor(imgType));
            //右侧事件
            TextView tvw_right_action = (TextView) activity
                    .findViewById(R.id.tvw_right_action);
            tvw_right_action.setTextColor(getTextColor(imgType));
            tvw_right_action.setVisibility(View.GONE);
            ImageView ivw_right_action = (ImageView) activity
                    .findViewById(R.id.ivw_right_action);
            ivw_right_action.setVisibility(View.INVISIBLE);
        }
    }

    public static void setHeadBarBgcolor(final Activity activity, String title, String bgcolor,
                                         BackImgType imgType, int rightImg,
                                         OnClickListener rightClickListener) {
        LinearLayout llt_head_bar_container = activity.findViewById(R.id.llt_head_bar_container);
        if (null != llt_head_bar_container) {
            llt_head_bar_container.setBackgroundColor(Color.parseColor(bgcolor));

            //左侧事件
            ImageView leftAction = (ImageView) activity
                    .findViewById(R.id.ivw_left_action);
            leftAction.setImageResource(getBackImageResId(imgType));
            leftAction.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    activity.finish();
                }
            });
            //中间标题
            TextView tvw_center_title = (TextView) activity
                    .findViewById(R.id.tvw_center_title);
            tvw_center_title.setText(title);
            tvw_center_title.setTextColor(getTextColor(imgType));
            //右侧事件
            TextView tvw_right_action = (TextView) activity
                    .findViewById(R.id.tvw_right_action);
            ImageView ivw_right_action = (ImageView) activity
                    .findViewById(R.id.ivw_right_action);
            if (rightImg > 0) {
                tvw_right_action.setVisibility(View.GONE);
                ivw_right_action.setImageResource(rightImg);
                ivw_right_action.setOnClickListener(rightClickListener);
                ivw_right_action.setVisibility(View.VISIBLE);
            } else {
                tvw_right_action.setVisibility(View.GONE);
                ivw_right_action.setVisibility(View.INVISIBLE);
            }

        }
    }

    public static void setHeadBarBgcolor(final Activity activity, String title, String bgcolor,
                                         BackImgType imgType, String rightText,
                                         OnClickListener rightClickListener) {
        LinearLayout llt_head_bar_container = activity.findViewById(R.id.llt_head_bar_container);
        if (null != llt_head_bar_container) {
            llt_head_bar_container.setBackgroundColor(Color.parseColor(bgcolor));

            //左侧事件
            ImageView leftAction = (ImageView) activity
                    .findViewById(R.id.ivw_left_action);
            leftAction.setImageResource(getBackImageResId(imgType));
            leftAction.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    activity.finish();
                }
            });
            //中间标题
            TextView tvw_center_title = (TextView) activity
                    .findViewById(R.id.tvw_center_title);
            tvw_center_title.setText(title);
            tvw_center_title.setTextColor(getTextColor(imgType));
            //右侧事件
            TextView tvw_right_action = (TextView) activity
                    .findViewById(R.id.tvw_right_action);
            ImageView ivw_right_action = (ImageView) activity
                    .findViewById(R.id.ivw_right_action);
            if (!TextUtils.isEmpty(rightText)) {
                tvw_right_action.setText(rightText);
                tvw_right_action.setTextColor(getTextColor(imgType));
                tvw_right_action.setVisibility(View.VISIBLE);
                tvw_right_action.setOnClickListener(rightClickListener);

                ivw_right_action.setVisibility(View.GONE);
            } else {
                tvw_right_action.setVisibility(View.GONE);
                ivw_right_action.setVisibility(View.INVISIBLE);
            }

        }
    }

    public static void setHeadBarBgcolor(final Activity activity, View fragmentView,
                                         String bgcolor, BackImgType imgType,
                                         String title) {
        LinearLayout llt_head_bar_container =
                fragmentView.findViewById(R.id.llt_head_bar_container);
        if (null != llt_head_bar_container) {
            llt_head_bar_container.setBackgroundColor(Color.parseColor(bgcolor));

            //左侧事件
            ImageView leftAction = (ImageView) fragmentView
                    .findViewById(R.id.ivw_left_action);
            leftAction.setImageResource(getBackImageResId(imgType));
            leftAction.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    activity.finish();
                }
            });
            //中间标题
            TextView tvw_center_title = (TextView) fragmentView
                    .findViewById(R.id.tvw_center_title);
            tvw_center_title.setText(title);
            tvw_center_title.setTextColor(getTextColor(imgType));
            //右侧事件
            TextView tvw_right_action = (TextView) fragmentView
                    .findViewById(R.id.tvw_right_action);
            tvw_right_action.setTextColor(getTextColor(imgType));
            tvw_right_action.setVisibility(View.GONE);
            ImageView ivw_right_action = (ImageView) fragmentView
                    .findViewById(R.id.ivw_right_action);
            ivw_right_action.setVisibility(View.INVISIBLE);
        }
    }

    //返回图片类型
    public enum BackImgType {
        Default, None, White, Black
    }

    //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    private static int getBackImageResId(BackImgType type) {
        int resId = -1;
        switch (type) {
            case White:
                resId = R.mipmap.fw_icon_action_goback;
                break;
            case Black:
                resId = R.mipmap.fw_icon_action_goback2;
                break;
            case None:
                resId = 0;
                break;
            case Default:
            default:
                resId = R.mipmap.fw_icon_action_goback;
                break;
        }
        return resId;
    }

    private static int getTextColor(BackImgType type) {
        StringBuffer sbColor = new StringBuffer("");
        switch (type) {
            case White:
                sbColor.append("#FFFFFF");
                break;
            case Black:
                sbColor.append("#000000");
                break;
            case None:
                sbColor.append("#00FFFFFF");
                break;
            case Default:
            default:
                sbColor.append("#FFFFFF");
                break;
        }
        return Color.parseColor(sbColor.toString());
    }

}
