package com.yuntoyun.fwcore.plug.network.retrofit;
/*
 * +----------------------------------------------------------------------
 * | @Author: codeavatar   @Year：2021
 * +----------------------------------------------------------------------
 * | @Email: codeavatar@aliyun.com
 * +----------------------------------------------------------------------
 */
import io.reactivex.observers.DisposableObserver;

/**
 * Rx管理标准
 */
public interface FwRxActionManager<T> {
    void add(T tag, DisposableObserver subscription);
    void remove(T tag);
    void cancel(T tag);
    void cancelAll();
    void cancelAll(T tag);
}
