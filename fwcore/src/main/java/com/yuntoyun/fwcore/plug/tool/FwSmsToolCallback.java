package com.yuntoyun.fwcore.plug.tool;
/*
 * +----------------------------------------------------------------------
 * | @Author: codeavatar   @Year：2021
 * +----------------------------------------------------------------------
 * | @Email: codeavatar@aliyun.com
 * +----------------------------------------------------------------------
 */
public interface FwSmsToolCallback {

    void initTimer();

    void startTimer(int counter);

    void runTimer(int counter);

    void stopTimer();
}
