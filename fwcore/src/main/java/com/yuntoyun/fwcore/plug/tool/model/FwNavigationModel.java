package com.yuntoyun.fwcore.plug.tool.model;
/*
 * +----------------------------------------------------------------------
 * | @Author: codeavatar   @Year：2021
 * +----------------------------------------------------------------------
 * | @Email: codeavatar@aliyun.com
 * +----------------------------------------------------------------------
 */
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

/**
 * 顶部导航栏模型
 */
public class FwNavigationModel {

    private LinearLayout lltBarContainer;
    private ImageView ivwLeftAction;
    private TextView tvwCenterTitle;
    private TextView tvwRightAction;
    private ImageView ivwRightAction;

    public LinearLayout getLltBarContainer() {
        return lltBarContainer;
    }

    public void setLltBarContainer(LinearLayout lltBarContainer) {
        this.lltBarContainer = lltBarContainer;
    }

    public ImageView getIvwLeftAction() {
        return ivwLeftAction;
    }

    public void setIvwLeftAction(ImageView ivwLeftAction) {
        this.ivwLeftAction = ivwLeftAction;
    }

    public TextView getTvwCenterTitle() {
        return tvwCenterTitle;
    }

    public void setTvwCenterTitle(TextView tvwCenterTitle) {
        this.tvwCenterTitle = tvwCenterTitle;
    }

    public TextView getTvwRightAction() {
        return tvwRightAction;
    }

    public void setTvwRightAction(TextView tvwRightAction) {
        this.tvwRightAction = tvwRightAction;
    }

    public ImageView getIvwRightAction() {
        return ivwRightAction;
    }

    public void setIvwRightAction(ImageView ivwRightAction) {
        this.ivwRightAction = ivwRightAction;
    }
}
