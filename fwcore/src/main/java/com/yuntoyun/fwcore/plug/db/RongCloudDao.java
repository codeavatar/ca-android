package com.yuntoyun.fwcore.plug.db;
/*
 * +----------------------------------------------------------------------
 * | @Author: codeavatar   @Year：2021
 * +----------------------------------------------------------------------
 * | @Email: codeavatar@aliyun.com
 * +----------------------------------------------------------------------
 */
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import com.yuntoyun.fwcore.plug.db.Model.RongCloudModel;

import java.util.ArrayList;
import java.util.List;

public class RongCloudDao extends BaseSQLiteDao {

    private final String currentTableName = "rongclouduser";

    private final int refreshMaxValue = 100; //每50次调用，该信息更新一次；

    public RongCloudDao(Context context) {
        super(context, "rongclouduser");
    }

    public List<RongCloudModel> getListAll() {
        List<RongCloudModel> list = new ArrayList<>();
        String sql = String.format("SELECT * FROM %s", currentTableName);
        Cursor cursor = super.select(sql, null);
        RongCloudModel.parseModel(cursor, list);
        if (!cursor.isClosed()) {
            cursor.close();
        }
        if (super.db.isOpen()) {
            super.db.close();
        }
        return list;
    }

    public RongCloudModel getModel(int id) {
        String sql = String.format("SELECT * FROM %s WHERE id=?", currentTableName);
        Cursor cursor = super.select(sql, new String[]{String.valueOf(id)});
        RongCloudModel model = RongCloudModel.parseModel(cursor);
        if (!cursor.isClosed()) {
            cursor.close();
        }
        if (super.db.isOpen()) {
            super.db.close();
        }

        //刷新计数器
        if (null != model) {
            this.incRefreshCounter(model.getId());
        }

        return model;
    }

    public RongCloudModel getModel(int id, boolean checkRefreshCounter) {
        RongCloudModel model = getModel(id);

        if (checkRefreshCounter) {
            if (null != model) {
                if (refreshMaxValue <= model.getRefreshCounter()) {
                    delModel(id);
                    return null;
                }
            }
        }

        return model;
    }

    public void setModel(RongCloudModel model) {
        ContentValues values = new ContentValues();
        values.put("id", model.getId());
        values.put("nickname", model.getNickname());
        values.put("icon", model.getIcon());
        values.put("refreshCounter", 0);
        super.insert(values);
    }

    public void delModel(int id) {
        super.delete(id);
    }

    public void delModelAll() {
        String sql = String.format("DELETE FROM %s", currentTableName);
        super.delete(sql);
    }

    public void updateModel(ContentValues values, String whereClause, String[] whereArgs) {
        super.update(currentTableName, values, whereClause, whereArgs);
    }

    public void incRefreshCounter(int id) {
        String sql = String.format("UPDATE %s SET refreshCounter=refreshCounter+1 WHERE id=%d",
                currentTableName, id);
        super.update(sql);
    }
}
