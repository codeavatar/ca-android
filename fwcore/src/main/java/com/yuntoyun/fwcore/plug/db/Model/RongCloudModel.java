package com.yuntoyun.fwcore.plug.db.Model;
/*
 * +----------------------------------------------------------------------
 * | @Author: codeavatar   @Year：2021
 * +----------------------------------------------------------------------
 * | @Email: codeavatar@aliyun.com
 * +----------------------------------------------------------------------
 */
import android.database.Cursor;

import java.util.List;

/**
 * 融云用户模型
 */
public class RongCloudModel {

    private int id;
    private String nickname;
    private String icon;
    private int refreshCounter;

    public RongCloudModel() {
    }

    public RongCloudModel(int id, String nickname, String icon) {
        this.id = id;
        this.nickname = nickname;
        this.icon = icon;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public int getRefreshCounter() {
        return refreshCounter;
    }

    public void setRefreshCounter(int refreshCounter) {
        this.refreshCounter = refreshCounter;
    }

    public static RongCloudModel parseModel(Cursor cursor) {
        RongCloudModel model = null;
        if (cursor.getCount() > 0) {
            model = new RongCloudModel();
            cursor.moveToNext();
            model.setId(cursor.getInt(0));
            model.setNickname(cursor.getString(1));
            model.setIcon(cursor.getString(2));
            model.setRefreshCounter(cursor.getInt(3));
        }
        return model;
    }

    public static void parseModel(Cursor cursor, List<RongCloudModel> list) {
        if (cursor.getCount() > 0) {
            while (cursor.moveToNext()) {
                RongCloudModel model = new RongCloudModel();
                model.setId(cursor.getInt(0));
                model.setNickname(cursor.getString(1));
                model.setIcon(cursor.getString(2));
                model.setRefreshCounter(cursor.getInt(3));
                list.add(model);
            }
        }

    }
}
