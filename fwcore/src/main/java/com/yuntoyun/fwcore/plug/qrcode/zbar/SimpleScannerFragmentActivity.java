package com.yuntoyun.fwcore.plug.qrcode.zbar;

import android.os.Bundle;

import com.yuntoyun.fwcore.R;

public class SimpleScannerFragmentActivity extends BaseScannerActivity {
    @Override
    public void onCreate(Bundle state) {
        super.onCreate(state);
        setContentView(R.layout.fw_activity_zbar_scanner_fragment);
        setupToolbar();
    }
}