package com.yuntoyun.fwcore.plug.tool;
/*
 * +----------------------------------------------------------------------
 * | @Author: codeavatar   @Year：2021
 * +----------------------------------------------------------------------
 * | @Email: codeavatar@aliyun.com
 * +----------------------------------------------------------------------
 */
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 正则工具类
 */
public class FwRegexTool {

    private static FwRegexTool instance = null;


    public static FwRegexTool builder() {
        if (null == instance) {
            instance = new FwRegexTool();
        }
        return instance;
    }

    public boolean isChinese(String chinese) {
        return regex(chinese, "^[\u4e00-\u9fa5]$");
    }

    public boolean isCellPhone(String cellphone) {
        return regex(cellphone, "^1\\d{10}$");
    }

    public boolean isNumeric(String numeric) {
        return regex(numeric, "^[0-9]*$");
    }

    public boolean isValidTagAndAlias(String s) {
        Pattern p = Pattern.compile("^[\u4E00-\u9FA50-9a-zA-Z_!@#$&*+=.|￥¥]+$");
        Matcher m = p.matcher(s);
        return m.matches();
    }

    private boolean regex(String target, String pattern) {
        Pattern p = Pattern.compile(pattern);
        Matcher m = p.matcher(target);
        return m.matches();
    }
}
