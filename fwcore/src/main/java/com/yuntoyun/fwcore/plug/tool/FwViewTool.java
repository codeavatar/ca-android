package com.yuntoyun.fwcore.plug.tool;
/*
 * +----------------------------------------------------------------------
 * | @Author: codeavatar   @Year：2021
 * +----------------------------------------------------------------------
 * | @Email: codeavatar@aliyun.com
 * +----------------------------------------------------------------------
 */
import android.content.Context;
import android.graphics.Paint;
import android.text.InputType;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.LeadingMarginSpan;
import android.widget.EditText;
import android.widget.TextView;

import com.yuntoyun.fwcore.util.FwSizeUtil;

/**
 * 组件工具类
 */
public class FwViewTool {

    private static final String TAG = "FwViewTool";

    private static FwViewTool init = null;

    public static synchronized FwViewTool builder() {
        if (null == init) {
            init = new FwViewTool();
        }
        return init;
    }

    //删除线
    public void setStrikeLine(TextView textView, boolean isUnderline) {
        if (isUnderline) {
            textView.getPaint().setFlags(Paint.UNDERLINE_TEXT_FLAG);
        } else {
            textView.getPaint().setFlags(Paint.STRIKE_THRU_TEXT_FLAG);
        }
    }

    //首行缩进
    public void setTextViewIndent(Context context, TextView textView, int indentDp, String text) {
        SpannableString ssStr = new SpannableString(text);
        int marginSpanSize = FwSizeUtil.builder().dpToPx(context, indentDp);
        LeadingMarginSpan leadingMarginSpan = new LeadingMarginSpan.Standard(marginSpanSize, 0);//仅首行缩进
        ssStr.setSpan(leadingMarginSpan, 0, text.length(), Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
        textView.setText(ssStr);
    }

    //输入框文本显示
    public void setEditTextTextVisiable(EditText ett, boolean isVisiable) {
        if (isVisiable) {
            ett.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
        } else {
            ett.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
        }
    }
}
