package com.yuntoyun.fwcore.plug.tool;
/*
 * +----------------------------------------------------------------------
 * | @Author: codeavatar   @Year：2021
 * +----------------------------------------------------------------------
 * | @Email: codeavatar@aliyun.com
 * +----------------------------------------------------------------------
 */
public class FwDistanceTool {

    /**
     * 简化距离
     *
     * @param distance  目标距离
     * @param keepDigit 小数位数
     * @return
     */
    public static String getSimpleDistance(String distance, int keepDigit) {
//        1Ym=1×10^24m（尧米）
//        1Zm=1×10^21m（泽米）
//        1Em=1×10^18m（艾米）
//        1光年=9.46×10^15m（光年）
//        1Pm=1×10^15m（拍米）
//        1Tm=1×10^12m（太米）
//        1Gm=1×10^9m（吉米）
//        1Mm=1×10^6m（兆米）
//        1km=1×10^3m （千米）
        String prefix = ((-1 != distance.indexOf('.')) ? distance.substring(0, distance.indexOf('.')) : distance);
        String suffix = ((-1 != distance.indexOf('.')) ? distance.substring(distance.indexOf('.')) : "");

        StringBuilder sbPrefix = new StringBuilder("");
        StringBuilder sbSuffix = new StringBuilder("");
        if (prefix.length() > 24) {
            sbPrefix.append(prefix.substring(0, prefix.length() - 24));
            sbSuffix.append(prefix.substring(prefix.length() - 24));
            sbSuffix.append(suffix.replace(".", ""));
            if(0 != keepDigit) {
                if (sbSuffix.length() > keepDigit) {
                    sbPrefix.append(".");
                    sbPrefix.append(sbSuffix.substring(0, keepDigit));
                } else {
                    sbPrefix.append(".");
                    sbPrefix.append(sbSuffix);
                }
            }
            sbPrefix.append("Ym");
        } else if (prefix.length() > 21) {
            sbPrefix.append(prefix.substring(0, prefix.length() - 21));
            sbSuffix.append(prefix.substring(prefix.length() - 21));
            sbSuffix.append(suffix.replace(".", ""));
            if(0 != keepDigit) {
                if (sbSuffix.length() > keepDigit) {
                    sbPrefix.append(".");
                    sbPrefix.append(sbSuffix.substring(0, keepDigit));
                } else {
                    sbPrefix.append(".");
                    sbPrefix.append(sbSuffix);
                }
            }
            sbPrefix.append("Zm");
        } else if (prefix.length() > 18) {
            sbPrefix.append(prefix.substring(0, prefix.length() - 18));
            sbSuffix.append(prefix.substring(prefix.length() - 18));
            sbSuffix.append(suffix.replace(".", ""));
            if(0 != keepDigit) {
                if (sbSuffix.length() > keepDigit) {
                    sbPrefix.append(".");
                    sbPrefix.append(sbSuffix.substring(0, keepDigit));
                } else {
                    sbPrefix.append(".");
                    sbPrefix.append(sbSuffix);
                }
            }
            sbPrefix.append("Em");
        } else if (prefix.length() > 15) {
            sbPrefix.append(prefix.substring(0, prefix.length() - 15));
            sbSuffix.append(prefix.substring(prefix.length() - 15));
            sbSuffix.append(suffix.replace(".", ""));
            if(0 != keepDigit) {
                if (sbSuffix.length() > keepDigit) {
                    sbPrefix.append(".");
                    sbPrefix.append(sbSuffix.substring(0, keepDigit));
                } else {
                    sbPrefix.append(".");
                    sbPrefix.append(sbSuffix);
                }
            }
            sbPrefix.append("Pm");
        } else if (prefix.length() > 12) {
            sbPrefix.append(prefix.substring(0, prefix.length() - 12));
            sbSuffix.append(prefix.substring(prefix.length() - 12));
            sbSuffix.append(suffix.replace(".", ""));
            if(0 != keepDigit) {
                if (sbSuffix.length() > keepDigit) {
                    sbPrefix.append(".");
                    sbPrefix.append(sbSuffix.substring(0, keepDigit));
                } else {
                    sbPrefix.append(".");
                    sbPrefix.append(sbSuffix);
                }
            }
            sbPrefix.append("Tm");
        } else if (prefix.length() > 9) {
            sbPrefix.append(prefix.substring(0, prefix.length() - 9));
            sbSuffix.append(prefix.substring(prefix.length() - 9));
            sbSuffix.append(suffix.replace(".", ""));
            if(0 != keepDigit) {
                if (sbSuffix.length() > keepDigit) {
                    sbPrefix.append(".");
                    sbPrefix.append(sbSuffix.substring(0, keepDigit));
                } else {
                    sbPrefix.append(".");
                    sbPrefix.append(sbSuffix);
                }
            }
            sbPrefix.append("Gm");
        } else if (prefix.length() > 6) {
            sbPrefix.append(prefix.substring(0, prefix.length() - 6));
            sbSuffix.append(prefix.substring(prefix.length() - 6));
            sbSuffix.append(suffix.replace(".", ""));
            if(0 != keepDigit) {
                if (sbSuffix.length() > keepDigit) {
                    sbPrefix.append(".");
                    sbPrefix.append(sbSuffix.substring(0, keepDigit));
                } else {
                    sbPrefix.append(".");
                    sbPrefix.append(sbSuffix);
                }
            }
            sbPrefix.append("Mm");
        } else if (prefix.length() > 3) {
            sbPrefix.append(prefix.substring(0, prefix.length() - 3));
            sbSuffix.append(prefix.substring(prefix.length() - 3));
            sbSuffix.append(suffix.replace(".", ""));
            if(0 != keepDigit) {
                if (sbSuffix.length() > keepDigit) {
                    sbPrefix.append(".");
                    sbPrefix.append(sbSuffix.substring(0, keepDigit));
                } else {
                    sbPrefix.append(".");
                    sbPrefix.append(sbSuffix);
                }
            }
            sbPrefix.append("Km");
        } else {
            sbPrefix.append(prefix);
            sbSuffix.append(suffix.replace(".", ""));
            if(0 != keepDigit) {
                if (sbSuffix.length() > keepDigit) {
                    sbPrefix.append(".");
                    sbPrefix.append(sbSuffix.substring(0, keepDigit));
                } else {
                    sbPrefix.append(".");
                    sbPrefix.append(sbSuffix);
                }
            }
            sbPrefix.append("m");
        }
        return sbPrefix.toString();
    }
}
