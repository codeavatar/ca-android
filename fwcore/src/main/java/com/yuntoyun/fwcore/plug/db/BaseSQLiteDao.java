package com.yuntoyun.fwcore.plug.db;
/*
 * +----------------------------------------------------------------------
 * | @Author: codeavatar   @Year：2021
 * +----------------------------------------------------------------------
 * | @Email: codeavatar@aliyun.com
 * +----------------------------------------------------------------------
 */
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import com.yuntoyun.fwcore.util.FwLogUtil;

import java.util.List;

public class BaseSQLiteDao implements ISQLiteDao {

    private final String TAG = BaseSQLiteDao.class.getSimpleName();

    protected Context context;
    protected SQLiteDBHelper dbHelper = null;
    protected SQLiteDatabase db = null;

    public BaseSQLiteDao(Context context) {
        this.context = context;
        dbHelper = SQLiteDBHelper.getInit(context);
    }

    public BaseSQLiteDao(Context context, String tableName) {
        this.context = context;
        dbHelper = SQLiteDBHelper.getInit(context);
        dbHelper.tableName = tableName;
    }

    /***********************************************
     ***************BaseDao扩展代码*********************
     ***********************************************/

    public void delete(int id) {
        db = dbHelper.getWritableDatabase();
        db.beginTransaction();
        try {
            db.delete(dbHelper.tableName, "id=?", new String[]{String.valueOf(id)});
            db.setTransactionSuccessful();
        } catch (SQLException e) {
            FwLogUtil.error(TAG, e);
        } finally {
            db.endTransaction();
            db.close();
        }
    }


    /***********************************************
     ***************IDao实现代码*********************
     ***********************************************/

    @Override
    public void insert(List<String> sqlList) {
        db = dbHelper.getWritableDatabase();
        db.beginTransaction();
        try {
            for (String sql : sqlList) {
                db.execSQL(sql);
            }
            db.setTransactionSuccessful();
        } catch (SQLException e) {
            FwLogUtil.error(TAG, e);
        } finally {
            db.endTransaction();
            db.close();
        }
    }

    @Override
    public void insert(String sql) {
        db = dbHelper.getWritableDatabase();
        db.beginTransaction();
        try {
            db.execSQL(sql);
            db.setTransactionSuccessful();
        } catch (SQLException e) {
            FwLogUtil.error(TAG, e);
        } finally {
            db.endTransaction();
            db.close();
        }
    }

    @Override
    public void insert(ContentValues values) {
        db = dbHelper.getWritableDatabase();
        db.beginTransaction();
        try {
            db.insert(dbHelper.tableName, null, values);
//        db.insertOrThrow(dbHelper.tableName, null, values);
            db.setTransactionSuccessful();
        } catch (SQLException e) {
            FwLogUtil.error(TAG, e);
        } finally {
            db.endTransaction();
            db.close();
        }
    }

    @Override
    public void delete(String sql) {
        db = dbHelper.getWritableDatabase();
        db.beginTransaction();
        try {
            db.execSQL(sql);
            db.setTransactionSuccessful();
        } catch (SQLException e) {
            FwLogUtil.error(TAG, e);
        } finally {
            db.endTransaction();
            db.close();
        }
    }

    @Override
    public void delete(String table, String whereClause, String[] whereArgs) {
        db = dbHelper.getWritableDatabase();
        db.beginTransaction();
        try {
            db.delete(table, whereClause, whereArgs);
            db.setTransactionSuccessful();
        } catch (SQLException e) {
            FwLogUtil.error(TAG, e);
        } finally {
            db.endTransaction();
            db.close();
        }
    }

    @Override
    public void update(String sql) {
        db = dbHelper.getWritableDatabase();
        db.beginTransaction();
        try {
            db.execSQL(sql);
            db.setTransactionSuccessful();
        } catch (SQLException e) {
            FwLogUtil.error(TAG, e);
        } finally {
            db.endTransaction();
            db.close();
        }
    }

    @Override
    public void update(String table, ContentValues values, String whereClause, String[] whereArgs) {
        db = dbHelper.getWritableDatabase();
        db.beginTransaction();
        try {
            db.update(table, values, whereClause, whereArgs);
            db.setTransactionSuccessful();
        } catch (SQLException e) {
            FwLogUtil.error(TAG, e);
        } finally {
            db.endTransaction();
            db.close();
        }
    }

    @Override
    public Cursor select(String table, String[] columns, String selection, String[]
            selectionArgs, String groupBy, String having, String orderBy) {
        Cursor cursor = null;
        db = dbHelper.getReadableDatabase();
        db.beginTransaction();
        try {
            cursor = db.query(table, columns, selection,
                    selectionArgs, groupBy, having,
                    orderBy);
            db.setTransactionSuccessful();
        } catch (SQLException e) {
            FwLogUtil.error(TAG, e);
        } finally {
            db.endTransaction();
            if (null == cursor) {
                db.close();
            }
        }
        return cursor;
    }

    @Override
    public Cursor select(String table, String[] columns, String selection, String[]
            selectionArgs, String groupBy, String having, String orderBy, String limit) {
        Cursor cursor = null;
        db = dbHelper.getReadableDatabase();
        db.beginTransaction();
        try {
            cursor = db.query(table, columns, selection,
                    selectionArgs, groupBy, having,
                    orderBy, limit);
            db.setTransactionSuccessful();
        } catch (SQLException e) {
            FwLogUtil.error(TAG, e);
        } finally {
            db.endTransaction();
            if (null == cursor) {
                db.close();
            }
        }
        return cursor;
    }

    @Override
    public Cursor select(boolean distinct, String table, String[] columns, String selection,
                         String[] selectionArgs, String groupBy, String having, String orderBy,
                         String limit) {
        Cursor cursor = null;
        db = dbHelper.getReadableDatabase();
        db.beginTransaction();
        try {
            cursor = db.query(distinct, table, columns,
                    selection, selectionArgs, groupBy,
                    having, orderBy, limit);
            db.setTransactionSuccessful();
        } catch (SQLException e) {
            FwLogUtil.error(TAG, e);
        } finally {
            db.endTransaction();
            if (null == cursor) {
                db.close();
            }
        }
        return cursor;
    }

    @Override
    public Cursor select(String sql, String[] selectionArgs) {
        Cursor cursor = null;
        db = dbHelper.getReadableDatabase();
        db.beginTransaction();
        try {
            cursor = db.rawQuery(sql, selectionArgs);
            db.setTransactionSuccessful();
        } catch (SQLException e) {
            FwLogUtil.error(TAG, e);
        } finally {
            db.endTransaction();
            if (null == cursor) {
                db.close();
            }
        }
        return cursor;
    }
}
