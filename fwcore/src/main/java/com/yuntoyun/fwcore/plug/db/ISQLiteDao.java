package com.yuntoyun.fwcore.plug.db;
/*
 * +----------------------------------------------------------------------
 * | @Author: codeavatar   @Year：2021
 * +----------------------------------------------------------------------
 * | @Email: codeavatar@aliyun.com
 * +----------------------------------------------------------------------
 */
import android.content.ContentValues;
import android.database.Cursor;

import java.util.List;


public interface ISQLiteDao {

    void insert(List<String> sqlList);

    void insert(String sql);

    void insert(ContentValues values);

    void delete(String sql);

    void delete(String table, String whereClause, String[] whereArgs);

    void update(String sql);

    void update(String table, ContentValues values, String whereClause, String[] whereArgs);

    Cursor select(String table, String[] columns, String selection,
                  String[] selectionArgs, String groupBy, String having,
                  String orderBy);

    Cursor select(String table, String[] columns, String selection, String[]
            selectionArgs, String groupBy, String having, String orderBy, String limit);

    Cursor select(boolean distinct, String table, String[] columns,
                  String selection, String[] selectionArgs, String groupBy,
                  String having, String orderBy, String limit);

    Cursor select(String sql, String[] selectionArgs);
}
