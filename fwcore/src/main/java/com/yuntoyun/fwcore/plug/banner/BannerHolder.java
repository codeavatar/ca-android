package com.yuntoyun.fwcore.plug.banner;
/*
 * +----------------------------------------------------------------------
 * | @Author: codeavatar   @Year：2021
 * +----------------------------------------------------------------------
 * | @Email: codeavatar@aliyun.com
 * +----------------------------------------------------------------------
 */
import android.view.View;

import com.bigkoo.convenientbanner.holder.Holder;
import com.google.gson.JsonObject;

public class BannerHolder extends Holder<JsonObject> {

    public BannerHolder(View itemView) {
        super(itemView);
    }

    @Override
    protected void initView(View itemView) {

    }

    @Override
    public void updateUI(JsonObject data) {
        if (null == data) return;

        //当前索引
        int pIndex = getAdapterPosition();

    }
}
