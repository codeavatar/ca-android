package com.yuntoyun.fwcore.plug.tool;
/*
 * +----------------------------------------------------------------------
 * | @Author: codeavatar   @Year：2021
 * +----------------------------------------------------------------------
 * | @Email: codeavatar@aliyun.com
 * +----------------------------------------------------------------------
 */
import android.text.TextUtils;

/**
 * 格式工具
 */
public class FwFormatTool {

    private final String TAG = FwFormatTool.class.getSimpleName();

    private static FwFormatTool instance = null;

    public synchronized static FwFormatTool builder() {
        if (null == instance) {
            instance = new FwFormatTool();
        }
        return instance;
    }

    public String insertStr(String targetStr, int perStrLen, String splitStr) {

        if (TextUtils.isEmpty(targetStr) || TextUtils.isEmpty(splitStr) || perStrLen < 1)
            return targetStr;

        int pLooper = ((targetStr.length() % perStrLen == 0) ? targetStr.length() / perStrLen :
                targetStr.length() / perStrLen + 1);
        StringBuffer sb = new StringBuffer("");
        for (int i = 0; i < pLooper; i++) {
            if (pLooper - 1 == i) {
                sb.append(targetStr.substring((i * perStrLen)));
            } else {
                sb.append(targetStr.substring((i * perStrLen), ((i + 1) * perStrLen)));
            }

            if (i + 1 < pLooper)
                sb.append(splitStr);
        }
        return sb.toString();
    }
}
