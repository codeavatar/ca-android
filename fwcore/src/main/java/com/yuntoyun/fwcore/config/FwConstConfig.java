package com.yuntoyun.fwcore.config;
/*
 * +----------------------------------------------------------------------
 * | @Author: codeavatar   @Year：2021
 * +----------------------------------------------------------------------
 * | @Email: codeavatar@aliyun.com
 * +----------------------------------------------------------------------
 */
/**
 * 系统固定设置
 */
public class FwConstConfig {

    //SharePreferences缓存文件名设置
    public final static String CONST_SHAREDATA_NAME = "ca_Shared_data";
    public final static String CONST_PRIVATEDATA_NAME = "ca_Private_data";
    public final static String CONST_AUTHDATA_NAME = "ca_Auth_data";
    public final static String CONST_POSTDATA_NAME = "ca_Post_data";

    //系统默认Packages名称
    public static final String CONST_PACKAGE_NAME = "com.yuntoyun.codeavatar.klshare";

    //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    //====APICore2018框架设置
    //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    public static final String API_KEY_SIGN = "fwSign";
    public static final String API_KEY_DATA = "fwData";
    public static final String API_KEY_LIST = "fwList";
    public static final String API_KEY_STATUS = "fwStatus";
    public static final String API_KEY_MESSAGE = "fwMessage";
    public static final String API_KEY_CURRENTPAGE = "fwCurrentPage";
    public static final String API_KEY_TOTALPAGE = "fwTotalPage";
    public static final String API_KEY_TOTALCOUNT = "fwTotalCount";

    public static final String API_KEY_PLATFORM = "fwPlatform";
    public static final String API_KEY_DEVICE = "fwDevice";
    public static final String API_KEY_VERSION = "fwVersion";
    public static final String API_KEY_RNDSTR = "fwRndstr";
    public static final String API_KEY_RNDNUM = "fwRndnum";

    public static final String API_KEY_USERID = "fwUserId";
    public static final String API_KEY_USERTOKEN = "fwUserToken";
    public static final String API_KEY_LONGITUDE = "fwLongitude";
    public static final String API_KEY_LATITUDE = "fwLatitude";
    public static final String API_KEY_PROVINCEID = "fwProvinceId";
    public static final String API_KEY_CITYID = "fwCityId";
    public static final String API_KEY_DISTRICTID = "fwDistrictId";
    public static final String API_KEY_COUNTYID = "fwCountyId";
}
