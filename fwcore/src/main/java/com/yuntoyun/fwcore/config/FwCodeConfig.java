package com.yuntoyun.fwcore.config;
/*
 * +----------------------------------------------------------------------
 * | @Author: codeavatar   @Year：2021
 * +----------------------------------------------------------------------
 * | @Email: codeavatar@aliyun.com
 * +----------------------------------------------------------------------
 */
/**
 * 数据结果编码
 */
public class FwCodeConfig {

    public static final int CODE_SUCCESSED = 100;
    public static final int CODE_FAILED = 1;
    public static final int CODE_LOGIN_AGAIN = -1;
    public static final int CODE_MLOGIN_AGAIN = -2;

    public static final int MESSAGE_WHAT_RECYCLERVIEW = 99999999;
}
