package com.yuntoyun.fwcore.config;
/*
 * +----------------------------------------------------------------------
 * | @Author: codeavatar   @Year：2021
 * +----------------------------------------------------------------------
 * | @Email: codeavatar@aliyun.com
 * +----------------------------------------------------------------------
 */
import com.yuntoyun.fwcore.factory.sign.SignType;

public interface FwIConfig {

    //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    //====框架控制中心配置
    //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    boolean getLogSwitch();

    boolean getSystemLogSwitch();


    String getRootApi();

    String getDomainSite();

    //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    //====框架安全配置
    //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    SignType getKeyType();

    String getMd5SignKey();

    String getRsaSignKey();

    String getRsa2SignKey();

    //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    //====框架第三方配置
    //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    String getWechatAppId();

    String getWechatAppSecret();

    String getWebSocketHost();

    int getWebSocketPort();

    boolean getJpushLaunch();

}
