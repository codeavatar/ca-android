package com.yuntoyun.fwcore.config;
/*
 * +----------------------------------------------------------------------
 * | @Author: codeavatar   @Year：2021
 * +----------------------------------------------------------------------
 * | @Email: codeavatar@aliyun.com
 * +----------------------------------------------------------------------
 */
import com.yuntoyun.fwcore.base.FwApplication;
import com.yuntoyun.fwcore.factory.sign.SignType;

/**
 * 系统开关控制
 */
public class FwAppConfig {

    //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    //====框架控制中心配置
    //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    /**
     * 数据接口
     */
    public static final String ROOT_API = FwApplication.getAppConfig().getRootApi();

    /**
     * 调试日志输出开关
     */
    public static final boolean PRINT_LOG_SWITCH = FwApplication.getAppConfig().getLogSwitch();

    /**
     * 系统日志输出开关
     */
    public static final boolean PRINT_LOG_SWITCH_SYSTEM = FwApplication.getAppConfig()
            .getSystemLogSwitch();

    //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    //====框架安全配置
    //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    public static final SignType KEY_TYPE = FwApplication.getAppConfig().getKeyType();
    public static final String MD5_KEY = FwApplication.getAppConfig().getMd5SignKey();
    public static final String RSA_KEY = FwApplication.getAppConfig().getRsaSignKey();
    public static final String RSA2_KEY = FwApplication.getAppConfig().getRsa2SignKey();

    //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    //====框架第三方配置
    //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    /**
     * 微信配置
     */
    public static final String WECHAT_APPID = FwApplication.getAppConfig().getWechatAppId();
    public static final String WECHAT_APPSECRET = FwApplication.getAppConfig().getWechatAppSecret();

    /**
     * WebSocket配置
     */
    public static final String WEBSOCKET_HOST = FwApplication.getAppConfig().getWebSocketHost();
    public static final int WEBSOCKET_PORT = FwApplication.getAppConfig().getWebSocketPort();

    /**
     * 极光推送配置
     */
    public static final boolean JPUSH_LAUNCH = FwApplication.getAppConfig().getJpushLaunch();

    //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    //====框架固定配置
    //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    /**
     * 请求服务器超时时间（单位秒）
     */
    public static final int HTTP_TIMEOUT = 20;
    /**
     * RecyclerView定时请求数据时间（单位毫秒）
     */
    public static final int RECYCLERVIEW_DO_TIMEOUT = 1000;
    /**
     * 随机数长度
     */
    public static final int RANDOM_NUM_LENGTH = 32;
    /**
     * 随机字符串
     */
    public static final int RANDOM_STR_LENGTH = 32;
    /**
     * WebSocket服务超时时间（单位毫秒）
     */
    public static final int WEBSOCKET_HEART = 40000;
    /**
     * 验证码倒计时时间（单位秒）
     */
    public static final int VALIDCODE_TIMEOUT = 120;
}
