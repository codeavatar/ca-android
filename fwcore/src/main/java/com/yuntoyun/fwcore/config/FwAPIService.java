package com.yuntoyun.fwcore.config;
/*
 * +----------------------------------------------------------------------
 * | @Author: codeavatar   @Year：2021
 * +----------------------------------------------------------------------
 * | @Email: codeavatar@aliyun.com
 * +----------------------------------------------------------------------
 */
import com.yuntoyun.fwcore.model.FwResultJsonDataModel;
import com.yuntoyun.fwcore.model.FwResultJsonListDataModel;

import java.util.HashMap;
import java.util.List;

import io.reactivex.Observable;
import okhttp3.MultipartBody;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

public interface FwAPIService {

    /**
     * 设置http路径前缀
     */
    final String httpPrefix = "/index.php";

    //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    //==我的功能
    //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    //订单详情
    @FormUrlEncoded
    @POST(httpPrefix + "/minipro/order/orderInfo")
    Observable<FwResultJsonDataModel> getOrderInfo(@FieldMap HashMap<String, Object> map);

    //打卡详情
    @FormUrlEncoded
    @POST(httpPrefix + "/minipro/course/getClockLog")
    Observable<FwResultJsonListDataModel> getSigninDetail(@FieldMap HashMap<String, Object> map);

    //打卡记录（我的订单）
    @FormUrlEncoded
    @POST(httpPrefix + "/minipro/order/index")
    Observable<FwResultJsonListDataModel> getOrderList(@FieldMap HashMap<String, Object> map);

    //提现记录
    @FormUrlEncoded
    @POST(httpPrefix + "/minipro/withdrawal/getList")
    Observable<FwResultJsonListDataModel> getWithdrawalRecordList(@FieldMap HashMap<String, Object> map);

    //用户申请提现
    @FormUrlEncoded
    @POST(httpPrefix + "/minipro/withdrawal/apply")
    Observable<FwResultJsonDataModel> getWithdrawal(@FieldMap HashMap<String, Object> map);

    //申请提现数据
    @FormUrlEncoded
    @POST(httpPrefix + "/minipro/withdrawal/getInfo")
    Observable<FwResultJsonDataModel> getWithdrawalInfo(@FieldMap HashMap<String, Object> map);

    //分享红包
    @FormUrlEncoded
    @POST(httpPrefix + "/minipro/member/getBalance")
    Observable<FwResultJsonListDataModel> getCashBagList(@FieldMap HashMap<String, Object> map);


    //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    //==机构功能
    //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    //机构
    @FormUrlEncoded
    @POST(httpPrefix + "/minipro/merchant/index")
    Observable<FwResultJsonListDataModel> getOrgList(@FieldMap HashMap<String, Object> map);

    //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    //==首页功能
    //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    //线上课堂
    @FormUrlEncoded
    @POST(httpPrefix + "/minipro/video/index")
    Observable<FwResultJsonListDataModel> getLessonList(@FieldMap HashMap<String, Object> map);

    //首页
    @FormUrlEncoded
    @POST(httpPrefix + "/minipro/index/index")
    Observable<FwResultJsonDataModel> getHomeIndex(@FieldMap HashMap<String, Object> map);

    //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    //==基础功能
    //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    //找回密码
    @FormUrlEncoded
    @POST(httpPrefix + "/minipro/login/findPwd")
    Observable<FwResultJsonDataModel> getFindLoginPwd(@FieldMap HashMap<String, Object> map);

//    //绑定手机号
//    @FormUrlEncoded
//    @POST(httpPrefix + "/member/OwnerMember/bindMobile")
//    Observable<FwResultJsonDataModel> setBindPhone(@FieldMap HashMap<String, Object> map);

    //登录入口(微信)
    @FormUrlEncoded
    @POST(httpPrefix + "/minipro/login/wechatLogin")
    Observable<FwResultJsonDataModel> getLoginByWechat(@FieldMap HashMap<String, Object> map);

    //绑定微信
    @FormUrlEncoded
    @POST(httpPrefix + "/minipro/member/bindWechat")
    Observable<FwResultJsonDataModel> setBindWechat(@FieldMap HashMap<String, Object> map);

    //登录入口
    @FormUrlEncoded
    @POST(httpPrefix + "/minipro/login/login")
    Observable<FwResultJsonDataModel> getLogin(@FieldMap HashMap<String, Object> map);

    //注册入口
    @FormUrlEncoded
    @POST(httpPrefix + "/minipro/login/regist")
    Observable<FwResultJsonDataModel> getRegist(@FieldMap HashMap<String, Object> map);

    //自动登录
    @FormUrlEncoded
    @POST(httpPrefix + "/minipro/login/autoLogin")
    Observable<FwResultJsonDataModel> getAutoLogin(@FieldMap HashMap<String, Object> map);


    //开关设置
    @FormUrlEncoded
    @POST(httpPrefix + "/minipro/index/getSetting")
    Observable<FwResultJsonDataModel> getSwitchSetting(@FieldMap HashMap<String, Object> map);

    //用户协议(隐私政策)
    @FormUrlEncoded
    @POST(httpPrefix + "/minipro/index/agreement")
    Observable<FwResultJsonDataModel> getAgreement(@FieldMap HashMap<String, Object> map);


    //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    //==共用功能
    //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    //发送验证码
    @FormUrlEncoded
    @POST(httpPrefix + "/minipro/index/sendCode")
    Observable<FwResultJsonDataModel> getSmsSend(@FieldMap HashMap<String, Object> map);

    //安全退出
    @FormUrlEncoded
    @POST(httpPrefix + "/minipro/member/signOut")
    Observable<FwResultJsonDataModel> getSignOut(@FieldMap HashMap<String, Object> map);

    //检测升级
    @FormUrlEncoded
    @POST(httpPrefix + "/minipro/index/upgrade")
    Observable<FwResultJsonDataModel> getAppUpgrade(@FieldMap HashMap<String, Object> map);

}
