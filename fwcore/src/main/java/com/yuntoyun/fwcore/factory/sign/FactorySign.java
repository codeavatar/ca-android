package com.yuntoyun.fwcore.factory.sign;
/*
 * +----------------------------------------------------------------------
 * | @Author: codeavatar   @Year：2021
 * +----------------------------------------------------------------------
 * | @Email: codeavatar@aliyun.com
 * +----------------------------------------------------------------------
 */
import android.text.TextUtils;

import com.yuntoyun.fwcore.config.FwAppConfig;
import com.yuntoyun.fwcore.config.FwConstConfig;
import com.yuntoyun.fwcore.security.alipaySign.AlipaySign;
import com.yuntoyun.fwcore.security.md5sign.Md5Sign;

import java.util.Map;

public class FactorySign {

    private static final Object mLock = new Object();

    private static FactorySign init = null;

    public static FactorySign builder() {
        if (null == init) {
            synchronized (mLock) {
                if (null == init) {
                    init = new FactorySign();
                }
            }
        }
        return init;
    }

    /**
     * 验签
     *
     * @param map         含签名的数据
     * @param signKeyName 默认为null，使用APICore框架的 fwSign 键名
     * @param type        {@link SignType}
     * @param secretKey   默认为null
     * @return
     */
    public boolean checkSign(Map<String, String> map, String signKeyName, SignType type, String
            secretKey) {
        if (null == map)
            return false;

        boolean result = false;
        String signStr = "";
        if (!TextUtils.isEmpty(signKeyName)) {
            signStr = map.get(signKeyName);
            map.remove(signKeyName);
        } else {
            signStr = map.get(FwConstConfig.API_KEY_SIGN);
            map.remove(FwConstConfig.API_KEY_SIGN);
        }

        switch (type) {
            case Md5:
                if (TextUtils.isEmpty(secretKey)) {
                    secretKey = FwAppConfig.MD5_KEY;
                }
                result = Md5Sign.checkSign(map, signStr, secretKey);
                break;
            case Rsa:
                if (TextUtils.isEmpty(secretKey)) {
                    secretKey = FwAppConfig.RSA_KEY;
                }
                result = AlipaySign.checkSign(map, signStr, secretKey, false);
                break;
            case Rsa2:
                if (TextUtils.isEmpty(secretKey)) {
                    secretKey = FwAppConfig.RSA2_KEY;
                }
                result = AlipaySign.checkSign(map, signStr, secretKey, true);
                break;
        }
        return result;
    }

    /**
     * 加签
     *
     * @param map       不含签名的数据
     * @param type      {@link SignType}
     * @param secretKey 默认为null
     * @return
     */
    public String addSign(Map<String, String> map, SignType type, String secretKey) {

        if (null == map)
            return "";

        String result = "";
        switch (type) {
            case Md5:
                if (TextUtils.isEmpty(secretKey)) {
                    secretKey = FwAppConfig.MD5_KEY;
                }
                result = Md5Sign.addSign(map, secretKey);
                break;
            case Rsa:
                if (TextUtils.isEmpty(secretKey)) {
                    secretKey = FwAppConfig.RSA_KEY;
                }
                result = AlipaySign.getSign(map, secretKey, false);
                break;
            case Rsa2:
                if (TextUtils.isEmpty(secretKey)) {
                    secretKey = FwAppConfig.RSA2_KEY;
                }
                result = AlipaySign.getSign(map, secretKey, true);
                break;
        }
        return result;
    }
}
