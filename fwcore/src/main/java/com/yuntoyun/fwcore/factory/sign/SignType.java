package com.yuntoyun.fwcore.factory.sign;
/*
 * +----------------------------------------------------------------------
 * | @Author: codeavatar   @Year：2021
 * +----------------------------------------------------------------------
 * | @Email: codeavatar@aliyun.com
 * +----------------------------------------------------------------------
 */
public enum SignType {
    Md5, Rsa, Rsa2
}
