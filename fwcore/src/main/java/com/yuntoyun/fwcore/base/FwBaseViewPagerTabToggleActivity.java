package com.yuntoyun.fwcore.base;
/*
 * +----------------------------------------------------------------------
 * | @Author: codeavatar   @Year：2021
 * +----------------------------------------------------------------------
 * | @Email: codeavatar@aliyun.com
 * +----------------------------------------------------------------------
 */
import android.graphics.Color;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;


import com.yuntoyun.fwcore.R;

import java.util.ArrayList;
import java.util.List;

/**
 * 应用 SlidingTabLayout + ViewPager 组件 Activity 基础类
 */
public abstract class FwBaseViewPagerTabToggleActivity extends FwBaseActivity {

    protected TextView tvw_tab1;
    protected TextView tvw_tab2;
    protected ViewPager vpr_pages;
    protected List<Fragment> mTabFragments;

    protected List<Fragment> gFragments = null;
    protected List<TextView> gSwitcherTabs = null;
    protected List<int[]> gSwitcherStyle = null;

    @Override
    protected int getLayoutResId() {
        int resId = this.initChildLayoutResId();
        if (resId < 1) {
            return R.layout.fw_layout_tabtoggle_viewpager;
        }
        return resId;
    }

    @Override
    protected void initView() {
        //初始化变量
        vpr_pages = findViewById(R.id.vpr_pages);
        tvw_tab1 = findViewById(R.id.tvw_tab1);
        tvw_tab2 = findViewById(R.id.tvw_tab2);

        this.mTabFragments = new ArrayList<>();
        gSwitcherTabs = new ArrayList<>();
        gSwitcherStyle = new ArrayList<>();

        vpr_pages.setAdapter(getPagerAdapter(getSupportFragmentManager()));
        this.initTabToggleView();
        this.initChildView();
    }

    @Override
    protected void initData() {
        this.initChildData();
    }

    @Override
    protected void initEvent() {
        this.initChildEvent();
    }

    protected abstract PagerAdapter getPagerAdapter(FragmentManager fragmentManager);

    protected abstract int initChildLayoutResId();

    protected abstract void initChildView();

    protected abstract void initChildData();

    protected abstract void initChildEvent();

    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    private void initTabToggleView(){
        //加载Tab组件
        gSwitcherTabs.add(tvw_tab1);
        gSwitcherTabs.add(tvw_tab2);
        //加载Tab样式
        gSwitcherStyle.add(new int[]{
                R.drawable.fw_style_tab_toggle_tab1_unchecked,
                R.drawable.fw_style_tab_toggle_tab1_checked
        });
        gSwitcherStyle.add(new int[]{
                R.drawable.fw_style_tab_toggle_tab2_unchecked,
                R.drawable.fw_style_tab_toggle_tab2_checked
        });
        //翻页事件
        vpr_pages.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset,
                                       int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                doToggleTabs(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        //Tabs点击事件
    }

    private void doToggleTabs(int index) {
        for (int i = 0; i < this.gSwitcherTabs.size(); i++) {
            TextView tmpTvw = this.gSwitcherTabs.get(i);
            if (i == index) {
                tmpTvw.setBackgroundResource(this.gSwitcherStyle.get(i)[1]);
                tmpTvw.setTextColor(Color.parseColor("#FFFFFF"));
            } else {
                tmpTvw.setBackgroundResource(this.gSwitcherStyle.get(i)[0]);
                tmpTvw.setTextColor(Color.parseColor("#FDA05A"));
            }
        }
    }

}
