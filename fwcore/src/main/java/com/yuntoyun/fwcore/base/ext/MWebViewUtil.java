package com.yuntoyun.fwcore.base.ext;
/*
 * +----------------------------------------------------------------------
 * | @Author: codeavatar   @Year：2021
 * +----------------------------------------------------------------------
 * | @Email: codeavatar@aliyun.com
 * +----------------------------------------------------------------------
 */
import android.app.Activity;
import android.content.Context;
import android.net.Uri;
import android.text.TextUtils;
import android.webkit.WebView;

public class MWebViewUtil {
	/**
	 * 加载html代码
	 * @param activity
	 * @param content
	 * @param mywebview
	 */
	public static void setWithBaseURL(final Activity activity, String content,WebView mywebview) {
		mywebview.loadDataWithBaseURL(null, content, "text/html", "utf-8", null);
	}

	/**
	 * 选择跳转  /wap/item-detail.html 商品详情
	 * @param url
	 * @param context
	 * @return
	 */
	public static  boolean intentView(String url, Context context){
		boolean isOk=false;
		try {
			Uri uri = Uri.parse(url);
			String path=uri.getPath();
			if(!TextUtils.isEmpty(path)){
				if (path.equals("/wap/item-detail.html")) {
//					Intent intent = new Intent(context, CommodityDetailsFragmentActivity.class);
//					String itemId = uri.getQueryParameter("item_id");
//					int item_id = Integer.valueOf(itemId);
//					intent.putExtra("item_id", item_id);
//					context.startActivity(intent);
					isOk=true;
				}
			}
		} catch (Exception e) {
			isOk=false;
		}
		return  isOk;
	}
}
