package com.yuntoyun.fwcore.base.ext;

/*
 * +----------------------------------------------------------------------
 * | @Author: codeavatar   @Year：2021
 * +----------------------------------------------------------------------
 * | @Email: codeavatar@aliyun.com
 * +----------------------------------------------------------------------
 */

//为实现自动加载扩展的事件
public interface OnScrollListener {

    public abstract void onScrollUp();//scroll down to up

    public abstract void onScrollDown();//scroll from up to down

    public abstract void onBottom();//load next page

    public abstract void onScrolled(int distanceX, int distanceY);// moving state,you can get the
    // move distance

}
