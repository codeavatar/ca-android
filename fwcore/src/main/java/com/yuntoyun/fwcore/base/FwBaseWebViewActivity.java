package com.yuntoyun.fwcore.base;
/*
 * +----------------------------------------------------------------------
 * | @Author: codeavatar   @Year：2021
 * +----------------------------------------------------------------------
 * | @Email: codeavatar@aliyun.com
 * +----------------------------------------------------------------------
 */
import android.util.DisplayMetrics;
import android.view.KeyEvent;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.ImageView;

import com.yuntoyun.fwcore.R;
import com.yuntoyun.fwcore.base.ext.MWebChromeClient;
import com.yuntoyun.fwcore.base.ext.MWebClient;

/**
 * 应用 WebView组件 Activity 基础类
 */
public abstract class FwBaseWebViewActivity extends FwBaseActivity {

    protected WebView wvw_webpage;

    @Override
    protected int getLayoutResId() {
        int resId = this.initChildLayoutResId();
        if (resId < 1) {
            return R.layout.fw_layout_webview;
        }
        return resId;
    }

    @Override
    protected void initView() {
        wvw_webpage = findViewById(R.id.wvw_webpage);
        ImageView ivw_left_action = findViewById(R.id.ivw_left_action);
        if (null != ivw_left_action) {
            ivw_left_action.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(isMustFinish()){
                        finish();
                    }else {
                        if (wvw_webpage.canGoBack()) {
                            wvw_webpage.goBack();
                        } else {
                            finish();
                        }
                    }
                }
            });
        }
        this.initWebView();
        this.initChildView();
    }

    @Override
    protected void initData() {
        this.initChildData();
        //加载网址
        if (this.isLoadUrl()) {
            //清除页面缓顾情况
            String tmpWebUrl = this.getWebUrlOrContent();
            if (-1 == tmpWebUrl.indexOf("?")) {
                tmpWebUrl += "?" + Math.random();
            } else {
                tmpWebUrl += "&" + Math.random();
            }
            this.wvw_webpage.loadUrl(tmpWebUrl);
        }else{
            this.wvw_webpage.loadData(this.getWebUrlOrContent(), "text/html", "utf-8");
//            this.wvw_webpage.loadDataWithBaseURL(null, this.getWebUrlOrContent(), "text/html", "utf-8", null);
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK) && wvw_webpage.canGoBack()) {
            wvw_webpage.goBack();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    protected void onDestroy() {
        wvw_webpage.destroy();
        super.onDestroy();
    }

    protected abstract int initChildLayoutResId();

    @Override
    protected void initEvent() {
        this.initChildEvent();
    }

    protected abstract void initChildView();

    protected abstract void initChildData();

    protected abstract void initChildEvent();

    protected abstract void initJsInterface(WebView wvw_webpage);

    protected abstract String getWebUrlOrContent();

    protected abstract boolean isLoadUrl();

    protected abstract boolean isMustFinish();

    //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    private void initWebView() {
        int screenDensity = getResources().getDisplayMetrics().densityDpi;
        WebSettings settings = wvw_webpage.getSettings();
        WebSettings.ZoomDensity zoomDensity = WebSettings.ZoomDensity.MEDIUM;
        switch (screenDensity) {
            case DisplayMetrics.DENSITY_LOW:
                zoomDensity = WebSettings.ZoomDensity.CLOSE;
                break;
            case DisplayMetrics.DENSITY_MEDIUM:
                zoomDensity = WebSettings.ZoomDensity.MEDIUM;
                break;
            case DisplayMetrics.DENSITY_HIGH:
                zoomDensity = WebSettings.ZoomDensity.FAR;
                break;
        }

        settings.setDefaultZoom(zoomDensity);
        // 设置webview为单列显示，是一些大图片适应屏幕宽度
        settings.setLayoutAlgorithm(WebSettings.LayoutAlgorithm.SINGLE_COLUMN);
        settings.setUseWideViewPort(true);
        // 设置数据
        settings.setJavaScriptEnabled(true);
        settings.setJavaScriptCanOpenWindowsAutomatically(true);
        settings.setUseWideViewPort(true);
        settings.setLoadWithOverviewMode(true);
        settings.setRenderPriority(WebSettings.RenderPriority.HIGH);

        //启动webview的Html5的本地存储功能。
        settings.setDomStorageEnabled(true);
        settings.setAllowFileAccess(true);
        settings.setAppCacheEnabled(true);


        wvw_webpage.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);
        wvw_webpage.setInitialScale(3);
        wvw_webpage.requestFocus();
        wvw_webpage.setWebViewClient(new MWebClient(fwActivity));
        wvw_webpage.setWebChromeClient(new MWebChromeClient());
        initJsInterface(wvw_webpage);
    }
}
