package com.yuntoyun.fwcore.base;
/*
 * +----------------------------------------------------------------------
 * | @Author: codeavatar   @Year：2021
 * +----------------------------------------------------------------------
 * | @Email: codeavatar@aliyun.com
 * +----------------------------------------------------------------------
 */
import com.flyco.tablayout.CommonTabLayout;
import com.flyco.tablayout.listener.CustomTabEntity;
import com.flyco.tablayout.listener.OnTabSelectListener;
import com.yuntoyun.fwcore.R;

import java.util.ArrayList;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

/**
 * 应用 ViewPager + SlidingTabLayout组件 Activity 基础类
 */
public abstract class FwBaseViewPagerNavActivity extends FwBaseActivity {

    protected CommonTabLayout ctl_tabs;
    protected ViewPager vpr_pages;

    protected ArrayList<Fragment> mTabFragments;
    protected ArrayList<CustomTabEntity> mTabEntities;

    @Override
    protected int getLayoutResId() {
        int resId = this.initChildLayoutResId();
        if (resId < 1) {
            return R.layout.fw_layout_viewpager_commontablayout;
        }
        return resId;
    }

    @Override
    protected void initView() {
        //初始化变量
        this.mTabFragments = getTabFragments();
        this.mTabEntities = getTabEntities();
        //初始化控件
        ctl_tabs = findViewById(R.id.ctl_tabs);
        vpr_pages = findViewById(R.id.vpr_pages);
        //翻页关联
        vpr_pages.setAdapter(getPagerAdapter(getSupportFragmentManager()));
        vpr_pages.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset,
                                       int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                ctl_tabs.setCurrentTab(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        //导航关联
        ctl_tabs.setTabData(mTabEntities);
        ctl_tabs.setOnTabSelectListener(new OnTabSelectListener() {
            @Override
            public void onTabSelect(int position) {
                vpr_pages.setCurrentItem(position, true);
            }

            @Override
            public void onTabReselect(int position) {

            }
        });
        ctl_tabs.setCurrentTab(0);
        this.initChildView();
    }

    @Override
    protected void initData() {
        this.initChildData();
    }

    @Override
    protected void initEvent() {
        this.initChildEvent();
    }

    protected abstract ArrayList<Fragment> getTabFragments();

    protected abstract ArrayList<CustomTabEntity> getTabEntities();

    protected abstract PagerAdapter getPagerAdapter(FragmentManager fragmentManager);

    protected abstract int initChildLayoutResId();

    protected abstract void initChildView();

    protected abstract void initChildData();

    protected abstract void initChildEvent();
}
