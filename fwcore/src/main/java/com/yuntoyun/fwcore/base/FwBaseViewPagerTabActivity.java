package com.yuntoyun.fwcore.base;
/*
 * +----------------------------------------------------------------------
 * | @Author: codeavatar   @Year：2021
 * +----------------------------------------------------------------------
 * | @Email: codeavatar@aliyun.com
 * +----------------------------------------------------------------------
 */
import android.widget.LinearLayout;

import com.flyco.tablayout.SlidingTabLayout;
import com.yuntoyun.fwcore.R;


import java.util.ArrayList;
import java.util.List;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

/**
 * 应用 SlidingTabLayout + ViewPager 组件 Activity 基础类
 */
public abstract class FwBaseViewPagerTabActivity extends FwBaseActivity {

    protected LinearLayout llt_page_container;
    protected SlidingTabLayout stl_tabs;
    protected ViewPager vpr_pages;

    protected List<Fragment> mTabFragments;
    protected List<String> mTabTitles;

    @Override
    protected int getLayoutResId() {
        int resId = this.initChildLayoutResId();
        if (resId < 1) {
            return R.layout.fw_layout_slidingtablayout_viewpager;
        }
        return resId;
    }

    @Override
    protected void initView() {
        //初始化变量
        this.mTabFragments = new ArrayList<>();
        this.mTabTitles = new ArrayList<>();
        //初始化控件
        llt_page_container = findViewById(R.id.llt_page_container);
        stl_tabs = findViewById(R.id.stl_tabs);
        vpr_pages = findViewById(R.id.vpr_pages);
        this.initLayoutStyle(this.llt_page_container, this.stl_tabs);
        vpr_pages.setAdapter(getPagerAdapter(getSupportFragmentManager()));
        stl_tabs.setViewPager(vpr_pages);
        this.initChildView();
    }

    @Override
    protected void initData() {
        this.initChildData();
    }

    @Override
    protected void initEvent() {
        this.initChildEvent();
    }

    protected abstract PagerAdapter getPagerAdapter(FragmentManager fragmentManager);

    protected abstract int initChildLayoutResId();

    protected abstract void initLayoutStyle(LinearLayout pageContainer, SlidingTabLayout tabLayout);

    protected abstract void initChildView();

    protected abstract void initChildData();

    protected abstract void initChildEvent();
}
