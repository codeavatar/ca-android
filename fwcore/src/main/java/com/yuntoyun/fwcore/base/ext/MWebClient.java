package com.yuntoyun.fwcore.base.ext;
/*
 * +----------------------------------------------------------------------
 * | @Author: codeavatar   @Year：2021
 * +----------------------------------------------------------------------
 * | @Email: codeavatar@aliyun.com
 * +----------------------------------------------------------------------
 */
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Build;
import android.text.TextUtils;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import androidx.annotation.RequiresApi;

import com.yuntoyun.fwcore.util.FwDialogUtil;
import com.yuntoyun.fwcore.util.FwLogUtil;

/**
 * WebViewClient主要帮助WebView处理各种通知、请求事件的，
 */
public class MWebClient extends WebViewClient {

    private final String fTag = MWebClient.class.getSimpleName();

    private Context context;

    public MWebClient(Context context) {
        this.context = context;
    }


    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
        String url = request.getUrl().toString();
        try {
            if (!MWebViewUtil.intentView(url, context)) {
                view.loadUrl(url);
            }
            return true;
        } catch (Exception e) {

        }
        return false;
    }

    @Override
    public boolean shouldOverrideUrlLoading(WebView view, String url) {
        FwLogUtil.info(fTag, "shouldOverrideUrlLoading》url》=" + url);

        try {
            if (!MWebViewUtil.intentView(url, context)) {
                view.loadUrl(url);
            }
            return true;
        } catch (Exception e) {

        }
        return false;
    }

    @Override
    public void onPageFinished(WebView view, String url) {
        FwLogUtil.info(fTag, "onPageFinished》url》=" + url);

        FwDialogUtil.dismissLoading();
        super.onPageFinished(view, url);

    }

    @Override
    public void onPageStarted(WebView view, String url, Bitmap favicon) {
        FwLogUtil.info(fTag, "onPageStarted》url》=" + url);

//        FwDialogUtil.showLoading(context, "", true);

        //WebUrl处理器
        doUrlHandler(url);
        super.onPageStarted(view, url, favicon);
    }

    @Override
    public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
        FwDialogUtil.dismissLoading();
//        FwToastUtil.builder().makeText("请检查网络链接!");
        super.onReceivedError(view, request, error);
    }

    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    //域名处理
    private void doUrlHandler(String url) {
        if (!TextUtils.isEmpty(url)) {
            if (-1 != url.indexOf("www.domain1.com")) {

            } else if (-1 != url.indexOf("www.domain2.com")) {

            } else {

            }
        }
    }
}
