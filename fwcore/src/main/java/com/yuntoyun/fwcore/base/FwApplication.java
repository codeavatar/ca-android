package com.yuntoyun.fwcore.base;
/*
 * +----------------------------------------------------------------------
 * | @Author: codeavatar   @Year：2021
 * +----------------------------------------------------------------------
 * | @Email: codeavatar@aliyun.com
 * +----------------------------------------------------------------------
 */
import android.app.Activity;
import android.content.Context;
import android.os.Bundle;

import androidx.multidex.MultiDexApplication;

import com.squareup.leakcanary.LeakCanary;
import com.yuntoyun.fwcore.config.FwAppConfig;
import com.yuntoyun.fwcore.config.FwIConfig;
import com.yuntoyun.fwcore.util.FwLogUtil;


/**
 * 支持多dex文件的应用程序
 */
public class FwApplication extends MultiDexApplication {

    private final String TAG = FwApplication.class.getSimpleName();

    //系统应用上下文
    private static Context appContext;

    private static FwIConfig appConfig;

    /**
     * 获取应用上下文（只读属性）
     *
     * @return
     */
    public static Context getAppContext() {
        return appContext;
    }

    /**
     * 获取应用配置
     *
     * @return
     */
    public static FwIConfig getAppConfig() {
        return appConfig;
    }

    /**
     * 当应用程序启动时执行服务
     */
    private void initStartService() {
        //推送
        this.initJpush();
        //分享
        this.initShareSdk();
//        //内存泻漏检测
//        this.initLeakCanary();
    }

    /**
     * Activity运行回调
     */
    private ActivityLifecycleCallbacks leftCycleCallbacks = new ActivityLifecycleCallbacks() {

        @Override
        public void onActivityCreated(Activity activity, Bundle savedInstanceState) {
            FwLogUtil.info("ActivityLifCycle", String.format("名称为%s的Activity已创建！", activity
                    .getLocalClassName()));
        }

        @Override
        public void onActivityStarted(Activity activity) {
            FwLogUtil.info("ActivityLifCycle", String.format("名称为%s的Activity已启动！", activity
                    .getLocalClassName()));
        }

        @Override
        public void onActivityResumed(Activity activity) {
            FwLogUtil.info("ActivityLifCycle", String.format("名称为%s的Activity已恢复！", activity
                    .getLocalClassName()));
//            if (!(activity instanceof WelcomeActivity)) {
//                CharSequence clipboardText = FwClipboardUtil.builder().getText();
//                FwLogUtil.info(TAG, "[ClipboardText]" + clipboardText);
//                if (!TextUtils.isEmpty(clipboardText)) {
//                    String clipText = clipboardText.toString();
//                    if (clipText.indexOf("http://") != -1 || clipText.indexOf("https://") != -1) {
//                        MainActivity mainActivity = MainActivity.getInstance();
//                        if (null != mainActivity) {
//                            TaobaoPwdDialog appTbDiaog = new TaobaoPwdDialog(activity, mainActivity.getFwObserverList());
//                            appTbDiaog.doAnalysisTbpwd(clipText);
//                            FwClipboardUtil.builder().removeText();
//                        }
//                    }
//                } else {
//                    MainActivity mainActivity = MainActivity.getInstance();
//                    if (null != mainActivity) {
//                        mainActivity.getWindow().getDecorView().postDelayed(new Runnable() {
//                            @Override
//                            public void run() {
//                                CharSequence clipboardText = FwClipboardUtil.builder().getText(mainActivity.getApplicationContext());
//                                FwLogUtil.info(TAG, "[ClipboardText]" + clipboardText);
//                                if (!TextUtils.isEmpty(clipboardText)) {
//                                    String clipText = clipboardText.toString();
//                                    if (clipText.indexOf("http://") != -1 || clipText.indexOf("https://") != -1) {
//                                        TaobaoPwdDialog appTbDiaog = new TaobaoPwdDialog(activity, mainActivity.getFwObserverList());
//                                        appTbDiaog.doAnalysisTbpwd(clipText);
//                                        FwClipboardUtil.builder().removeText(mainActivity.getApplicationContext());
//                                    }
//                                }
//                            }
//                        }, 1000);
//                    }
//                }
//            }
        }

        @Override
        public void onActivityPaused(Activity activity) {
            FwLogUtil.info("ActivityLifCycle", String.format("名称为%s的Activity已暂停！", activity
                    .getLocalClassName()));
        }

        @Override
        public void onActivityStopped(Activity activity) {
            FwLogUtil.info("ActivityLifCycle", String.format("名称为%s的Activity已停止！", activity
                    .getLocalClassName()));
        }

        @Override
        public void onActivitySaveInstanceState(Activity activity, Bundle outState) {
            FwLogUtil.info("ActivityLifCycle", String.format("名称为%s的Activity初始状态！", activity
                    .getLocalClassName()));
        }

        @Override
        public void onActivityDestroyed(Activity activity) {
            FwLogUtil.info("ActivityLifCycle", String.format("名称为%s的Activity已销毁！", activity
                    .getLocalClassName()));
        }
    };

    //++++++++++++++++++++++++++++++++++++++
    //++ Application 的生命周期
    //++++++++++++++++++++++++++++++++++++++

    // 程序创建的时候执行
    @Override
    public void onCreate() {
        super.onCreate();
        //实例化应用上下文
        appContext = getApplicationContext();
        //运行子应用
        this.initChildApp();
        //Activity生成周期回调事件注册
        registerActivityLifecycleCallbacks(leftCycleCallbacks);
        //运行服务
        this.initStartService();
    }

    // 程序低内存的时候执行
    @Override
    public void onLowMemory() {
        super.onLowMemory();
    }

    // 程序内存清理的时候执行
    @Override
    public void onTrimMemory(int level) {
        super.onTrimMemory(level);
    }

    // 程序终止的时候执行
    @Override
    public void onTerminate() {
        super.onTerminate();
    }

    //++++++++++++++++++++++++++++++++++++++
    //++ 需要实现在的方法
    //++++++++++++++++++++++++++++++++++++++

    /**
     * 运行子应用代码（使用方法，子类重写些方法）
     */
    protected void initChildApp() {
        //仅用于子类重写，此处无逻辑代码
    }

    /**
     * 子类必须调用该方法
     *
     * @param iconfig
     */
    protected synchronized void setAppConfig(FwIConfig iconfig) {
        if (null != appConfig)
            return;
        appConfig = iconfig;
    }

    //++++++++++++++++++++++++++++++++++++++
    //++ 当前应用第三方服务
    //++++++++++++++++++++++++++++++++++++++

    /**
     * 启动极光推送服务
     */
    private void initJpush() {
//        if (FwAppConfig.JPUSH_LAUNCH) {
//            PlugJpush.builder().doInit(appContext);
//        }
    }

    /**
     * 启动ShareSDK服务
     */
    private void initShareSdk() {
//        MobSDK.init(this);
    }

    /**
     * 内存泻漏检测
     */
    private void initLeakCanary() {
        if (FwAppConfig.PRINT_LOG_SWITCH) {
            if (LeakCanary.isInAnalyzerProcess(this)) {
                return;
            }
            LeakCanary.install(this);
        }
    }

}
