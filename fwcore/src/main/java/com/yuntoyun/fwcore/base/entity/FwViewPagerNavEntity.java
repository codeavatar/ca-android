package com.yuntoyun.fwcore.base.entity;
/*
 * +----------------------------------------------------------------------
 * | @Author: codeavatar   @Year：2021
 * +----------------------------------------------------------------------
 * | @Email: codeavatar@aliyun.com
 * +----------------------------------------------------------------------
 */
import com.flyco.tablayout.listener.CustomTabEntity;

/**
 * FwBaseViewPagerNavActivity 使用的实体
 */
public class FwViewPagerNavEntity implements CustomTabEntity {

    private String gTabtitle;
    private int gTabSelectedIcon;
    private int gTabUnselectedIncon;

    public FwViewPagerNavEntity(String title, int selectedIcon, int unselectedIcon) {
        this.gTabtitle = title;
        this.gTabSelectedIcon = selectedIcon;
        this.gTabUnselectedIncon = unselectedIcon;
    }

    @Override
    public String getTabTitle() {
        return this.gTabtitle;
    }

    @Override
    public int getTabSelectedIcon() {
        return this.gTabSelectedIcon;
    }

    @Override
    public int getTabUnselectedIcon() {
        return this.gTabUnselectedIncon;
    }
}
