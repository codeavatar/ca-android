package com.yuntoyun.fwcore.base;
/*
 * +----------------------------------------------------------------------
 * | @Author: codeavatar   @Year：2021
 * +----------------------------------------------------------------------
 * | @Email: codeavatar@aliyun.com
 * +----------------------------------------------------------------------
 */
import android.graphics.Color;
import android.view.View;

import com.yuntoyun.fwcore.R;
import com.yuntoyun.fwcore.config.FwAppConfig;

import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

/**
 * 应用 SwipeRefreshLayout + ViewPager 组件 Fragment 基础类
 */
public abstract class FwBaseSwipeRefreshViewPagerFragment extends FwBaseViewPagerFragment implements SwipeRefreshLayout.OnRefreshListener {

    protected SwipeRefreshLayout srl_swipeRefreshLayout;

    @Override
    protected void initView(View layoutView) {
        //初始化控制
        srl_swipeRefreshLayout = layoutView.findViewById(R.id.srl_swipeRefreshLayout);

        srl_swipeRefreshLayout.setOnRefreshListener(this);
        srl_swipeRefreshLayout.setColorSchemeColors(Color.rgb(47, 223, 189));
        //加载数据
        srl_swipeRefreshLayout.setRefreshing(true);
        srl_swipeRefreshLayout.postDelayed(new Runnable() {
            @Override
            public void run() {
                onRefresh();
            }
        }, FwAppConfig.RECYCLERVIEW_DO_TIMEOUT);
        //初始子视图
        initSwipeView(layoutView);
    }

    @Override
    public void onRefresh() {
        this.reloadData();
        this.srl_swipeRefreshLayout.setRefreshing(false);
    }

    protected abstract void initSwipeView(View layoutView);

    protected abstract void reloadData();
}
