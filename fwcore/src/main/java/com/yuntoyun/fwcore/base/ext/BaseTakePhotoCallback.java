package com.yuntoyun.fwcore.base.ext;
/*
 * +----------------------------------------------------------------------
 * | @Author: codeavatar   @Year：2021
 * +----------------------------------------------------------------------
 * | @Email: codeavatar@aliyun.com
 * +----------------------------------------------------------------------
 */
import android.net.Uri;

import java.util.List;

/**
 * TaskPhoto回调工具类
 */
public interface BaseTakePhotoCallback {

    void takePhotoSingleCallback(Uri uri);

    void takePhotoMultipleCallback(List<Uri> listUri, List<String> listPath);
}
