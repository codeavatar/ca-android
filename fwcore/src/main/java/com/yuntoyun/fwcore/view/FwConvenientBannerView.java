package com.yuntoyun.fwcore.view;
/*
 * +----------------------------------------------------------------------
 * | @Author: codeavatar   @Year：2021
 * +----------------------------------------------------------------------
 * | @Email: codeavatar@aliyun.com
 * +----------------------------------------------------------------------
 */
import android.content.Context;
import android.util.AttributeSet;

import com.bigkoo.convenientbanner.ConvenientBanner;

/**
 * 轮播Banner
 *
 * src:https://github.com/saiwu-bigkoo/Android-ConvenientBanner
 *
 * @param <T>
 */
public class FwConvenientBannerView<T> extends ConvenientBanner<T> {

    public FwConvenientBannerView(Context context) {
        super(context);
    }

    public FwConvenientBannerView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }
}
