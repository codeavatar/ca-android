package com.yuntoyun.fwcore.view;
/*
 * +----------------------------------------------------------------------
 * | @Author: codeavatar   @Year：2021
 * +----------------------------------------------------------------------
 * | @Email: codeavatar@aliyun.com
 * +----------------------------------------------------------------------
 */
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.Window;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.yuntoyun.fwcore.R;

/**
 * 加载提示工具类
 */
public class FwDialogLoadingView extends Dialog {

    private Context context;
    private Handler myHandler = new Handler();
    private ImageView ivwLoading;
    private boolean toggle = true;

    public FwDialogLoadingView(Context context) {
        super(context, R.style.fw_dialog_loading);
        this.context = context;
    }

    public void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        requestWindowFeature(Window.PROGRESS_VISIBILITY_ON);

        super.onCreate(savedInstanceState);
        ivwLoading = new ImageView(context);
        ivwLoading.setImageResource(R.mipmap.fw_dialog_loading1);
        FrameLayout.LayoutParams lp = new FrameLayout.LayoutParams(
                FrameLayout.LayoutParams.MATCH_PARENT,
                FrameLayout.LayoutParams.WRAP_CONTENT);
        lp.gravity = Gravity.CENTER;
        addContentView(ivwLoading, lp);
        setCanceledOnTouchOutside(false);

//		Animation operatingAnim = AnimationUtils.loadAnimation(context,
//				R.drawable.action_rotate);
//		LinearInterpolator lin = new LinearInterpolator(); // LinearInterpolator为匀速效果
//		operatingAnim.setInterpolator(lin);
//		ivwLoading.startAnimation(operatingAnim);

    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            this.hide();
            return true;
        }
        return true;
    }

    @Override
    public void show() {
        try {
            super.show();
            myHandler.postDelayed(runnable, 0);
        } catch (Exception e) {

        }
    }

    @Override
    public void dismiss() {
        try {
            super.dismiss();
            myHandler.removeCallbacks(runnable);
        } catch (Exception e) {

        }

    }

    Runnable runnable = new Runnable() {
        @Override
        public void run() {
            if (toggle) {
                ivwLoading.setImageResource(R.mipmap.fw_dialog_loading1);
            } else {
                ivwLoading.setImageResource(R.mipmap.fw_dialog_loading2);
            }
            toggle = !toggle;
            myHandler.postDelayed(runnable, 100);

        }
    };
}
