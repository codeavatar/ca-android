package com.yuntoyun.fwcore.view;
/*
 * +----------------------------------------------------------------------
 * | @Author: codeavatar   @Year：2021
 * +----------------------------------------------------------------------
 * | @Email: codeavatar@aliyun.com
 * +----------------------------------------------------------------------
 */
import android.content.Context;
import android.util.AttributeSet;
import android.widget.ListView;

/**
 * 兼容ScrollView滑动ListView
 */
public class FwListView extends ListView {
    public FwListView(Context context) {
        super(context);
    }

    public FwListView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public FwListView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int expandSpec=MeasureSpec.makeMeasureSpec(Integer.MAX_VALUE>>2,MeasureSpec.AT_MOST);
        super.onMeasure(widthMeasureSpec, expandSpec);
    }
}
