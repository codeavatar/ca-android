package com.yuntoyun.fwcore.view;
/*
 * +----------------------------------------------------------------------
 * | @Author: codeavatar   @Year：2021
 * +----------------------------------------------------------------------
 * | @Email: codeavatar@aliyun.com
 * +----------------------------------------------------------------------
 */
import android.content.Context;
import android.util.AttributeSet;
import android.widget.GridView;

/**
 * 兼容ScrollView滑动GridView
 */
public class FwGridView extends GridView {
    public FwGridView(Context context) {
        super(context);
    }

    public FwGridView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public FwGridView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int expandSpec = MeasureSpec.makeMeasureSpec(Integer.MAX_VALUE >> 2, MeasureSpec.AT_MOST);
        super.onMeasure(widthMeasureSpec, expandSpec);
    }
}
