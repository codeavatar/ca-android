package com.yuntoyun.fwcore.view;
/*
 * +----------------------------------------------------------------------
 * | @Author: codeavatar   @Year：2021
 * +----------------------------------------------------------------------
 * | @Email: codeavatar@aliyun.com
 * +----------------------------------------------------------------------
 */
import android.content.Context;
import android.util.AttributeSet;
import android.widget.ScrollView;

public class FwScrollView extends ScrollView {

    private OnScrollVerticalChangedListener scrollVerticalChangedListener;

    public FwScrollView(Context context) {
        super(context);
    }

    public FwScrollView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public FwScrollView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    /**
     * @param l    Current horizontal scroll origin.
     * @param t    Current vertical scroll origin.
     * @param oldl Previous horizontal scroll origin.
     * @param oldt Previous vertical scroll origin.
     */
    @Override
    protected void onScrollChanged(int l, int t, int oldl, int oldt) {
        super.onScrollChanged(l, t, oldl, oldt);
        if (t != oldt && null != scrollVerticalChangedListener) {
            scrollVerticalChangedListener.onScrollVerticalChangedListener(t);
        }
    }

    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


    //==============================================================================

    public void setScrollVerticalChangedListener(OnScrollVerticalChangedListener scrollVerticalChangedListener) {
        this.scrollVerticalChangedListener = scrollVerticalChangedListener;
    }

    public interface OnScrollVerticalChangedListener {
        void onScrollVerticalChangedListener(int currentY);
    }
}
