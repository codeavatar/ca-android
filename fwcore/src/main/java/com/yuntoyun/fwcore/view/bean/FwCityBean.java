package com.yuntoyun.fwcore.view.bean;

/*
 * +----------------------------------------------------------------------
 * | @Author: codeavatar   @Year：2021
 * +----------------------------------------------------------------------
 * | @Email: codeavatar@aliyun.com
 * +----------------------------------------------------------------------
 */
public class FwCityBean {

    private String name;
    private String cityCode;
    private String pinyin;
    private boolean showLetter;

    public FwCityBean(String name, String cityCode) {
        this.name = name;
        this.cityCode = cityCode;
    }

    public FwCityBean(String name, String cityCode, String pinyin) {
        this.name = name;
        this.cityCode = cityCode;
        this.pinyin = pinyin;
    }

    public FwCityBean(String name, String cityCode, String pinyin, boolean showLetter) {
        this.name = name;
        this.cityCode = cityCode;
        this.pinyin = pinyin;
        this.showLetter = showLetter;
    }

    public FwCityBean() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPinyin() {
        return pinyin;
    }

    public String getCityCode() {
        return cityCode;
    }

    public void setCityCode(String cityCode) {
        this.cityCode = cityCode;
    }

    public void setPinyin(String pinyin) {
        this.pinyin = pinyin;
    }

    public boolean isShowLetter() {
        return showLetter;
    }

    public void setShowLetter(boolean showLetter) {
        this.showLetter = showLetter;
    }
}
