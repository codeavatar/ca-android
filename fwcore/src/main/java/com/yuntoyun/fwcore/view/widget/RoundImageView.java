package com.yuntoyun.fwcore.view.widget;
/*
 * +----------------------------------------------------------------------
 * | @Author: codeavatar   @Year：2021
 * +----------------------------------------------------------------------
 * | @Email: codeavatar@aliyun.com
 * +----------------------------------------------------------------------
 */
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Path;
import android.os.Build;
import android.util.AttributeSet;
import android.view.View;

import androidx.appcompat.widget.AppCompatImageView;

import com.yuntoyun.fwcore.R;

public class RoundImageView extends AppCompatImageView {

    private float borderRadius = 0;
    private float borderTopRadius = 0;
    private float borderBottomRadius = 0;
    private float borderTopLeftRadius = 0;
    private float borderTopRightRadius = 0;
    private float borderBottomLeftRadius = 0;
    private float borderBottomRightRadius = 0;

    private float width = 0;
    private float height = 0;

    public RoundImageView(Context context) {
        this(context, null);
        init(context, null);
    }

    public RoundImageView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
        init(context, attrs);
    }

    public RoundImageView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    private void init(Context context, AttributeSet attrs) {
        if (Build.VERSION.SDK_INT < 18) {
            setLayerType(View.LAYER_TYPE_SOFTWARE, null);
        }

        TypedArray typedArray = getContext().obtainStyledAttributes(attrs, R.styleable.RoundImageView);
        borderRadius = typedArray.getDimension(R.styleable.RoundImageView_rivw_borderRadius, borderRadius);
        borderTopRadius = typedArray.getDimension(R.styleable.RoundImageView_rivw_borderTopRadius, borderTopRadius);
        borderBottomRadius = typedArray.getDimension(R.styleable.RoundImageView_rivw_borderBottomRadius, borderBottomRadius);
        borderTopLeftRadius = typedArray.getDimension(R.styleable.RoundImageView_rivw_borderTopLeftRadius, borderTopLeftRadius);
        borderTopRightRadius = typedArray.getDimension(R.styleable.RoundImageView_rivw_borderTopRightRadius, borderTopRightRadius);
        borderBottomLeftRadius = typedArray.getDimension(R.styleable.RoundImageView_rivw_borderBottomLeftRadius, borderBottomLeftRadius);
        borderBottomRightRadius = typedArray.getDimension(R.styleable.RoundImageView_rivw_borderBottomRightRadius, borderBottomRightRadius);
        typedArray.recycle();
    }

    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        super.onLayout(changed, left, top, right, bottom);
        width = getWidth();
        height = getHeight();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        //这里做下判断，只有图片的宽高大于设置的圆角距离的时候才进行裁剪

        if (borderRadius > 0) {
            //统一圆角
            if (width >= borderRadius && height > borderRadius) {
                Path path = new Path();
                //四个圆角
                path.moveTo(borderRadius, 0);
                //右上
                path.lineTo(width - borderRadius, 0);
                path.quadTo(width, 0, width, borderRadius);
                //右下
                path.lineTo(width, height - borderRadius);
                path.quadTo(width, height, width - borderRadius, height);
                //左下
                path.lineTo(borderRadius, height);
                path.quadTo(0, height, 0, height - borderRadius);
                //左上
                path.lineTo(0, borderRadius);
                path.quadTo(0, 0, borderRadius, 0);

                canvas.clipPath(path);
            }
        } else if (borderTopRadius > 0 || borderBottomRadius > 0) {
            Path path = new Path();
            //四个圆角
            path.moveTo(borderTopRadius, 0);
            //右上
            path.lineTo(width - borderTopRadius, 0);
            path.quadTo(width, 0, width, borderTopRadius);
            //右下
            path.lineTo(width, height - borderBottomRadius);
            path.quadTo(width, height, width - borderBottomRadius, height);
            //左下
            path.lineTo(borderBottomRadius, height);
            path.quadTo(0, height, 0, height - borderBottomRadius);
            //左上
            path.lineTo(0, borderTopRadius);
            path.quadTo(0, 0, borderTopRadius, 0);

            canvas.clipPath(path);
        } else if (borderTopLeftRadius > 0 || borderTopRightRadius > 0 || borderBottomLeftRadius > 0 || borderBottomRightRadius > 0) {
            float maxLeft = Math.max(borderTopLeftRadius, borderBottomLeftRadius);
            float maxRight = Math.max(borderTopRightRadius, borderBottomRightRadius);
            float minWidth = maxLeft + maxRight;
            float maxTop = Math.max(borderTopLeftRadius, borderTopRightRadius);
            float maxBottom = Math.max(borderBottomLeftRadius, borderBottomRightRadius);
            float minHeight = maxTop + maxBottom;
            if (width >= minWidth && height > minHeight) {
                Path path = new Path();
                //四个角：右上，右下，左下，左上
                path.moveTo(borderTopLeftRadius, 0);
                path.lineTo(width - borderTopRightRadius, 0);
                path.quadTo(width, 0, width, borderTopRightRadius);

                path.lineTo(width, height - borderBottomRightRadius);
                path.quadTo(width, height, width - borderBottomRightRadius, height);

                path.lineTo(borderBottomLeftRadius, height);
                path.quadTo(0, height, 0, height - borderBottomLeftRadius);

                path.lineTo(0, borderTopLeftRadius);
                path.quadTo(0, 0, borderTopLeftRadius, 0);

                canvas.clipPath(path);
            }
        }
        super.onDraw(canvas);
    }

}
