package com.yuntoyun.fwcore.appext.model;

/**
 * 用户数据模型
 */
public class UserModel {

    private int userId;
    private String nickname;
    private String portrait;
    private String userToken;
    private String longitude;
    private String latitude;
    private int provinceId;
    private int cityId;
    private int districtId;
    private int countyId;
    //合伙人身份：0 不是；1 普通合伙人；2 商家合伙人；3 普通合伙人-已冻结-已到期；4 商家合伙人-已冻结-已到期
    private int userAuth;
    //商户认证状态：0 认证中；1 认证通过； 2 认证失败
    private int userAuthState;

    public UserModel() {
    }

    public UserModel(int userId, String nickname, String userToken) {
        this.userId = userId;
        this.nickname = nickname;
        this.userToken = userToken;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getPortrait() {
        return portrait;
    }

    public void setPortrait(String portrait) {
        this.portrait = portrait;
    }

    public String getUserToken() {
        return userToken;
    }

    public void setUserToken(String userToken) {
        this.userToken = userToken;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public int getProvinceId() {
        return provinceId;
    }

    public void setProvinceId(int provinceId) {
        this.provinceId = provinceId;
    }

    public int getCityId() {
        return cityId;
    }

    public void setCityId(int cityId) {
        this.cityId = cityId;
    }

    public int getDistrictId() {
        return districtId;
    }

    public void setDistrictId(int districtId) {
        this.districtId = districtId;
    }

    public int getCountyId() {
        return countyId;
    }

    public void setCountyId(int countyId) {
        this.countyId = countyId;
    }

    public int getUserAuth() {
        return userAuth;
    }

    public void setUserAuth(int userAuth) {
        this.userAuth = userAuth;
    }

    public int getUserAuthState() {
        return userAuthState;
    }

    public void setUserAuthState(int userAuthState) {
        this.userAuthState = userAuthState;
    }
}
