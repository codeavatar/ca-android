package com.yuntoyun.fwcore.appext.data;
/*
 * +----------------------------------------------------------------------
 * | @Author: codeavatar   @Year：2021
 * +----------------------------------------------------------------------
 * | @Email: codeavatar@aliyun.com
 * +----------------------------------------------------------------------
 */
import com.google.gson.JsonObject;
import com.yuntoyun.fwcore.config.FwConstConfig;
import com.yuntoyun.fwcore.util.FwSharedPreferencesUtil;

import java.util.Map;
import java.util.Set;

/**
 * 缓存应用数据
 */
public class FwSPSetUtil {

    //存储应用临时数据
    public static final String gShareFile = FwConstConfig.CONST_SHAREDATA_NAME;
    //存储用户私有数据
    public static final String gPrivateFile = FwConstConfig.CONST_PRIVATEDATA_NAME;
    //存储用户权限数据
    public static final String gAuthFile = FwConstConfig.CONST_AUTHDATA_NAME;
    //存储用户登录状态的公用数据
    public static final String gPostFile = FwConstConfig.CONST_POSTDATA_NAME;

    //==============================================================================================

    /////////////////////////////////////////////////////////////////
    ///++++管理临时缓存数据 开始[gShareFile]
    /////////////////////////////////////////////////////////////////

    public static boolean setShareString(String key, String value) {
        FwSharedPreferencesUtil util = FwSharedPreferencesUtil.builder();
        return util.setString(key, value, gShareFile);
    }

    public static boolean setShareInt(String key, int value) {
        FwSharedPreferencesUtil util = FwSharedPreferencesUtil.builder();
        return util.setInt(key, value, gShareFile);
    }

    public static boolean setShareBoolean(String key, boolean value) {
        FwSharedPreferencesUtil util = FwSharedPreferencesUtil.builder();
        return util.setBoolean(key, value, gShareFile);
    }

    public static boolean setShareFloat(String key, float value) {
        FwSharedPreferencesUtil util = FwSharedPreferencesUtil.builder();
        return util.setFloat(key, value, gShareFile);
    }

    public static boolean setShareLong(String key, long value) {
        FwSharedPreferencesUtil util = FwSharedPreferencesUtil.builder();
        return util.setLong(key, value, gShareFile);
    }

    public static boolean setShareStringSet(String key, Set<String> value) {
        FwSharedPreferencesUtil util = FwSharedPreferencesUtil.builder();
        return util.setStringSet(key, value, gShareFile);
    }

    public static String getShareString(String key) {
        FwSharedPreferencesUtil util = FwSharedPreferencesUtil.builder();
        return util.getString(key, gShareFile);
    }

    public static int getShareInt(String key) {
        FwSharedPreferencesUtil util = FwSharedPreferencesUtil.builder();
        return util.getInt(key, gShareFile);
    }

    public static boolean getShareBoolean(String key) {
        FwSharedPreferencesUtil util = FwSharedPreferencesUtil.builder();
        return util.getBoolean(key, gShareFile);
    }

    public static float getShareFloat(String key) {
        FwSharedPreferencesUtil util = FwSharedPreferencesUtil.builder();
        return util.getFloat(key, gShareFile);
    }

    public static long getShareLong(String key) {
        FwSharedPreferencesUtil util = FwSharedPreferencesUtil.builder();
        return util.getLong(key, gShareFile);
    }

    public static Set<String> getShareStringSet(String key) {
        FwSharedPreferencesUtil util = FwSharedPreferencesUtil.builder();
        return util.getStringSet(key, gShareFile);
    }

    /**
     * 缓存应用临时数据
     *
     * @param jo
     * @return
     */
    public static boolean setShareData(JsonObject jo) {
        FwSharedPreferencesUtil util = FwSharedPreferencesUtil.builder();
        return util.setSharedPreferencesData(jo, gShareFile);
    }

    /**
     * 缓存应用临时数据
     *
     * @param map
     * @return
     */
    public static boolean setShareData(Map<String, Object> map) {
        FwSharedPreferencesUtil util = FwSharedPreferencesUtil.builder();
        return util.setSharedPreferencesData(map, gShareFile);
    }

    /**
     * 缓存应用临时数据
     *
     * @param key
     * @param value
     * @return
     */
    public static boolean setShareData(String key, Set<String> value) {
        FwSharedPreferencesUtil util = FwSharedPreferencesUtil.builder();
        return util.setStringSet(key, value, gShareFile);
    }

    /////////////////////////////////////////////////////////////////
    ///++++管理临时缓存数据 结束[gShareFile]
    /////////////////////////////////////////////////////////////////

    //==============================================================================================

    /////////////////////////////////////////////////////////////////
    ///++++管理用户私有数据 开始[gPrivateFile]
    /////////////////////////////////////////////////////////////////

    public static boolean setPrivateString(String key, String value) {
        FwSharedPreferencesUtil util = FwSharedPreferencesUtil.builder();
        return util.setString(key, value, gPrivateFile);
    }

    public static boolean setPrivateInt(String key, int value) {
        FwSharedPreferencesUtil util = FwSharedPreferencesUtil.builder();
        return util.setInt(key, value, gPrivateFile);
    }

    public static boolean setPrivateBoolean(String key, boolean value) {
        FwSharedPreferencesUtil util = FwSharedPreferencesUtil.builder();
        return util.setBoolean(key, value, gPrivateFile);
    }

    public static boolean setPrivateFloat(String key, float value) {
        FwSharedPreferencesUtil util = FwSharedPreferencesUtil.builder();
        return util.setFloat(key, value, gPrivateFile);
    }

    public static boolean setPrivateLong(String key, long value) {
        FwSharedPreferencesUtil util = FwSharedPreferencesUtil.builder();
        return util.setLong(key, value, gPrivateFile);
    }

    public static boolean setPrivateStringSet(String key, Set<String> value) {
        FwSharedPreferencesUtil util = FwSharedPreferencesUtil.builder();
        return util.setStringSet(key, value, gPrivateFile);
    }

    public static String getPrivateString(String key) {
        FwSharedPreferencesUtil util = FwSharedPreferencesUtil.builder();
        return util.getString(key, gPrivateFile);
    }

    public static int getPrivateInt(String key) {
        FwSharedPreferencesUtil util = FwSharedPreferencesUtil.builder();
        return util.getInt(key, gPrivateFile);
    }

    public static boolean getPrivateBoolean(String key) {
        FwSharedPreferencesUtil util = FwSharedPreferencesUtil.builder();
        return util.getBoolean(key, gPrivateFile);
    }

    public static float getPrivateFloat(String key) {
        FwSharedPreferencesUtil util = FwSharedPreferencesUtil.builder();
        return util.getFloat(key, gPrivateFile);
    }

    public static long getPrivateLong(String key) {
        FwSharedPreferencesUtil util = FwSharedPreferencesUtil.builder();
        return util.getLong(key, gPrivateFile);
    }

    public static Set<String> getPrivateStringSet(String key) {
        FwSharedPreferencesUtil util = FwSharedPreferencesUtil.builder();
        return util.getStringSet(key, gPrivateFile);
    }

    /////////////////////////////////////////////////////////////////
    ///++++管理用户私有数据 结束[gPrivateFile]
    /////////////////////////////////////////////////////////////////

    //==============================================================================================

    /////////////////////////////////////////////////////////////////
    ///++++管理用户权限数据 开始[gAuthFile]
    /////////////////////////////////////////////////////////////////

    public static boolean setAuthString(String key, String value) {
        FwSharedPreferencesUtil util = FwSharedPreferencesUtil.builder();
        return util.setString(key, value, gAuthFile);
    }

    public static boolean setAuthInt(String key, int value) {
        FwSharedPreferencesUtil util = FwSharedPreferencesUtil.builder();
        return util.setInt(key, value, gAuthFile);
    }

    public static boolean setAuthBoolean(String key, boolean value) {
        FwSharedPreferencesUtil util = FwSharedPreferencesUtil.builder();
        return util.setBoolean(key, value, gAuthFile);
    }

    public static boolean setAuthFloat(String key, float value) {
        FwSharedPreferencesUtil util = FwSharedPreferencesUtil.builder();
        return util.setFloat(key, value, gAuthFile);
    }

    public static boolean setAuthLong(String key, long value) {
        FwSharedPreferencesUtil util = FwSharedPreferencesUtil.builder();
        return util.setLong(key, value, gAuthFile);
    }

    public static boolean setAuthStringSet(String key, Set<String> value) {
        FwSharedPreferencesUtil util = FwSharedPreferencesUtil.builder();
        return util.setStringSet(key, value, gAuthFile);
    }

    public static String getAuthString(String key) {
        FwSharedPreferencesUtil util = FwSharedPreferencesUtil.builder();
        return util.getString(key, gAuthFile);
    }

    public static int getAuthInt(String key) {
        FwSharedPreferencesUtil util = FwSharedPreferencesUtil.builder();
        return util.getInt(key, gAuthFile);
    }

    public static boolean getAuthBoolean(String key) {
        FwSharedPreferencesUtil util = FwSharedPreferencesUtil.builder();
        return util.getBoolean(key, gAuthFile);
    }

    public static float getAuthFloat(String key) {
        FwSharedPreferencesUtil util = FwSharedPreferencesUtil.builder();
        return util.getFloat(key, gAuthFile);
    }

    public static long getAuthLong(String key) {
        FwSharedPreferencesUtil util = FwSharedPreferencesUtil.builder();
        return util.getLong(key, gAuthFile);
    }

    public static Set<String> getAuthStringSet(String key) {
        FwSharedPreferencesUtil util = FwSharedPreferencesUtil.builder();
        return util.getStringSet(key, gAuthFile);
    }


    /**
     * 缓存应用临时数据
     *
     * @param key
     * @param value
     * @return
     */
    public static boolean setAuthData(String key, String value) {
        FwSharedPreferencesUtil util = FwSharedPreferencesUtil.builder();
        return util.setString(key, value, gAuthFile);
    }

    /**
     * 缓存应用临时数据
     *
     * @param jo
     * @return
     */
    public static boolean setAuthData(JsonObject jo) {
        FwSharedPreferencesUtil util = FwSharedPreferencesUtil.builder();
        return util.setSharedPreferencesData(jo, gAuthFile);
    }

    /**
     * 缓存应用临时数据
     *
     * @param map
     * @return
     */
    public static boolean setAuthData(Map<String, Object> map) {
        FwSharedPreferencesUtil util = FwSharedPreferencesUtil.builder();
        return util.setSharedPreferencesData(map, gAuthFile);
    }

    /**
     * 缓存应用临时数据
     *
     * @param key
     * @param value
     * @return
     */
    public static boolean setAuthData(String key, Set<String> value) {
        FwSharedPreferencesUtil util = FwSharedPreferencesUtil.builder();
        return util.setStringSet(key, value, gAuthFile);
    }

    /////////////////////////////////////////////////////////////////
    ///++++管理用户权限数据 结束[gAuthFile]
    /////////////////////////////////////////////////////////////////

    //==============================================================================================

    /////////////////////////////////////////////////////////////////
    ///++++清除缓存数据 开始
    /////////////////////////////////////////////////////////////////

    public static void removeShareData(String key) {
        FwSharedPreferencesUtil.builder().clearSharedPreferencesData(key, gShareFile);
    }

    public static void removePrivateData(String key) {
        FwSharedPreferencesUtil.builder().clearSharedPreferencesData(key, gPrivateFile);
    }

    public static void removeAuthData(String key) {
        FwSharedPreferencesUtil.builder().clearSharedPreferencesData(key, gPostFile);
    }

    public static void removePostData(String key) {
        FwSharedPreferencesUtil.builder().clearSharedPreferencesData(key, gPostFile);
    }

    public static void clearAllData() {
        FwSharedPreferencesUtil.builder().clearSharedPreferencesData(gShareFile);
        FwSharedPreferencesUtil.builder().clearSharedPreferencesData(gPrivateFile);
        FwSharedPreferencesUtil.builder().clearSharedPreferencesData(gAuthFile);
        FwSharedPreferencesUtil.builder().clearSharedPreferencesData(gPostFile);
    }

    public static void clearLoginData() {
        FwSharedPreferencesUtil.builder().clearSharedPreferencesData(gAuthFile);
        FwSharedPreferencesUtil.builder().clearSharedPreferencesData(gPostFile);
        clearCacheData();
    }

    public static void clearCacheData() {
        FwSharedPreferencesUtil.builder().clearSharedPreferencesData(gShareFile);
    }

    /////////////////////////////////////////////////////////////////
    ///++++清除缓存数据 结束
    /////////////////////////////////////////////////////////////////
}
