package com.yuntoyun.fwcore.appext.data;
/*
 * +----------------------------------------------------------------------
 * | @Author: codeavatar   @Year：2021
 * +----------------------------------------------------------------------
 * | @Email: codeavatar@aliyun.com
 * +----------------------------------------------------------------------
 */
import com.yuntoyun.fwcore.appext.model.FwCacheKeyModel;
import com.yuntoyun.fwcore.appext.model.FwUserModel;
import com.yuntoyun.fwcore.tool.auth.AuthTool;
import com.yuntoyun.fwcore.config.FwConstConfig;
import com.yuntoyun.fwcore.util.FwSharedPreferencesUtil;

import java.util.HashMap;
import java.util.Map;

/**
 * 缓存应用数据
 */
public class FwAppData extends FwSPSetUtil {

    /////////////////////////////////////////////////////////////////
    ///++++管理应用数据 开始（可无限扩展）
    /////////////////////////////////////////////////////////////////

    //////////////////////////应用数据///////////////////////////////


    public static void setAppAgreement(boolean isAgreed) {
        FwSPSetUtil.setPrivateBoolean(FwCacheKeyModel.KEY_APP_AGREEMENT, isAgreed);
    }

    public static boolean getAppAgreement() {
        return FwSPSetUtil.getPrivateBoolean(FwCacheKeyModel.KEY_APP_AGREEMENT);
    }

    public static void setAppIsFirstStart(boolean isFirst) {
        FwSPSetUtil.setPrivateBoolean(FwCacheKeyModel.KEY_APP_ISFIRST_START, isFirst);
    }

    public static String getAppIsFirstStart() {
        return FwSPSetUtil.getPrivateString(FwCacheKeyModel.KEY_APP_ISFIRST_START);
    }

    public static void setAppAuditSwitch(int flag) {
        FwSPSetUtil.setPrivateInt(FwCacheKeyModel.KEY_APP_SWITCH_AUDIT, flag);
    }

    public static boolean isAppAuditSwitch() {
        return (1 == FwSPSetUtil.getPrivateInt(FwCacheKeyModel.KEY_APP_SWITCH_AUDIT));
    }

    //////////////////////////身份数据///////////////////////////////

    public static void setAppUserId(int userId) {
        FwSPSetUtil.setAuthInt(FwCacheKeyModel.KEY_USERID, userId);
    }

    public static int getAppUserId() {
        return FwSPSetUtil.getAuthInt(FwCacheKeyModel.KEY_USERID);
    }

    public static void setAppUserToken(String token) {
        FwSPSetUtil.setAuthString(FwCacheKeyModel.KEY_USERTOKEN, token);
    }

    public static String getAppUserToken() {
        return FwSPSetUtil.getAuthString(FwCacheKeyModel.KEY_USERTOKEN);
    }

    public static void setAppNickname(String userName) {
        FwSPSetUtil.setAuthString(FwCacheKeyModel.KEY_NICKNAME, userName);
    }

    public static String getAppNickname() {
        return FwSPSetUtil.getAuthString(FwCacheKeyModel.KEY_NICKNAME);
    }

    public static void setAppUserPortrait(String userPortrait) {
        FwSPSetUtil.setAuthString(FwCacheKeyModel.KEY_PORTRAIT, userPortrait);
    }

    public static String getAppUserPortrait() {
        return FwSPSetUtil.getAuthString(FwCacheKeyModel.KEY_PORTRAIT);
    }

    public static void setAppUserAuth(int authId) {
        FwSPSetUtil.setAuthInt(FwCacheKeyModel.KEY_USERAUTH, authId);
    }

    public static int getAppUserAuth() {
        return FwSPSetUtil.getAuthInt(FwCacheKeyModel.KEY_USERAUTH);
    }

    public static void setAppUserAuthState(int authState) {
        FwSPSetUtil.setAuthInt(FwCacheKeyModel.KEY_USERAUTH_STATE, authState);
    }

    public static int getAppUserAuthState() {
        return FwSPSetUtil.getAuthInt(FwCacheKeyModel.KEY_USERAUTH_STATE);
    }

    //////////////////////////私有数据///////////////////////////////

    public static void setAppCityId(String cityId) {
        FwSPSetUtil.setPrivateString(FwCacheKeyModel.KEY_CITYID, cityId);
    }

    public static String getAppCityId() {
        return FwSPSetUtil.getPrivateString(FwCacheKeyModel.KEY_CITYID);
    }

    public static void setAppCityName(String cityName) {
        FwSPSetUtil.setPrivateString(FwCacheKeyModel.KEY_CITYNAME, cityName);
    }

    public static String getAppCityName() {
        return FwSPSetUtil.getPrivateString(FwCacheKeyModel.KEY_CITYNAME);
    }

    public static void setAppLongitude(String longitude) {
        FwSPSetUtil.setPrivateString(FwCacheKeyModel.KEY_LONGITUDE, longitude);
    }

    public static String getAppLongitude() {
        return FwSPSetUtil.getPrivateString(FwCacheKeyModel.KEY_LONGITUDE);
    }

    public static void setAppLatitude(String latitude) {
        FwSPSetUtil.setPrivateString(FwCacheKeyModel.KEY_LATITUDE, latitude);
    }

    public static String getAppLatitude() {
        return FwSPSetUtil.getPrivateString(FwCacheKeyModel.KEY_LATITUDE);
    }

    /////////////////////////////////////////////////////////////////
    ///++++管理应用数据 结束
    /////////////////////////////////////////////////////////////////


    /////////////////////////////////////////////////////////////////
    ///++++管理用户数据 开始
    /////////////////////////////////////////////////////////////////

    /**
     * 缓存用户数据
     *
     * @param model
     * @return
     */
    public static boolean setAppUserData(FwUserModel model) {
        if (null == model)
            return false;

        //批理处理数据
        FwSharedPreferencesUtil util = FwSharedPreferencesUtil.builder();
        util.setInt(FwCacheKeyModel.KEY_USERID, model.getUserId(), FwSPSetUtil.gAuthFile);
        util.setString(FwCacheKeyModel.KEY_USERTOKEN, model.getUserToken(), FwSPSetUtil.gAuthFile);
        util.setInt(FwCacheKeyModel.KEY_USERAUTH, model.getUserAuth(), FwSPSetUtil.gAuthFile);
        util.setInt(FwCacheKeyModel.KEY_USERAUTH_STATE, model.getUserAuthState(), FwSPSetUtil.gAuthFile);

        //根据需要自由增减
        return true;
    }

    /**
     * 获取用户缓存数据
     *
     * @return
     */
    public static FwUserModel getAppUserData() {
        FwSharedPreferencesUtil util = FwSharedPreferencesUtil.builder();

        if (!util.hasSharedPreferencesKey(FwCacheKeyModel.KEY_USERID, FwSPSetUtil.gAuthFile)) {
            return null;
        }

        //批理处理数据
        FwUserModel model = new FwUserModel();
        model.setUserId(util.getInt(FwCacheKeyModel.KEY_USERID, FwSPSetUtil.gAuthFile));
        model.setUserToken(util.getString(FwCacheKeyModel.KEY_USERTOKEN, FwSPSetUtil.gAuthFile));
        model.setUserAuth(util.getInt(FwCacheKeyModel.KEY_USERAUTH, FwSPSetUtil.gAuthFile));
        model.setUserAuthState(util.getInt(FwCacheKeyModel.KEY_USERAUTH_STATE, FwSPSetUtil.gAuthFile));

        //根据需要自由增减
        return model;
    }

    /////////////////////////////////////////////////////////////////
    ///++++管理用户数据 结束
    /////////////////////////////////////////////////////////////////


    /////////////////////////////////////////////////////////////////
    ///++++管理应用公用数据 开始
    /////////////////////////////////////////////////////////////////

    /**
     * 缓存用户登录状态的公用数据
     *
     * @param model
     * @return
     */
    public static boolean setAppPostData(FwUserModel model) {
        if (null == model)
            return false;

        //批理处理数据
        FwSharedPreferencesUtil util = FwSharedPreferencesUtil.builder();
        util.setInt(FwCacheKeyModel.KEY_USERID, model.getUserId(), FwSPSetUtil.gPostFile);
        util.setString(FwCacheKeyModel.KEY_USERTOKEN, model.getUserToken(), FwSPSetUtil.gPostFile);
        return true;
    }

    /**
     * 获取用户登录状态的公用数据
     *
     * @return
     */
    public static FwUserModel getAppPostData() {
        FwSharedPreferencesUtil util = FwSharedPreferencesUtil.builder();

        if (!util.hasSharedPreferencesKey(FwCacheKeyModel.KEY_USERID, FwSPSetUtil.gPostFile)) {
            return null;
        }

        //批理处理数据
        FwUserModel model = new FwUserModel();
        model.setUserId(util.getInt(FwCacheKeyModel.KEY_USERID, FwSPSetUtil.gPostFile));
        model.setUserToken(util.getString(FwCacheKeyModel.KEY_USERTOKEN, FwSPSetUtil.gPostFile));
        return model;
    }

    /**
     * 获取用户公用数据
     *
     * @return
     */
    public static Map<String, Object> getUserPost() {
        FwUserModel model = getAppPostData();
        if (null == model) {
            return null;
        }
        Map<String, Object> map = new HashMap<>();
        //用户身份数据
        map.put(FwConstConfig.API_KEY_USERID, model.getUserId());
        map.put(FwConstConfig.API_KEY_USERTOKEN, model.getUserToken());
//        //用户定位数据
//        map.put(FwConstConfig.API_KEY_LONGITUDE, model.getLongitude());
//        map.put(FwConstConfig.API_KEY_LATITUDE, model.getLatitude());
//        //用户区域数据
//        map.put(FwConstConfig.API_KEY_PROVINCEID, model.getProvinceId());
//        map.put(FwConstConfig.API_KEY_CITYID, model.getCityId());
//        map.put(FwConstConfig.API_KEY_DISTRICTID, model.getDistrictId());
//        map.put(FwConstConfig.API_KEY_COUNTYID, model.getCountyId());
        return map;
    }

    public static HashMap<String, Object> getPostHasMap() {
        FwUserModel model = getAppPostData();
        if (null == model) {
            return null;
        }
        HashMap<String, Object> map = new HashMap<>();
        //用户身份数据
//        map.put("user_id", model.getUserId());
        map.put("user_token", model.getUserToken());
        return map;
    }

    /////////////////////////////////////////////////////////////////
    ///++++管理应用公用数据 结束
    /////////////////////////////////////////////////////////////////

    /////////////////////////////////////////////////////////////////
    ///++++管理应用清除数据 开始
    /////////////////////////////////////////////////////////////////


    public static void clearLoginData() {
        AuthTool.loginOut();
    }

    /////////////////////////////////////////////////////////////////
    ///++++管理应用清除数据 结束
    /////////////////////////////////////////////////////////////////
}
