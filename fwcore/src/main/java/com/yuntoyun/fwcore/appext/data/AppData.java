package com.yuntoyun.fwcore.appext.data;

import android.text.TextUtils;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.yuntoyun.fwcore.appext.model.CacheKeyModel;
import com.yuntoyun.fwcore.appext.model.UserModel;
import com.yuntoyun.fwcore.tool.login.LoginTool;
import com.yuntoyun.fwcore.config.FwConstConfig;
import com.yuntoyun.fwcore.util.FwGsonUtil;
import com.yuntoyun.fwcore.util.FwSharedPreferencesUtil;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 缓存应用数据
 */
public class AppData {

    /////////////////////////////////////////////////////////////////
    ///++++管理应用数据 开始（可无限扩展）
    /////////////////////////////////////////////////////////////////

    //////////////////////////应用数据///////////////////////////////


    public static void setAppAgreement(boolean isAgreed) {
        SPSetUtil.setPrivateBoolean(CacheKeyModel.KEY_APP_AGREEMENT, isAgreed);
    }

    public static boolean getAppAgreement() {
        return SPSetUtil.getPrivateBoolean(CacheKeyModel.KEY_APP_AGREEMENT);
    }

    public static void setAppIsFirstStart(boolean isFirst) {
        SPSetUtil.setPrivateBoolean(CacheKeyModel.KEY_APP_ISFIRST_START, isFirst);
    }

    public static String getAppIsFirstStart() {
        return SPSetUtil.getPrivateString(CacheKeyModel.KEY_APP_ISFIRST_START);
    }

    public static void setAppTaskSwitch(int flag) {
        SPSetUtil.setPrivateInt(CacheKeyModel.KEY_APP_SWITCH_TASK, flag);
    }

    public static boolean isAppTaskSwitch() {
        return (1 == SPSetUtil.getPrivateInt(CacheKeyModel.KEY_APP_SWITCH_TASK));
    }

    //////////////////////////身份数据///////////////////////////////

    public static void setUserId(int userId) {
        SPSetUtil.setAuthInt(CacheKeyModel.KEY_USERID, userId);
    }

    public static int getUserId() {
        return SPSetUtil.getAuthInt(CacheKeyModel.KEY_USERID);
    }

    public static void setUserToken(String token) {
        SPSetUtil.setAuthString(CacheKeyModel.KEY_USERTOKEN, token);
    }

    public static String getUserToken() {
        return SPSetUtil.getAuthString(CacheKeyModel.KEY_USERTOKEN);
    }

    public static void setNickname(String userName) {
        SPSetUtil.setAuthString(CacheKeyModel.KEY_NICKNAME, userName);
    }

    public static String getNickname() {
        return SPSetUtil.getAuthString(CacheKeyModel.KEY_NICKNAME);
    }

    public static void setUserPortrait(String userPortrait) {
        SPSetUtil.setAuthString(CacheKeyModel.KEY_PORTRAIT, userPortrait);
    }

    public static String getUserPortrait() {
        return SPSetUtil.getAuthString(CacheKeyModel.KEY_PORTRAIT);
    }

    public static void setUserAuth(int authId) {
        SPSetUtil.setAuthInt(CacheKeyModel.KEY_USERAUTH, authId);
    }

    public static int getUserAuth() {
        return SPSetUtil.getAuthInt(CacheKeyModel.KEY_USERAUTH);
    }

    public static void setUserAuthState(int authState) {
        SPSetUtil.setAuthInt(CacheKeyModel.KEY_USERAUTH_STATE, authState);
    }

    public static int getUserAuthState() {
        return SPSetUtil.getAuthInt(CacheKeyModel.KEY_USERAUTH_STATE);
    }

    public static void setWithdrawalPwd(boolean isHas) {
        SPSetUtil.setAuthBoolean(CacheKeyModel.KEY_WITHDRAWAL_PWD, isHas);
    }

    public static boolean isWithdrawalPwd() {
        return SPSetUtil.getAuthBoolean(CacheKeyModel.KEY_WITHDRAWAL_PWD);
    }

    public static void setTaobaoAuth(boolean isAuth) {
        SPSetUtil.setAuthBoolean(CacheKeyModel.KEY_TAOBAOAUTH, isAuth);
    }

    public static boolean getTaobaoAuth() {
        return SPSetUtil.getAuthBoolean(CacheKeyModel.KEY_TAOBAOAUTH);
    }

    public static void setAlipayAccount(boolean isHas) {
        SPSetUtil.setAuthBoolean(CacheKeyModel.KEY_ALIPAY_ACCOUNT, isHas);
    }

    public static boolean isAlipayAccount() {
        return SPSetUtil.getAuthBoolean(CacheKeyModel.KEY_ALIPAY_ACCOUNT);
    }

    //////////////////////////私有数据///////////////////////////////

    public static void setCityId(String cityId) {
        SPSetUtil.setPrivateString(CacheKeyModel.KEY_CITYID, cityId);
    }

    public static String getCityId() {
        return SPSetUtil.getPrivateString(CacheKeyModel.KEY_CITYID);
    }

    public static void setCityName(String cityName) {
        SPSetUtil.setPrivateString(CacheKeyModel.KEY_CITYNAME, cityName);
    }

    public static String getCityName() {
        return SPSetUtil.getPrivateString(CacheKeyModel.KEY_CITYNAME);
    }

    public static void setLongitude(String longitude) {
        SPSetUtil.setPrivateString(CacheKeyModel.KEY_LONGITUDE, longitude);
    }

    public static String getLongitude() {
        return SPSetUtil.getPrivateString(CacheKeyModel.KEY_LONGITUDE);
    }

    public static void setLatitude(String latitude) {
        SPSetUtil.setPrivateString(CacheKeyModel.KEY_LATITUDE, latitude);
    }

    public static String getLatitude() {
        return SPSetUtil.getPrivateString(CacheKeyModel.KEY_LATITUDE);
    }

    //////////////////////////缓存数据///////////////////////////////

    /**
     * 缓存关键词
     *
     * @param keywords  缓存关键词
     * @param maxNum    限制最大数量（0不限）
     * @param cachedKey 缓存标识
     */
    public static void cacheKeywords(String keywords, int maxNum, String cachedKey) {
        String jsonStr = SPSetUtil.getShareString(cachedKey);
        JsonArray jarr = null;
        if (!TextUtils.isEmpty(jsonStr)) {
            jarr = FwGsonUtil.builder().getAsJsonArray(jsonStr);
            for (int i = 0; i < jarr.size(); i++) {
                String item = jarr.get(i).getAsString();
                if (item.equals(keywords)) {
                    jarr.remove(i);
                    //已存在中断运行
                    break;
                }
            }
            //最大数量限制
            if (0 != maxNum) {
                if (jarr.size() >= maxNum) {
                    //正序
//                    jarr.remove(jarr.size() - 1);
                    //倒序
                    jarr.remove(0);
                }
            }
        } else {
            jarr = new JsonArray();
        }
        //添加最新词
        jarr.add(keywords);
        SPSetUtil.setShareString(cachedKey, jarr.toString());
    }

    /**
     * 读取关键词缓存的数据
     *
     * @param cachedKey 缓存标识
     * @return
     */
    public static List<String> cacheKeywords(String cachedKey) {
        List<String> pJoList = new ArrayList<>();
        String jsonStr = SPSetUtil.getShareString(cachedKey);
        if (!TextUtils.isEmpty(jsonStr)) {
            JsonArray jarr = FwGsonUtil.builder().getAsJsonArray(jsonStr);
            //正序算法
//            for (int i = 0; i < jarr.size(); i++) {
//                pJoList.add(jarr.get(i).getAsString());
//            }
            //倒序算法
            for (int i = jarr.size() - 1; i > -1; i--) {
                pJoList.add(jarr.get(i).getAsString());
            }
        }
        return pJoList;
    }

    /**
     * 缓存访问的城市
     *
     * @param cityName
     * @param cityCode
     * @param maxNum   限制最大数量（0不限）
     */
    public static void cacheHistoryCity(String cityName, String cityCode, int maxNum) {
        String cityJsonStr = SPSetUtil.getShareString(CacheKeyModel.KEY_TMP_CITY_SET);
        JsonArray cityArray = null;
        if (!TextUtils.isEmpty(cityJsonStr)) {
            cityArray = FwGsonUtil.builder().getAsJsonArray(cityJsonStr);
            for (int i = 0; i < cityArray.size(); i++) {
                JsonObject object = (JsonObject) cityArray.get(i);
                if (cityCode.equals(FwGsonUtil.builder().getString(object, "cityCode"))) {
                    //已存在中断运行
                    return;
                }
            }
            //最大数量限制
            if (0 != maxNum) {
                if (cityArray.size() >= maxNum) {
                    //正序
//                    cityArray.remove(cityArray.size() - 1);
                    //倒序
                    cityArray.remove(0);
                }
            }
        } else {
            cityArray = new JsonArray();
        }
        JsonObject joItem = new JsonObject();
        joItem.addProperty("cityName", cityName);
        joItem.addProperty("cityCode", cityCode);
        cityArray.add(joItem);
        SPSetUtil.setShareString(CacheKeyModel.KEY_TMP_CITY_SET, cityArray.toString());
    }

    /**
     * 读取缓存的历史城市
     *
     * @return
     */
    public static List<JsonObject> cacheHistoryCity() {
        List<JsonObject> pJoList = new ArrayList<>();
        String cityJsonStr = SPSetUtil.getShareString(CacheKeyModel.KEY_TMP_CITY_SET);
        if (!TextUtils.isEmpty(cityJsonStr)) {
            JsonArray cityArray = FwGsonUtil.builder().getAsJsonArray(cityJsonStr);
            //正序算法
//            for (int i = 0; i < cityArray.size(); i++) {
//                JsonObject object = (JsonObject) cityArray.get(i);
//                pJoList.add(object);
//            }
            //倒序算法
            for (int i = cityArray.size() - 1; i > -1; i--) {
                JsonObject object = (JsonObject) cityArray.get(i);
                pJoList.add(object);
            }

        }
        return pJoList;
    }

    /////////////////////////////////////////////////////////////////
    ///++++管理应用数据 结束
    /////////////////////////////////////////////////////////////////


    /////////////////////////////////////////////////////////////////
    ///++++管理用户数据 开始
    /////////////////////////////////////////////////////////////////

    /**
     * 缓存用户数据
     *
     * @param model
     * @return
     */
    public static boolean setUserData(UserModel model) {
        if (null == model)
            return false;

        //批理处理数据
        FwSharedPreferencesUtil util = FwSharedPreferencesUtil.builder();
        util.setInt(CacheKeyModel.KEY_USERID, model.getUserId(), SPSetUtil.gAuthFile);
        util.setString(CacheKeyModel.KEY_USERTOKEN, model.getUserToken(), SPSetUtil.gAuthFile);
        util.setInt(CacheKeyModel.KEY_USERAUTH, model.getUserAuth(), SPSetUtil.gAuthFile);
        util.setInt(CacheKeyModel.KEY_USERAUTH_STATE, model.getUserAuthState(), SPSetUtil.gAuthFile);

        //根据需要自由增减
        return true;
    }

    /**
     * 获取用户缓存数据
     *
     * @return
     */
    public static UserModel getUserData() {
        FwSharedPreferencesUtil util = FwSharedPreferencesUtil.builder();

        if (!util.hasSharedPreferencesKey(CacheKeyModel.KEY_USERID, SPSetUtil.gAuthFile)) {
            return null;
        }

        //批理处理数据
        UserModel model = new UserModel();
        model.setUserId(util.getInt(CacheKeyModel.KEY_USERID, SPSetUtil.gAuthFile));
        model.setUserToken(util.getString(CacheKeyModel.KEY_USERTOKEN, SPSetUtil.gAuthFile));
        model.setUserAuth(util.getInt(CacheKeyModel.KEY_USERAUTH, SPSetUtil.gAuthFile));
        model.setUserAuth(util.getInt(CacheKeyModel.KEY_USERAUTH_STATE, SPSetUtil.gAuthFile));

        //根据需要自由增减
        return model;
    }

    /////////////////////////////////////////////////////////////////
    ///++++管理用户数据 结束
    /////////////////////////////////////////////////////////////////


    /////////////////////////////////////////////////////////////////
    ///++++管理应用公用数据 开始
    /////////////////////////////////////////////////////////////////

    /**
     * 缓存用户登录状态的公用数据
     *
     * @param model
     * @return
     */
    public static boolean setPostData(UserModel model) {
        if (null == model)
            return false;

        //批理处理数据
        FwSharedPreferencesUtil util = FwSharedPreferencesUtil.builder();
        util.setInt(CacheKeyModel.KEY_USERID, model.getUserId(), SPSetUtil.gPostFile);
        util.setString(CacheKeyModel.KEY_USERTOKEN, model.getUserToken(), SPSetUtil.gPostFile);
        return true;
    }

    /**
     * 获取用户登录状态的公用数据
     *
     * @return
     */
    public static UserModel getPostData() {
        FwSharedPreferencesUtil util = FwSharedPreferencesUtil.builder();

        if (!util.hasSharedPreferencesKey(CacheKeyModel.KEY_USERID, SPSetUtil.gPostFile)) {
            return null;
        }

        //批理处理数据
        UserModel model = new UserModel();
        model.setUserId(util.getInt(CacheKeyModel.KEY_USERID, SPSetUtil.gPostFile));
        model.setUserToken(util.getString(CacheKeyModel.KEY_USERTOKEN, SPSetUtil.gPostFile));
        return model;
    }

    /**
     * 获取用户公用数据
     *
     * @return
     */
    public static Map<String, Object> getUserPost() {
        UserModel model = getPostData();
        if (null == model) {
            return null;
        }
        Map<String, Object> map = new HashMap<>();
        //用户身份数据
        map.put(FwConstConfig.API_KEY_USERID, model.getUserId());
        map.put(FwConstConfig.API_KEY_USERTOKEN, model.getUserToken());
//        //用户定位数据
//        map.put(FwConstConfig.API_KEY_LONGITUDE, model.getLongitude());
//        map.put(FwConstConfig.API_KEY_LATITUDE, model.getLatitude());
//        //用户区域数据
//        map.put(FwConstConfig.API_KEY_PROVINCEID, model.getProvinceId());
//        map.put(FwConstConfig.API_KEY_CITYID, model.getCityId());
//        map.put(FwConstConfig.API_KEY_DISTRICTID, model.getDistrictId());
//        map.put(FwConstConfig.API_KEY_COUNTYID, model.getCountyId());
        return map;
    }

    public static HashMap<String, Object> getPostHasMap() {
        UserModel model = getPostData();
        if (null == model) {
            return null;
        }
        HashMap<String, Object> map = new HashMap<>();
        //用户身份数据
//        map.put("user_id", model.getUserId());
        map.put("user_token", model.getUserToken());
        return map;
    }

    /////////////////////////////////////////////////////////////////
    ///++++管理应用公用数据 结束
    /////////////////////////////////////////////////////////////////

    /////////////////////////////////////////////////////////////////
    ///++++管理应用清除数据 开始
    /////////////////////////////////////////////////////////////////


    public static void clearLoginData() {
        LoginTool.loginOut();
    }

    /////////////////////////////////////////////////////////////////
    ///++++管理应用清除数据 结束
    /////////////////////////////////////////////////////////////////
}
