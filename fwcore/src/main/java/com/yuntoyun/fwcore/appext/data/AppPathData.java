package com.yuntoyun.fwcore.appext.data;

import com.yuntoyun.fwcore.util.FwSDCardUtil;

/**
 * 路径管理
 */
public class AppPathData {

    private final String gApk = "apk";
    private final String gImage = "image";
    private final String gTmp = "temp";
    private final String gOther = "other";


    //++++++++++++++++++++++++++++++++++++++++++++++++++++

    private static AppPathData init = null;

    synchronized public static AppPathData init() {
        if (null == init) {
            init = new AppPathData();
        }
        return init;
    }

    //++++++++++++++++++++++++++++++++++++++++++++++++++++
    //++++++++++++++++++++++++++++++++++++++++++++++++++++

    public String getApkPath() {
        return FwSDCardUtil.builder().getCustomePath(gApk);
    }

    public String getImgPath() {
        return FwSDCardUtil.builder().getCustomePath(gImage);
    }

    public String getCropImgPath() {
        return FwSDCardUtil.builder().getCustomePath(gImage + "/crop");
    }

    public String getTmpPath() {
        return FwSDCardUtil.builder().getCustomePath(gTmp);
    }

    public String getOtherPath() {
        return FwSDCardUtil.builder().getCustomePath(gOther);
    }
}
