package com.yuntoyun.fwcore.appext.model;
/*
 * +----------------------------------------------------------------------
 * | @Author: codeavatar   @Year：2021
 * +----------------------------------------------------------------------
 * | @Email: codeavatar@aliyun.com
 * +----------------------------------------------------------------------
 */
/**
 * 缓存键名
 */
public class FwCacheKeyModel {

    //应用
    public static final String KEY_APP_AGREEMENT = "ca_app_agreement";
    public static final String KEY_APP_ISFIRST_START = "ca_app_isfirst_start";
    public static final String KEY_APP_SWITCH_AUDIT = "ca_app_switch_audit";

    //用户私人数据
    public static final String KEY_USERID = "ca_user_id";
    public static final String KEY_USERTOKEN = "ca_user_token";
    public static final String KEY_USERAUTH = "ca_user_auth";
    public static final String KEY_USERAUTH_STATE = "ca_user_auth_state";
    public static final String KEY_NICKNAME = "ca_nickname";
    public static final String KEY_PORTRAIT = "ca_portrait";

    //用户定位坐标
    public static final String KEY_LONGITUDE = "ca_longitude";
    public static final String KEY_LATITUDE = "ca_latitude";

    //用户域区数据
    public static final String KEY_COUNTRYID = "ca_country_id";
    public static final String KEY_PROVINCEID = "ca_province_id";
    public static final String KEY_CITYID = "ca_city_id";
    public static final String KEY_COUNTYID = "ca_county_id";
    public static final String KEY_DISTRICTID = "ca_district_id";

    public static final String KEY_COUNTRYNAME = "ca_country_name";
    public static final String KEY_PROVINCENAME = "ca_province_name";
    public static final String KEY_CITYNAME = "ca_city_name";
    public static final String KEY_COUNTYNAME = "ca_county_name";
    public static final String KEY_DISTRICTNAME = "ca_district_name";
}
