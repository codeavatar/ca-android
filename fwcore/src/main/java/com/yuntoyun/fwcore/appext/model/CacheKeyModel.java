package com.yuntoyun.fwcore.appext.model;

/**
 * 缓存键名
 */
public class CacheKeyModel {

    //应用
    public static final String KEY_APP_AGREEMENT = "sy_app_agreement";
    public static final String KEY_APP_ISFIRST_START = "sy_app_isfirst_start";
    public static final String KEY_APP_SWITCH_TASK = "sy_app_switch_task";


    //用户私人数据
    public static final String KEY_USERID = "sy_user_id";
    public static final String KEY_USERTOKEN = "sy_user_token";
    public static final String KEY_USERAUTH = "sy_user_auth";
    public static final String KEY_USERAUTH_STATE = "sy_user_auth_state";
    public static final String KEY_NICKNAME = "sy_nickname";
    public static final String KEY_PORTRAIT = "sy_portrait";
    public static final String KEY_TAOBAOAUTH = "sy_taobao_auth";
    public static final String KEY_WITHDRAWAL_PWD = "sy_has_withdrawal_pwd";
    public static final String KEY_ALIPAY_ACCOUNT = "sy_has_alipay_account";

    //用户定位坐标
    public static final String KEY_LONGITUDE = "sy_longitude";
    public static final String KEY_LATITUDE = "sy_latitude";

    //用户域区数据
    public static final String KEY_COUNTRYID = "sy_country_id";
    public static final String KEY_PROVINCEID = "sy_province_id";
    public static final String KEY_CITYID = "sy_city_id";
    public static final String KEY_COUNTYID = "sy_county_id";
    public static final String KEY_DISTRICTID = "sy_district_id";

    public static final String KEY_COUNTRYNAME = "sy_country_name";
    public static final String KEY_PROVINCENAME = "sy_province_name";
    public static final String KEY_CITYNAME = "sy_city_name";
    public static final String KEY_COUNTYNAME = "sy_county_name";
    public static final String KEY_DISTRICTNAME = "sy_district_name";

    //缓存数据
    public static final String KEY_TMP_CITY_SET = "sy_tmp_city_set";
    public static final String KEY_TMP_Keywords_JD = "sy_tmp_keywords_jd";
    public static final String KEY_TMP_Keywords_TB = "sy_tmp_keywords_tb";
    public static final String KEY_TMP_Keywords_PDD = "sy_tmp_keywords_pdd";

}
