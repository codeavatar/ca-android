package com.yuntoyun.fwcore.adapter;
/*
 * +----------------------------------------------------------------------
 * | @Author: codeavatar   @Year：2021
 * +----------------------------------------------------------------------
 * | @Email: codeavatar@aliyun.com
 * +----------------------------------------------------------------------
 */
import android.content.Context;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;

import java.util.List;

/**
 * RecyclerView适配器
 */
public class FwBaseRecyclerViewAdapter<T> extends BaseQuickAdapter<T, BaseViewHolder> {

    Context context;
    IFwViewHolder<T> viewHolder;

    public FwBaseRecyclerViewAdapter(Context context, int layoutResId, List<T> data,
                                     IFwViewHolder<T> viewHolder) {
        super(layoutResId, data);
        this.viewHolder = viewHolder;
        this.context = context;
    }

    @Override
    protected void convert(BaseViewHolder helper, T item) {
        viewHolder.itemViewHolder(helper, item, context);
    }

    public interface IFwViewHolder<T> {
        void itemViewHolder(BaseViewHolder helper, T item, Context context);
    }

}
