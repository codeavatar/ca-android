package com.yuntoyun.fwcore.adapter;
/*
 * +----------------------------------------------------------------------
 * | @Author: codeavatar   @Year：2021
 * +----------------------------------------------------------------------
 * | @Email: codeavatar@aliyun.com
 * +----------------------------------------------------------------------
 */
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import java.util.List;

/**
 * ViewPager无缓存适配器
 * 推荐大量Fragment使用
 */
public class FwFragmentStatePagerAdapter extends FragmentStatePagerAdapter {

    List<Fragment> fragments;
    List<String> tabNames;

    public FwFragmentStatePagerAdapter(FragmentManager fm, List<Fragment> fragments, List<String>
            tabNames) {
        super(fm);
        this.fragments = fragments;
        this.tabNames = tabNames;
    }

    @Override
    public Fragment getItem(int position) {
        return fragments.get(position);
    }

    @Override
    public int getCount() {
        return fragments.size();
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        if (null != tabNames) {
            return tabNames.get(position);
        }
        return super.getPageTitle(position);
    }

    @Override
    public int getItemPosition(@NonNull Object object) {
        return super.getItemPosition(object);
//        return POSITION_NONE;
    }
}
