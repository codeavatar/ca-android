package com.yuntoyun.fwcore.adapter;
/*
 * +----------------------------------------------------------------------
 * | @Author: codeavatar   @Year：2021
 * +----------------------------------------------------------------------
 * | @Email: codeavatar@aliyun.com
 * +----------------------------------------------------------------------
 */
import java.util.List;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

/**
 * ViewPager 有缓存适配器
 * 推荐少量Fragment使用
 */
public class FwFragmentPagerAdapter extends FragmentPagerAdapter {

    List<Fragment> fragments;
    List<String> tabNames;

    public FwFragmentPagerAdapter(FragmentManager fm, List<Fragment> fragments, List<String>
            tabNames) {
        super(fm, FragmentPagerAdapter.BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
        this.fragments = fragments;
        this.tabNames = tabNames;
    }

    @Override
    public Fragment getItem(int position) {
        return fragments.get(position);
    }

    @Override
    public int getCount() {
        return fragments.size();
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        if (null != tabNames && tabNames.size() > 0) {
            return tabNames.get(position);
        }
        return super.getPageTitle(position);
    }
}
