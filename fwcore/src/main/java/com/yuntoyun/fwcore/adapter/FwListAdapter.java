package com.yuntoyun.fwcore.adapter;
/*
 * +----------------------------------------------------------------------
 * | @Author: codeavatar   @Year：2021
 * +----------------------------------------------------------------------
 * | @Email: codeavatar@aliyun.com
 * +----------------------------------------------------------------------
 */
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SimpleAdapter;

import java.util.List;
import java.util.Map;

/**
 * ListView或GridView适配器
 */
public class FwListAdapter extends SimpleAdapter {

    private Context context;
    private IFwListViewHolder iFwViewHolder = null;
    private int resource;
    List<? extends Map<String, ?>> data;

    public FwListAdapter(Context context, List<? extends Map<String, ?>> data, int resource,
                         IFwListViewHolder iFwListViewHolder) {
        super(context, data, resource, null, null);
        this.context = context;
        this.data = data;
        this.resource = resource;
        iFwViewHolder = iFwListViewHolder;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View layoutView = LayoutInflater.from(context).inflate(resource, parent, false);
        return iFwViewHolder.getView(position, data, layoutView);
    }

    public interface IFwListViewHolder {
        View getView(int position, List<? extends Map<String, ?>> data, View layoutView);
    }
}
