package com.yuntoyun.fwcore.adapter;
/*
 * +----------------------------------------------------------------------
 * | @Author: codeavatar   @Year：2021
 * +----------------------------------------------------------------------
 * | @Email: codeavatar@aliyun.com
 * +----------------------------------------------------------------------
 */
import android.content.Context;

import com.google.gson.JsonObject;

import java.util.List;

/**
 * RecyclerView适配器
 * 固定Json格式
 */
public class FwRecyclerViewJsonAdapter extends FwBaseRecyclerViewAdapter<JsonObject> {

    public FwRecyclerViewJsonAdapter(Context context, int layoutResId, List<JsonObject> data,
                                     IFwViewHolder viewHolder) {
        super(context, layoutResId, data, viewHolder);
    }
}
