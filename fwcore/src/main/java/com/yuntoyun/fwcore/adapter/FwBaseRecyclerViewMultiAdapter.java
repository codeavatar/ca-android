package com.yuntoyun.fwcore.adapter;
/*
 * +----------------------------------------------------------------------
 * | @Author: codeavatar   @Year：2021
 * +----------------------------------------------------------------------
 * | @Email: codeavatar@aliyun.com
 * +----------------------------------------------------------------------
 */
import android.content.Context;

import com.chad.library.adapter.base.BaseMultiItemQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.chad.library.adapter.base.entity.MultiItemEntity;

import java.util.List;

/**
 * RecyclerView适配器(多种显示类型)
 */
public class FwBaseRecyclerViewMultiAdapter<T extends MultiItemEntity> extends BaseMultiItemQuickAdapter<T, BaseViewHolder> {

    Context context;
    IFwViewHolder viewHolder;

    public FwBaseRecyclerViewMultiAdapter(Context context, int layoutResId, List<T> data,
                                          IFwViewHolder viewHolder) {
        super(data);
        this.viewHolder = viewHolder;
        this.context = context;
    }

    @Override
    protected void convert(BaseViewHolder helper, T item) {
        viewHolder.itemViewHolder(helper, item, context);
    }

    public interface IFwViewHolder<T> {
        void itemViewHolder(BaseViewHolder helper, T item, Context context);
    }

}
