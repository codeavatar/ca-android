package com.yuntoyun.fwcore.adapter;
/*
 * +----------------------------------------------------------------------
 * | @Author: codeavatar   @Year：2021
 * +----------------------------------------------------------------------
 * | @Email: codeavatar@aliyun.com
 * +----------------------------------------------------------------------
 */
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.yuntoyun.fwcore.model.FwSpinnerAdapterModel;

import java.util.List;

public class FwBaseSpinnerAdapter extends ArrayAdapter<FwSpinnerAdapterModel> {

    private Context context;
    private List<FwSpinnerAdapterModel> list;

    public FwBaseSpinnerAdapter(Context context, int resource,
                                List<FwSpinnerAdapterModel> list) {
        super(context, resource, list);
        this.context = context;
        this.list = list;
    }

    @Override
    public void setDropDownViewResource(int resource) {
        super.setDropDownViewResource(resource);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (null == convertView) {
            convertView = LayoutInflater.from(context).inflate(
                    android.R.layout.simple_spinner_item, parent, false);

        }
        FwSpinnerAdapterModel module = list.get(position);
        TextView tvw = (TextView) convertView.findViewById(android.R.id.text1);
        tvw.setText(module.getName());
        tvw.setTag(module.getValue());
        return convertView;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        if (null == convertView) {
            convertView = LayoutInflater.from(context).inflate(
                    android.R.layout.simple_spinner_dropdown_item, parent, false);

        }
        FwSpinnerAdapterModel module = list.get(position);
        TextView tvw = (TextView) convertView.findViewById(android.R.id.text1);
        tvw.setText(module.getName());
        tvw.setTag(module.getValue());
        return convertView;
    }

}
