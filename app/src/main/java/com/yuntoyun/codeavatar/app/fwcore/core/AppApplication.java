package com.yuntoyun.codeavatar.app.fwcore.core;

import android.content.Context;

import com.yuntoyun.fwcore.base.FwApplication;
import com.yuntoyun.fwcore.config.FwIConfig;

/**
 * 系统
 * 访问修饰必须是public
 */
public class AppApplication extends FwApplication {

    @Override
    protected void initChildApp() {
        FwIConfig tmpconfig = (FwIConfig) new AppConfigSetting();
        super.setAppConfig(tmpconfig);
    }

    /**
     * 应用上下文引用
     *
     * @return
     */
    public static Context appContext() {
        return getAppContext();
    }
}
