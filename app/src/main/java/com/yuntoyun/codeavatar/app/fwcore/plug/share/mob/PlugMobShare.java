package com.yuntoyun.codeavatar.app.fwcore.plug.share.mob;

import android.app.Activity;
import android.graphics.Bitmap;
import android.text.TextUtils;

import com.mob.MobSDK;
import com.yuntoyun.fwcore.appext.data.AppPathData;
import com.yuntoyun.fwcore.plug.image.glide.PlugGlide;
import com.yuntoyun.fwcore.util.FwLogUtil;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;

import cn.sharesdk.framework.Platform;
import cn.sharesdk.framework.PlatformActionListener;
import cn.sharesdk.framework.ShareSDK;
import cn.sharesdk.onekeyshare.OnekeyShare;
import cn.sharesdk.wechat.friends.Wechat;

public class PlugMobShare extends Activity {

    private final String fTag = PlugMobShare.class.getSimpleName();

//    public void doInit(Application application) {
//        MobSDK.init(application);
//    }

    /**
     * 需要 开启存储权限
     * @param platformName
     * @param title
     * @param remark
     * @param thumbnailPath
     * @param webUrl
     */
    public void doThirdShare(final String platformName, final String title, final String remark, final String thumbnailPath, final String webUrl) {

        try {
            //url转码
            String tmpThumbnail = URLDecoder.decode(thumbnailPath, "UTF-8");
            FwLogUtil.info(fTag, "Url decode is " + tmpThumbnail);

            PlugGlide.init().downloadImage(tmpThumbnail, new PlugGlide.DownloadImageCallback() {
                @Override
                public void download(Bitmap bitmap, Bitmap.CompressFormat format) {

                    FwLogUtil.info(String.format("目标图片大小：%d", bitmap.getByteCount()));

                    Bitmap thumbBmp = Bitmap.createScaledBitmap(bitmap, 150, 150, true);
//                                bitmap.recycle();
                    // 首先保存图片
                    File appDir = new File(AppPathData.init().getImgPath(), "thumbnail");
                    if (!appDir.exists()) {
                        appDir.mkdir();
                    }
//                                String fileName = System.currentTimeMillis() + ".jpg";
                    String fileName = "publish_match_share.jpg";
                    File file = new File(appDir, fileName);
                    try {
                        FileOutputStream fos = new FileOutputStream(file);
                        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fos);
                        fos.flush();
                        fos.close();
//                        String attachLocatImage = file.getAbsolutePath();
                        String attachLocatImage = file.getPath();

                        //Mob 代码
                        OnekeyShare oks = new OnekeyShare();
                        if (TextUtils.isEmpty(platformName)) {
                            oks.setPlatform(Wechat.NAME);
                        }else{
                            oks.setPlatform(platformName);
                        }
                        // title标题，微信、QQ和QQ空间等平台使用
                        oks.setTitle(title);
                        // titleUrl QQ和QQ空间跳转链接
                        oks.setTitleUrl(webUrl);
                        // text是分享文本，所有平台都需要这个字段
                        oks.setText(remark);
                        // imagePath是图片的本地路径，确保SDcard下面存在此张图片
                        oks.setImagePath(attachLocatImage);
                        // url在微信、Facebook等平台中使用
                        oks.setUrl(webUrl);
                        // 启动分享GUI
                        oks.show(MobSDK.getContext());
                    } catch (FileNotFoundException e) {
                        FwLogUtil.error(e.getMessage());
                    } catch (IOException e) {
                        FwLogUtil.error(e.getMessage());
                    }
                }
            });
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void doThirdLogin(String platformName, PlatformActionListener platformActionListener) {
        if (TextUtils.isEmpty(platformName)) {
            platformName = Wechat.NAME;
        }
        Platform plat = ShareSDK.getPlatform(platformName);
        if (plat.isAuthValid()) {
            //移除授权状态和本地缓存，下次授权会重新授权
            plat.removeAccount(true);
        }
        //SSO授权，传false默认是客户端授权
        plat.SSOSetting(false);
        //授权回调监听，监听oncomplete，onerror，oncancel三种状态
        plat.setPlatformActionListener(platformActionListener);
//        //抖音登录适配安卓9.0
//        ShareSDK.setActivity(MainActivity.this);
        //要数据不要功能，主要体现在不会重复出现授权界面
        plat.showUser(null);
        //要功能，不要数据
//        plat.authorize();
    }
}
