package com.yuntoyun.codeavatar.app.fwcore.plug.pay.wxpay;

import com.tencent.mm.opensdk.modelpay.PayResp;

public interface WxCallback {

    /**
     * 支付成功
     *
     * @param resp
     */
    void paySuccessed(PayResp resp);

    /**
     *
     * @param resp
     */
    void payFailed(PayResp resp);
}
