package com.yuntoyun.codeavatar.app.activity.main;

import android.content.Context;
import android.os.Bundle;
import android.os.Message;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.chad.library.adapter.base.BaseViewHolder;
import com.google.gson.JsonObject;
import com.yuntoyun.codeavatar.app.R;
import com.yuntoyun.codeavatar.app.fwcore.core.AppCoreSign;
import com.yuntoyun.fwcore.adapter.FwBaseRecyclerViewAdapter;
import com.yuntoyun.fwcore.base.FwBaseRecyclerViewPagerFragment;
import com.yuntoyun.fwcore.model.FwRecyclerViewAdapterModel;
import com.yuntoyun.fwcore.model.FwRecyclerViewPostModel;
import com.yuntoyun.fwcore.model.FwResultJsonDataModel;
import com.yuntoyun.fwcore.model.FwResultJsonListDataModel;
import com.yuntoyun.fwcore.plug.image.glide.PlugGlide;
import com.yuntoyun.fwcore.plug.network.retrofit.FwOkHttp;
import com.yuntoyun.fwcore.plug.tool.FwViewTool;
import com.yuntoyun.fwcore.util.FwIntentUtil;

import java.util.HashMap;

import io.reactivex.Observable;

public class HomeTabListFragment extends FwBaseRecyclerViewPagerFragment implements FwBaseRecyclerViewAdapter.IFwViewHolder<JsonObject> {

    private int platformType;
    private int platformCategoryId;

    @Override
    protected void initChildView() {

    }

    @Override
    protected void initChildData() {

    }

    @Override
    protected void initChildEvent() {

    }

    @Override
    protected FwRecyclerViewPostModel getRequestModel() {
        HashMap<String, Object> map = new HashMap<>();
        map.put("fwCurrentPage", super.fwCurrentPage);
        map.put("platformType", getPlatformType());
        map.put("categoryId", getPlatformCategoryId());
        AppCoreSign.addSign(map);
        Observable<FwResultJsonListDataModel> observable = FwOkHttp.getFwAPIService().getGoodsList(map);
        return new FwRecyclerViewPostModel(map, observable);
    }

    @Override
    protected FwRecyclerViewAdapterModel getAdapterModel() {
        return new FwRecyclerViewAdapterModel(R.layout.fragment_main_home_tab_list_item, R.layout.fw_layout_list_nodata, this);
    }

    @Override
    protected RecyclerView.LayoutManager getLayoutManager() {
        return super.getCustomLayoutManager(LayoutManangerEnum.StaggeredGridVertical, 2);
    }

    @Override
    protected void setResultData(JsonObject data) {

    }

    @Override
    protected void setResultListItem(JsonObject data) {

    }

    @Override
    protected void onItemClickHandler(JsonObject data, View view, int position) {
        if (null == data) return;

        Bundle extras = new Bundle();
        extras.putString("ext_data", data.toString());
        extras.putInt("ext_platform", getPlatformType());
//        FwIntentUtil.builder().startActivity(getActivity(), ThirdGoodsDetailActivity.class, extras);
    }

    @Override
    protected void onItemChildClickHandler(JsonObject data, View view, int position) {

    }

    @Override
    protected void childMessageHandler(FwResultJsonListDataModel model) {

    }

    @Override
    protected View getHeaderView() {
        return null;
    }

    @Override
    protected View getFooterView() {
        return null;
    }

    @Override
    protected boolean isAgainCreateView() {
        return false;
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.layout_third_list_recyclerview;
    }

    @Override
    protected void initNavigation() {

    }

    @Override
    protected void messageHandler(Message msg) {

    }

    @Override
    protected void messageHandler(FwResultJsonDataModel model) {

    }

    @Override
    public void onClick(View v) {

    }

    @Override
    public void itemViewHolder(BaseViewHolder helper, JsonObject item, Context context) {
        ImageView ivw_thumbnail = helper.getView(R.id.ivw_thumbnail);
        PlugGlide.init().loadImg(context, fwGsonUtil.getString(item, "goodsThumbnail"),
                PlugGlide.GlidePlaceHolderType.Square, ivw_thumbnail);

        helper.setText(R.id.tvw_title, fwGsonUtil.getString(item, "goodsName"));
        helper.setText(R.id.tvw_remark, fwGsonUtil.getString(item, "goodsTip"));
        helper.setText(R.id.tvw_price, String.format("¥ %s", fwGsonUtil.getString(item, "goodsPrice")));
        helper.setText(R.id.tvw_commission, fwGsonUtil.getString(item, "commission").replace("返利:", ""));

        //逻辑处理
        String pCouponTip = fwGsonUtil.getString(item, "couponTip");
        TextView tvw_price_remark = helper.getView(R.id.tvw_price_remark);
        if (TextUtils.isEmpty(pCouponTip)) {
            TextView tvw_coupon = helper.getView(R.id.tvw_coupon);
            tvw_price_remark.setVisibility(View.GONE);
            tvw_coupon.setVisibility(View.GONE);
        } else {
//            helper.setText(R.id.tvw_price_remark, String.format("¥ %s元", fwGsonUtil.getString(item, "goodsCost")));
            tvw_price_remark.setText(String.format("¥ %s", fwGsonUtil.getString(item, "goodsCost")));
            FwViewTool.builder().setStrikeLine(tvw_price_remark, false);
            helper.setText(R.id.tvw_coupon, pCouponTip);
        }

//        //标题首行缩进
//        String tmpTitle = fwGsonUtil.getString(item, "goodsName");
//        TextView tvw_title = helper.getView(R.id.tvw_title);
//        FwViewTool.builder().setTextViewIndent(fwActivity, tvw_title, 40, tmpTitle);

//        //平台类型(1 京东；2 淘宝；3 拼多多；)
//        ImageView ivw_tag = helper.getView(R.id.ivw_tag);
//
//        switch (getPlatformType()) {
//            case 1:
//                ivw_tag.setImageResource(R.mipmap.icon_flag_home_tab_jd);
//                break;
//            case 2:
//                ivw_tag.setImageResource(R.mipmap.icon_flag_home_tab_tb);
//                break;
//            case 3:
//                ivw_tag.setImageResource(R.mipmap.icon_flag_home_tab_pdd);
//                break;
//            case 4:
//                ivw_tag.setImageResource(R.mipmap.icon_flag_home_tab_tmall);
//                break;
//            default:
//                ivw_tag.setVisibility(View.GONE);
//                break;
//        }
    }

    //========================================================================

    public int getPlatformType() {
        return platformType;
    }

    public void setPlatformType(int platformType) {
        this.platformType = platformType;
    }

    public int getPlatformCategoryId() {
        return platformCategoryId;
    }

    public void setPlatformCategoryId(int platformCategoryId) {
        this.platformCategoryId = platformCategoryId;
    }


    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


}
