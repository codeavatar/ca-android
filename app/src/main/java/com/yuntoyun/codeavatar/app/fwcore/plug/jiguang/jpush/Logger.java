package com.yuntoyun.codeavatar.app.fwcore.plug.jiguang.jpush;

import com.yuntoyun.fwcore.util.FwLogUtil;

/**
 * Created by efan on 2017/4/13.
 */

public class Logger {


    public static void i(String tag, String msg) {
        FwLogUtil.info(tag, msg);
    }

    public static void v(String tag, String msg) {
        FwLogUtil.verbose(tag, msg);
    }

    public static void d(String tag, String msg) {
        FwLogUtil.debug(tag, msg);
    }

    public static void w(String tag, String msg) {
        FwLogUtil.warn(tag, msg);
    }

    public static void e(String tag, String msg) {
        FwLogUtil.error(tag, msg);
    }

}
