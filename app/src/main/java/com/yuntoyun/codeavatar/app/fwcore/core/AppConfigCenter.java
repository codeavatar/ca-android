package com.yuntoyun.codeavatar.app.fwcore.core;

/**
 * 项目配置中心
 */
public class AppConfigCenter {

    /**
     * 应用发布开关
     */
    private final boolean RELEASE_APP = true;
    /**
     * 调试日志输出开关
     */
    public final boolean PRINT_LOG_SWITCH = !RELEASE_APP;
    /**
     * 系统日志输出开关
     */
    public final boolean PRINT_LOG_SWITCH_SYSTEM = !RELEASE_APP;
    /**
     * API正式与测试切换开关
     */
    public final boolean NORMAL_ROOT_API = RELEASE_APP;
}
