package com.yuntoyun.codeavatar.app.fwcore.core;

import com.yuntoyun.fwcore.factory.sign.SignType;
import com.yuntoyun.fwcore.util.FwApplicationUtil;

/**
 * 系统扩展设置
 *
 * @author zgc
 * @version 1.0
 * @email frameworkunion@aliyun.com
 */
public class AppConfigVal {

    //API必备参数
    public final static String POST_PLATFORM = "merchant";
    public final static String POST_DEVICE = "android";
    public final static String POST_VERSION = FwApplicationUtil.builder().getAppVersionName();

    //线上API
    public final String ROOT_API_NORMAL = "http://api.zph369.com";
    public final String DOMAIN_NORMAL = "http://www.zph369.com";
    //线下API
    public final String ROOT_API_TEST = "http://zph.hbshuoyun.net";
    public final String DOMAIN_TEST = "http://zph-manage.hbshuoyun.net";
    //SOCKET
    public final String WEBSOCKET_HOST_NORMAL = ROOT_API_NORMAL.replace("http://","").replace("https://","");
    public final String WEBSOCKET_HOST_TEST = ROOT_API_TEST.replace("http://","").replace("https://","");
    public final int WEBSOCKET_PORT_NORMAL = 6262;
    public final int WEBSOCKET_PORT_TEST = 6262;

    //安全配置
    public final SignType KEY_TYPE = SignType.Md5;
    public final String MD5_SIGN_KEY ="MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAxgI4lZm3mEd36+IpnCcGO+abcBCrAiMfun7VTqF9JhNXa9jJBuWU4YXpmm9DM1q86e+tGACny+MOVRCU9FMK+yg57fvkZbz/BOJ2FHFyyU7oysEA1vTFad5KGrExbSAVlvq5cZ14iBBN8rhabXXi6XZobPKaG8WZLC3Fg1pl4601+fV96/H1l4sqLANYtXHl4qE3o4h+mNJGVVoYlpPZZXIlZ57Yi8oXviX1FwKtUpoji2rFDxke7LaPrXqMrB+M/v7dLQeRIXrdW5gL1Ezi+UNuFomfZzFlK2VtF+YUVbzxOxOO+zR+RLKcXko1/bysKGKo+kzUFbSrxLrz+rvP8QIDAQAB";
    public final String RSA_SIGN_KEY = "";
    public final String RSA2_SIGN_KEY = "";
    public final String CERT_PATH = "";

    //第三方插件
    public final String WECHAT_APPID = "wx4af226fb5bf8706a";
    public final String WECHAT_APPSECRET = "a767eb71c8af9bf15dd83f174549dead";
    public final boolean JPUSH_LAUNCH = false;

}
