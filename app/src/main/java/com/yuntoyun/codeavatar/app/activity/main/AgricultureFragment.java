package com.yuntoyun.codeavatar.app.activity.main;

import android.os.Message;
import android.view.View;

import com.yuntoyun.codeavatar.app.R;
import com.yuntoyun.fwcore.base.FwBaseFragment;
import com.yuntoyun.fwcore.model.FwResultJsonDataModel;
import com.yuntoyun.fwcore.model.FwResultJsonListDataModel;

public class AgricultureFragment extends FwBaseFragment {

    private final String fTag = AgricultureFragment.class.getSimpleName();

    @Override
    protected boolean isAgainCreateView() {
        return false;
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.fragment_main_agriculture;
    }

    @Override
    protected void initView(View layoutView) {

    }

    @Override
    protected void initData() {

    }

    @Override
    protected void initEvent() {

    }

    @Override
    protected void initNavigation() {

    }

    @Override
    protected void messageHandler(Message msg) {

    }

    @Override
    protected void messageHandler(FwResultJsonDataModel model) {

    }

    @Override
    protected void messageHandler(FwResultJsonListDataModel model) {

    }

    @Override
    public void onClick(View v) {

    }

}
