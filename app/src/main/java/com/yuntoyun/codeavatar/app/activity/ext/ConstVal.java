package com.yuntoyun.codeavatar.app.activity.ext;

public class ConstVal {

    //1》帐户；2》手机号；3》微信；
    public static final int APP_LOGIN_ACCOUNT = 1;
    public static final int APP_LOGIN_PHONE = 2;
    public static final int APP_LOGIN_WECHAT = 3;

    //1》余额；2》微信；3》支付宝；
    public static final int APP_PAY_BALANCE = 1;
    public static final int APP_PAY_WECHAT = 2;
    public static final int APP_PAY_ALIPAY = 3;

    // 平台：1 京东；2 淘宝；3 拼多多；4 天猫；
    public static final int APP_THIRD_PLATFORM_JD = 1;
    public static final int APP_THIRD_PLATFORM_TB = 2;
    public static final int APP_THIRD_PLATFORM_PDD = 3;
    public static final int APP_THIRD_PLATFORM_TMALL = 4;

    //The other symbal.
    public static final String APP_SYMBAL_YUAN = "¥ ";
}
