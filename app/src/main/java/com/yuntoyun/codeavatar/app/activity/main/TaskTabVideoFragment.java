package com.yuntoyun.codeavatar.app.activity.main;

import android.content.Context;
import android.graphics.Color;
import android.os.Message;
import android.view.View;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.yuntoyun.codeavatar.app.R;
import com.yuntoyun.fwcore.adapter.FwBaseRecyclerViewAdapter;
import com.yuntoyun.fwcore.adapter.FwFragmentPagerAdapter;
import com.yuntoyun.fwcore.base.FwBaseFragment;
import com.yuntoyun.fwcore.base.ext.RvLinearLayoutManager;
import com.yuntoyun.fwcore.model.FwResultJsonDataModel;
import com.yuntoyun.fwcore.model.FwResultJsonListDataModel;
import com.yuntoyun.fwcore.util.FwLogUtil;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

public class TaskTabVideoFragment extends FwBaseFragment {

    private final String fTag = TaskTabVideoFragment.class.getSimpleName();

    @BindView(R.id.rvw_tags)
    RecyclerView rvw_tags;
    @BindView(R.id.vpr_pages)
    ViewPager vpr_pages;

    private JsonArray jarrTags;
    private List<String> gTabNames = new ArrayList<>();
    private List<Fragment> gTabFragments = new ArrayList<>();
    private List<JsonObject> gTabList = new ArrayList<>();
    private FwBaseRecyclerViewAdapter<JsonObject> gTabAdapter;
    private int gTabPostion = 0;

    @Override
    protected boolean isAgainCreateView() {
        return false;
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.fragment_main_task_tab_video;
    }

    @Override
    protected void initView(View layoutView) {
        //清除数据
        gTabList.clear();

        if (null != jarrTags) {
            for (JsonElement element : jarrTags) {
                gTabList.add(element.getAsJsonObject());
            }
        }
        //数据适配器
        gTabAdapter = new FwBaseRecyclerViewAdapter<JsonObject>(fwActivity, R.layout.fragment_main_task_tab_video_tag, gTabList,
                new FwBaseRecyclerViewAdapter.IFwViewHolder<JsonObject>() {
                    @Override
                    public void itemViewHolder(BaseViewHolder helper, JsonObject item, Context context) {
                        TextView tvw_option = helper.getView(R.id.tvw_option);
                        tvw_option.setText(fwGsonUtil.getString(item, "title"));
                        if (helper.getAdapterPosition() == gTabPostion) {
                            tvw_option.setBackgroundResource(R.drawable.style_task_tag_corner_gray_bgcolor);
                            tvw_option.setTextColor(Color.parseColor("#EF5DB0"));
                        } else {
                            tvw_option.setBackgroundResource(R.drawable.style_button_corner_gray_bgcolor);
                            tvw_option.setTextColor(Color.parseColor("#666666"));
                        }
                    }
                });
        gTabAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                scrollToPosition(position);
                vpr_pages.setCurrentItem(position, true);
            }
        });
        rvw_tags.setLayoutManager(new RvLinearLayoutManager(fwActivity, RvLinearLayoutManager.HORIZONTAL, false));
        rvw_tags.setAdapter(gTabAdapter);
        //ViewPager
        initTabsView(jarrTags);
    }

    @Override
    protected void initData() {

    }

    @Override
    protected void initEvent() {

    }

    @Override
    protected void initNavigation() {

    }

    @Override
    protected void messageHandler(Message msg) {

    }

    @Override
    protected void messageHandler(FwResultJsonDataModel model) {

    }

    @Override
    protected void messageHandler(FwResultJsonListDataModel model) {

    }

    @Override
    public void onClick(View v) {

    }

    //=================================================================================

    public void setJarrTags(JsonArray jarrTags) {
        this.jarrTags = jarrTags;
    }


    public void reloadData() {
        if (gTabFragments.size() > 0) {
            refreshTab(true);
        }
    }

    //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    private void refreshTab(boolean isFore) {
        int currentTabIndex = ((isFore) ? vpr_pages.getCurrentItem() : -1);
        for (int i = 0; i < gTabFragments.size(); i++) {
            TaskTabOtherFragment fragment = (TaskTabOtherFragment) gTabFragments.get(i);
            fragment.doRefreshData(i == currentTabIndex);
        }
        FwLogUtil.info("任务列表》回调刷新代码已运行");
    }

    private void scrollToPosition(int idx) {
        gTabPostion = idx;
        gTabAdapter.notifyDataSetChanged();
        rvw_tags.scrollToPosition(idx);
    }

    private void initTabsView(JsonArray jarrTags) {

        //清除数据
        gTabNames.clear();
        gTabFragments.clear();

        for (int i = 0; i < jarrTags.size(); i++) {
            JsonObject joItem = fwGsonUtil.getJsonObject(jarrTags, i);
            //组装
            TaskTabOtherFragment pFragment = new TaskTabOtherFragment();
            pFragment.setListType(1);//任务类型：0 其他任务；1 视频任务；
            pFragment.setPlatformType(fwGsonUtil.getInt(joItem, "id"));
            gTabFragments.add(pFragment);
        }
        //ViewPager 未嵌套使用 getFragmentManager()，嵌套使用 getChildFragmentManager()。(否则不加载界面)
        FwFragmentPagerAdapter fwPagerAdapter = new FwFragmentPagerAdapter(getChildFragmentManager(), gTabFragments, gTabNames);
        vpr_pages.setAdapter(fwPagerAdapter);
        vpr_pages.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                scrollToPosition(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

    }

}
