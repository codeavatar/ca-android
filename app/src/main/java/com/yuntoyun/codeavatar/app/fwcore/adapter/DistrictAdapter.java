package com.yuntoyun.codeavatar.app.fwcore.adapter;

import android.content.Context;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.google.gson.JsonObject;
import com.yuntoyun.codeavatar.app.R;
import com.yuntoyun.fwcore.util.FwGsonUtil;

import java.util.List;

public class DistrictAdapter extends BaseQuickAdapter<JsonObject, BaseViewHolder> {

    Context context;
    int itemPosition = -1;

    public DistrictAdapter(int layoutResId, @Nullable List<JsonObject> data, Context context) {
        super(layoutResId, data);
        this.context = context;
    }

    public void setSelect(int itemPosition) {
        this.itemPosition = itemPosition;
        notifyDataSetChanged();
    }

    @Override
    protected void convert(BaseViewHolder helper, JsonObject item) {

        String name = FwGsonUtil.builder().getString(item, "name");
        TextView textView = helper.getView(R.id.tvw_option);
        textView.setText(name);
        if (itemPosition == helper.getAdapterPosition()) {
            textView.setTextColor(context.getResources().getColor(R.color.appTxtMainColor));
        } else {
            textView.setTextColor(context.getResources().getColor(R.color.appTxtBlackColor));
        }
    }
}
