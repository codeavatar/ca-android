package com.yuntoyun.codeavatar.app.fwcore.plug.pay.alipay;

public interface AlipayCallback {
    /**
     * 支付回调
     *
     * @param result 支付结果
     * @param status 支付编码
     */
    void payResult(boolean result, String status);
}
