package com.yuntoyun.codeavatar.app.activity.main;

import android.os.Message;
import android.view.View;

import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import com.flyco.tablayout.SlidingTabLayout;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.yuntoyun.codeavatar.app.R;
import com.yuntoyun.fwcore.adapter.FwFragmentPagerAdapter;
import com.yuntoyun.fwcore.base.FwBaseFragment;
import com.yuntoyun.fwcore.model.FwResultJsonDataModel;
import com.yuntoyun.fwcore.model.FwResultJsonListDataModel;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

public class HomeTabFragment extends FwBaseFragment {

    private final String fTag = HomeTabFragment.class.getSimpleName();

    @BindView(R.id.stl_tabs)
    SlidingTabLayout stl_tabs;
    @BindView(R.id.vpr_pages)
    ViewPager vpr_pages;

    private JsonArray categories;
    private int platformType;

    private List<Fragment> gTabFragments = new ArrayList<>();
    private List<String> gTabNames = new ArrayList<>();

    @Override
    protected boolean isAgainCreateView() {
        return false;
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.fragment_main_home_tab;
    }

    @Override
    protected void initView(View layoutView) {
        this.initTabsView();
    }

    @Override
    protected void initData() {

    }

    @Override
    protected void initEvent() {

    }

    @Override
    protected void initNavigation() {

    }

    @Override
    protected void messageHandler(Message msg) {

    }

    @Override
    protected void messageHandler(FwResultJsonDataModel model) {

    }

    @Override
    protected void messageHandler(FwResultJsonListDataModel model) {

    }

    @Override
    public void onClick(View v) {

    }

    //=================================================================================

    public JsonArray getCategories() {
        return categories;
    }

    public void setCategories(JsonArray categories) {
        this.categories = categories;
    }

    public int getPlatformType() {
        return platformType;
    }

    public void setPlatformType(int platformType) {
        this.platformType = platformType;
    }


    //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    private void initTabsView() {
        if (null == getCategories()) return;

        //清除数据
        gTabNames.clear();
        gTabFragments.clear();

        for (int i = 0; i < this.categories.size(); i++) {
            JsonObject joItem = fwGsonUtil.getJsonObject(this.categories, i);
            //组装
            gTabNames.add(fwGsonUtil.getString(joItem, "tag"));
            HomeTabListFragment pFragment = new HomeTabListFragment();
            pFragment.setPlatformType(getPlatformType());
            pFragment.setPlatformCategoryId(fwGsonUtil.getInt(joItem, "cid"));
            gTabFragments.add(pFragment);
        }
        //ViewPager 未嵌套使用 getFragmentManager()，嵌套使用 getChildFragmentManager()。(否则不加载界面)
        FwFragmentPagerAdapter fwPagerAdapter = new FwFragmentPagerAdapter(getChildFragmentManager(), gTabFragments, gTabNames);
        vpr_pages.setAdapter(fwPagerAdapter);
        stl_tabs.setViewPager(vpr_pages);
    }

}
