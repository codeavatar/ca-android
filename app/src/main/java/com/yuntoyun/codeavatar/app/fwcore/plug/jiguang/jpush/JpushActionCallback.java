package com.yuntoyun.codeavatar.app.fwcore.plug.jiguang.jpush;

import android.content.Context;

import cn.jpush.android.api.JPushMessage;

public interface JpushActionCallback {

    void onTagOperatorResult(Context context, JPushMessage jPushMessage);

    void onCheckTagOperatorResult(Context context, JPushMessage jPushMessage);

    void onAliasOperatorResult(Context context, JPushMessage jPushMessage);

    void onMobileNumberOperatorResult(Context context, JPushMessage jPushMessage);
}
