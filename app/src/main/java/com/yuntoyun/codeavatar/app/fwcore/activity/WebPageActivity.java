package com.yuntoyun.codeavatar.app.fwcore.activity;

import android.os.Message;
import android.view.View;
import android.webkit.WebView;

import com.yuntoyun.codeavatar.app.R;
import com.yuntoyun.fwcore.base.FwBaseWebViewActivity;
import com.yuntoyun.fwcore.model.FwResultJsonDataModel;
import com.yuntoyun.fwcore.model.FwResultJsonListDataModel;
import com.yuntoyun.fwcore.plug.tool.FwNavigationTool;

public class WebPageActivity extends FwBaseWebViewActivity {

    public static final int PARTNER_INFO = 1001;
    public static final int SERVICE_DETAIL = 1002;
    public static final int TAOBAO_AUTH = 1003;

    private String gWebUrl;
    private int gActionId = -1;
    private boolean gMustFinish = false;

    @Override
    protected int initChildLayoutResId() {
        return R.layout.activity_basic_webpage;
    }

    @Override
    protected void initChildView() {

    }

    @Override
    protected void initChildData() {

    }

    @Override
    protected void initChildEvent() {

    }

    @Override
    protected void initJsInterface(WebView wvw_webpage) {
        switch (gActionId) {
            case PARTNER_INFO:
//                wvw_webpage.addJavascriptInterface(new PartnerJsInstance(this), "android");
                break;
            case SERVICE_DETAIL:
//                wvw_webpage.addJavascriptInterface(new ServiceJsInstance(this), "android");
                break;
            case TAOBAO_AUTH:
//                wvw_webpage.addJavascriptInterface(new TaoAuthJsInstance(this), "android");
                break;
        }
    }

    @Override
    protected String getWebUrlOrContent() {
        return this.gWebUrl;
    }

    @Override
    protected boolean isLoadUrl() {
        return true;
    }

    @Override
    protected boolean isMustFinish() {
        return this.gMustFinish;
    }

    @Override
    protected void beforeLayout() {

    }

    @Override
    protected void initNavigation() {
        if (null != fwBundle) {
            this.gMustFinish = fwBundle.getBoolean("ext_mustFinish", false);
            String title = fwBundle.getString("ext_title");
            FwNavigationTool.setHeadBar(this, title);
            this.gWebUrl = fwBundle.getString("ext_url");
            if (fwBundle.containsKey("ext_actionId")) {
                this.gActionId = fwBundle.getInt("ext_actionId", -1);
            }
        }
    }

    @Override
    protected void messageHandler(Message msg) {

    }

    @Override
    protected void messageHandler(FwResultJsonDataModel model) {

    }

    @Override
    protected void messageHandler(FwResultJsonListDataModel model) {

    }

    @Override
    public void onClick(View v) {

    }
}
