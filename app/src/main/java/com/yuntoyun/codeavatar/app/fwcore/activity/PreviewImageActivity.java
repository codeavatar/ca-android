package com.yuntoyun.codeavatar.app.fwcore.activity;

import android.os.Message;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import com.google.gson.JsonArray;
import com.yuntoyun.codeavatar.app.R;
import com.yuntoyun.fwcore.adapter.FwFragmentPagerAdapter;
import com.yuntoyun.fwcore.base.FwBaseActivity;
import com.yuntoyun.fwcore.model.FwResultJsonDataModel;
import com.yuntoyun.fwcore.model.FwResultJsonListDataModel;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

/**
 * 预览图片
 */
public class PreviewImageActivity extends FwBaseActivity {

    private final String fTag = PreviewImageActivity.class.getSimpleName();

    @BindView(R.id.tvw_current)
    TextView tvw_current;
    @BindView(R.id.tvw_total)
    TextView tvw_total;
    @BindView(R.id.vpr_preview_imgs)
    ViewPager vpr_preview_imgs;

    @Override
    protected void beforeLayout() {

    }

    @Override
    protected int getLayoutResId() {
        return R.layout.fw_activity_preview_image;
    }

    @Override
    protected void initView() {

    }

    @Override
    protected void initData() {
        if (null != fwBundle) {
            String jsonDataStr = fwBundle.getString("ext_imagesJarrStr");
            String currentImage = fwBundle.getString("ext_currentImage");
            this.setLoadData(jsonDataStr, currentImage);
        }
    }

    @Override
    protected void initEvent() {

    }

    @Override
    protected void initNavigation() {

    }

    @Override
    protected void messageHandler(Message msg) {

    }

    @Override
    protected void messageHandler(FwResultJsonDataModel model) {

    }

    @Override
    protected void messageHandler(FwResultJsonListDataModel model) {

    }

    @Override
    public void onClick(View v) {

    }

    //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    private void setLoadData(String jarrImagsStr, String currentImage) {
        JsonArray jarrImags = fwGsonUtil.getAsJsonArray(jarrImagsStr);

        if (null == jarrImags) return;

        //设置总数
        tvw_total.setText(jarrImags.size() + "");

        int pPosition = 0;
        List<Fragment> pFragments = new ArrayList<>();
        for (int i = 0; i < jarrImags.size(); i++) {
            String pImageUrl = fwGsonUtil.getString(jarrImags, i);
            PreviewImageFagment pFragment = new PreviewImageFagment();
            pFragment.setTmpActivity(PreviewImageActivity.this);
            pFragment.setImagePath(pImageUrl);
            pFragment.setClickFinish(true);
            pFragments.add(pFragment);

            if (!TextUtils.isEmpty(currentImage)) {
                if (pImageUrl.equals(currentImage)) {
                    pPosition = i;
                }
            }
        }
        //ViewPager 未嵌套使用 getFragmentManager()，嵌套使用 getChildFragmentManager()。(否则不加载界面)
        FwFragmentPagerAdapter fwPagerAdapter = new FwFragmentPagerAdapter(getSupportFragmentManager(), pFragments, null);
        vpr_preview_imgs.setAdapter(fwPagerAdapter);
        //切换事件
        vpr_preview_imgs.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                tvw_current.setText((position + 1) + "/");
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        vpr_preview_imgs.setCurrentItem(pPosition, true);
    }
}
