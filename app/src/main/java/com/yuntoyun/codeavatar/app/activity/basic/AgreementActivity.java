package com.yuntoyun.codeavatar.app.activity.basic;

import android.os.Message;
import android.view.View;
import android.webkit.WebView;


import com.yuntoyun.codeavatar.app.R;
import com.yuntoyun.fwcore.base.FwBaseWebViewActivity;
import com.yuntoyun.fwcore.model.FwResultJsonDataModel;
import com.yuntoyun.fwcore.model.FwResultJsonListDataModel;
import com.yuntoyun.fwcore.plug.tool.FwNavigationTool;


public class AgreementActivity extends FwBaseWebViewActivity {

    private final String fTag = AgreementActivity.class.getSimpleName();

    private String gWebUrl = "";

    @Override
    protected int initChildLayoutResId() {
        return R.layout.activity_basic_agreement;
    }

    @Override
    protected void initChildView() {

    }

    @Override
    protected void initChildData() {
        if (null != super.fwBundle) {
            this.gWebUrl = super.fwBundle.getString("ext_weburl");
        }
    }

    @Override
    protected void initChildEvent() {

    }

    @Override
    protected void initJsInterface(WebView wvw_webpage) {

    }

    @Override
    protected String getWebUrlOrContent() {
        return this.gWebUrl;
    }

    @Override
    protected boolean isLoadUrl() {
        return true;
    }

    @Override
    protected boolean isMustFinish() {
        return false;
    }

    @Override
    protected void beforeLayout() {

    }

    @Override
    protected void initNavigation() {
//        FwNavigationTool.setHeadBarBgcolor(fwActivity, "服务协议",
//                getString(R.string.appColorNavigationBgWhite), FwNavigationTool.BackImgType.Black);

        FwNavigationTool.setHeadBar(fwActivity, "服务协议");
    }

    @Override
    protected void messageHandler(Message msg) {

    }

    @Override
    protected void messageHandler(FwResultJsonDataModel model) {

    }

    @Override
    protected void messageHandler(FwResultJsonListDataModel model) {

    }

    @Override
    public void onClick(View v) {

    }
}
