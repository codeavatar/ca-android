package com.yuntoyun.codeavatar.app.tool;

import android.app.Activity;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.viewpager.widget.ViewPager;

import com.google.gson.JsonArray;
import com.yuntoyun.codeavatar.app.fwcore.activity.PreviewImageFagment;
import com.yuntoyun.fwcore.adapter.FwFragmentPagerAdapter;
import com.yuntoyun.fwcore.util.FwGsonUtil;
import com.yuntoyun.fwcore.util.FwSizeUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * 图册预览工具类(有指示器)
 */
public class PreviewTool {

    private Activity activity;
    private FragmentManager fragmentManager;
    private LinearLayout lltIndicators;
    private ViewPager vprPreviewImgs;

    private int[] indicator;
    private List<ImageView> indicatorList;
    private List<Fragment> fragments;
    private int currentPosition = 0;

    private int indicatorWidthDp = -1;

    public void initView(Activity activity, FragmentManager fragmentManager, LinearLayout lltIndicators, ViewPager vprPreviewImgs) {
        this.activity = activity;
        this.fragmentManager = fragmentManager;
        this.lltIndicators = lltIndicators;
        this.vprPreviewImgs = vprPreviewImgs;

        //初始化
        indicatorList = new ArrayList<>();
        fragments = new ArrayList<>();

        //指示器尺寸
        setIndicatorWidthDp(8);
    }

    /**
     * @param indicator int[]{默认，选中}
     */
    public void setIndicator(int[] indicator) {
        this.indicator = indicator;
    }

    public void loadViewData(JsonArray jarrGallery) {
        if (null == jarrGallery || jarrGallery.size() < 1) return;

        //清空数据
        indicatorList.clear();
        fragments.clear();

        //数据源
        String pGalleryStr = jarrGallery.toString();
        for (int i = 0; i < jarrGallery.size(); i++) {
            String pImageUrl = FwGsonUtil.builder().getString(jarrGallery, i);
            PreviewImageFagment pFragment = new PreviewImageFagment();
            pFragment.setTmpActivity(this.activity);
            pFragment.setImagePath(pImageUrl);
            pFragment.setImageJarrStr(pGalleryStr);
            pFragment.setClick(true);
            fragments.add(pFragment);
            //指示器
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(indicatorWidthDp, indicatorWidthDp);
            params.setMargins(10, 0, 10, 0);
            ImageView ivw = new ImageView(activity);
            ivw.setLayoutParams(params);
//            ivw.setPadding(10, 0, 10, 0);
            ivw.setImageResource(this.indicator[0]);
            indicatorList.add(ivw);
            lltIndicators.addView(ivw);
        }
        //设置ViewPager
        //ViewPager 未嵌套使用 getFragmentManager()，嵌套使用 getChildFragmentManager()。(否则不加载界面)
        FwFragmentPagerAdapter fwPagerAdapter = new FwFragmentPagerAdapter(fragmentManager, fragments, null);
        vprPreviewImgs.setAdapter(fwPagerAdapter);
        //设置指示器
        vprPreviewImgs.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                currentPosition = vprPreviewImgs.getCurrentItem();
            }

            @Override
            public void onPageSelected(int position) {
                indicatorList.get(currentPosition).setImageResource(indicator[0]);
                indicatorList.get(position).setImageResource(indicator[1]);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        indicatorList.get(currentPosition).setImageResource(indicator[1]);
    }

    /////////
    ///属性
    ////////

    public int getIndicatorWidthDp() {
        return indicatorWidthDp;
    }

    public void setIndicatorWidthDp(int indicatorWidthDp) {
        this.indicatorWidthDp = FwSizeUtil.builder().dpToPx(this.activity, indicatorWidthDp);;
    }
}
