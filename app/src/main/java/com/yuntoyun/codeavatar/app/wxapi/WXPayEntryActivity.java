package com.yuntoyun.codeavatar.app.wxapi;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import com.yuntoyun.codeavatar.app.R;
import com.yuntoyun.codeavatar.app.fwcore.plug.pay.wxpay.PlugWxpay;
import com.tencent.mm.opensdk.constants.ConstantsAPI;
import com.tencent.mm.opensdk.modelbase.BaseReq;
import com.tencent.mm.opensdk.modelbase.BaseResp;
import com.tencent.mm.opensdk.modelmsg.ShowMessageFromWX;
import com.tencent.mm.opensdk.modelmsg.WXAppExtendObject;
import com.tencent.mm.opensdk.modelmsg.WXMediaMessage;
import com.tencent.mm.opensdk.modelpay.PayResp;
import com.tencent.mm.opensdk.openapi.IWXAPI;
import com.tencent.mm.opensdk.openapi.IWXAPIEventHandler;
import com.tencent.mm.opensdk.openapi.WXAPIFactory;
import com.yuntoyun.fwcore.config.FwAppConfig;
import com.yuntoyun.fwcore.util.FwLogUtil;

/**
 * 该文件只能有一个
 */
public class WXPayEntryActivity extends Activity implements IWXAPIEventHandler {

    private static final String TAG = "MicroMsg.SDKSample.WXPayEntryActivity";

    private IWXAPI api;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fw_plug_wxpay_result);

        api = WXAPIFactory.createWXAPI(this, FwAppConfig.WECHAT_APPID);
//        api = WXAPIFactory.createWXAPI(this, FwAppConfig.WECHAT_APPID, false);

        try {
            api.handleIntent(getIntent(), this);
        } catch (Exception e) {
            e.printStackTrace();
        }
        FwLogUtil.info(TAG, "onCreate");
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
        api.handleIntent(intent, this);
        FwLogUtil.info(TAG, "onNewIntent");
    }

    // 微信发送请求到第三方应用时，会回调到该方法
    @Override
    public void onReq(BaseReq req) {
        FwLogUtil.info(TAG, "[onReq] type=" + req.getType());

        switch (req.getType()) {
            case ConstantsAPI.COMMAND_GETMESSAGE_FROM_WX:
                //由响应方法处理
                break;
            case ConstantsAPI.COMMAND_SHOWMESSAGE_FROM_WX:
                ShowMessageFromWX.Req showReq = (ShowMessageFromWX.Req) req;
                if (null != showReq) {
                    WXMediaMessage wxMsg = showReq.message;
                    WXAppExtendObject obj = (WXAppExtendObject) wxMsg.mediaObject;

                    //以上两实例处理逻辑省略
                }
                break;
            case ConstantsAPI.COMMAND_LAUNCH_BY_WX:
                //由微信启动应用
                break;
            default:
                break;
        }
    }

    // 第三方应用发送到微信的请求处理后的响应结果，会回调到该方法 (发送结果 )
    @Override
    public void onResp(BaseResp resp) {
        FwLogUtil.info(TAG, "[onResp] type=" + resp.getType()+"》errCode = " + resp.errCode);

        if (resp.getType() == ConstantsAPI.COMMAND_PAY_BY_WX) {
            if (resp instanceof PayResp) {
                PayResp payResp = (PayResp) resp;
                if (BaseResp.ErrCode.ERR_OK == payResp.errCode) {
                    PlugWxpay.wxCallback.paySuccessed(payResp);
                } else {
                    PlugWxpay.wxCallback.payFailed(payResp);
                }
            }
        } else if (resp.getType() == ConstantsAPI.COMMAND_SENDAUTH) {

        }

        StringBuffer sb = new StringBuffer("");
        switch (resp.errCode) {
            case BaseResp.ErrCode.ERR_OK:
                sb.append("操作成功");
                break;
            case BaseResp.ErrCode.ERR_USER_CANCEL:
                sb.append("操作取消");
                break;
            case BaseResp.ErrCode.ERR_AUTH_DENIED:
                sb.append("操作被拒绝");
                break;
            case BaseResp.ErrCode.ERR_UNSUPPORT:
                sb.append("不支持错误");
                break;
            default:
                sb.append("操作返回");
                break;
        }
        Toast.makeText(this, sb.toString(), Toast.LENGTH_SHORT).show();
        finish();
    }

    @Override
    protected void onDestroy() {
        PlugWxpay.wxCallback = null;
        System.gc();

        super.onDestroy();
    }
}