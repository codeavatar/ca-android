package com.yuntoyun.codeavatar.app.activity.ext;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.text.TextUtils;
import android.webkit.JavascriptInterface;

import com.yuntoyun.codeavatar.app.activity.main.MainActivity;
import com.yuntoyun.fwcore.util.FwAlertDialogUtil;
import com.yuntoyun.fwcore.util.FwLogUtil;
import com.yuntoyun.fwcore.util.FwToastUtil;

public class WebJsInstance {

    private final String fTag = WebJsInstance.class.getSimpleName();

    private Activity mActivity;

    public WebJsInstance(Activity activity) {
        this.mActivity = activity;
    }

    @JavascriptInterface
    public void load(String joStr) {
        //加载后事件
    }

    @JavascriptInterface
    public void backToIndex(String joStr) {
        //返回首页

        FwAlertDialogUtil.builder().showCustomConfirmDialog(mActivity, "提示", "确定操作？", "取消",
                "确定", new FwAlertDialogUtil.CustomClickListener() {
                    @Override
                    public void clickPositiveButton(DialogInterface dialog) {
                        //确认事件
                        Intent intent = new Intent(mActivity, MainActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        intent.putExtra("ext_tabIndex", 0);
                        mActivity.startActivity(intent);

                        //因显示为启动Activity，故不可关闭当前Activity.
                        if (null != MainActivity.getInstance()) {
                            MainActivity.getInstance().switchTabs(0);
                        }
//                //清除任务，创建新任务
//                Intent intent = new Intent(mActivity, LT_MainFragmentActivity.class);
//                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
//                intent.putExtra("ext_tabIndex", 0);
//                mActivity.startActivity(intent);
                    }

                    @Override
                    public void clickNegativeButton(DialogInterface dialog) {
                        //取消事件
                    }
                });
    }

    @JavascriptInterface
    public void inviteFriends(String joStr) {

        if (!TextUtils.isEmpty(joStr)) {
            FwLogUtil.info(fTag, "函数参数：" + joStr);

            //显示时，必须在某activity中的子线程中。
            this.mActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {

                }
            });
        } else {
            FwToastUtil.builder().makeText("分享信息错误！");
        }
    }

    //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

}
