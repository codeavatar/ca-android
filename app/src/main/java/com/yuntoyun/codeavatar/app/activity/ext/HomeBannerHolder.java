package com.yuntoyun.codeavatar.app.activity.ext;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;

import com.bigkoo.convenientbanner.holder.Holder;
import com.google.gson.JsonObject;
import com.yuntoyun.codeavatar.app.R;
import com.yuntoyun.fwcore.plug.image.glide.PlugGlide;
import com.yuntoyun.fwcore.util.FwGsonUtil;

public class HomeBannerHolder extends Holder<JsonObject> {

    private Context context;

    public HomeBannerHolder(Context context,View itemView) {
        super(itemView);
        this.context = context;
    }

    public HomeBannerHolder(View itemView) {
        super(itemView);
    }

    @Override
    protected void initView(View itemView) {

    }

    @Override
    public void updateUI(JsonObject data) {
        if (null == data) return;

//        //当前索引
//        int pIndex = getAdapterPosition();

        if (null != itemView) {
            ImageView ivw_banner_img = itemView.findViewById(R.id.ivw_banner_img);

            PlugGlide.init().loadImg(context, FwGsonUtil.builder().getString(data, "image"),
                    PlugGlide.GlidePlaceHolderType.RectangleBanner, ivw_banner_img);
        }
    }
}
