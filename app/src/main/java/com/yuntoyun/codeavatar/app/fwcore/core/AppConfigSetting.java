package com.yuntoyun.codeavatar.app.fwcore.core;


import com.yuntoyun.fwcore.config.FwIConfig;
import com.yuntoyun.fwcore.factory.sign.SignType;
import com.yuntoyun.fwcore.util.FwLogUtil;

public class AppConfigSetting implements FwIConfig {

    private AppConfigCenter afCoreConfig = new AppConfigCenter();
    private AppConfigVal afCoreExtConfig = new AppConfigVal();

    public AppConfigSetting() {
    }

    @Override
    public String getRootApi() {
        if (afCoreConfig.NORMAL_ROOT_API) {
            FwLogUtil.info("************【线上】API数据调用************");
            return afCoreExtConfig.ROOT_API_NORMAL;
        }
        FwLogUtil.info("############【线下】API数据调用############");
        return afCoreExtConfig.ROOT_API_TEST;
    }

    @Override
    public String getDomainSite() {
        if (afCoreConfig.NORMAL_ROOT_API) {
            FwLogUtil.info("************【线上】站点调用************");
            return afCoreExtConfig.DOMAIN_NORMAL;
        }
        FwLogUtil.info("############【线下】站点调用############");
        return afCoreExtConfig.DOMAIN_TEST;
    }

    @Override
    public boolean getLogSwitch() {
        return afCoreConfig.PRINT_LOG_SWITCH;
    }

    @Override
    public boolean getSystemLogSwitch() {
        return afCoreConfig.PRINT_LOG_SWITCH_SYSTEM;
    }

    @Override
    public SignType getKeyType() {
        return afCoreExtConfig.KEY_TYPE;
    }

    @Override
    public String getMd5SignKey() {
        return afCoreExtConfig.MD5_SIGN_KEY;
    }

    @Override
    public String getRsaSignKey() {
        return afCoreExtConfig.RSA_SIGN_KEY;
    }

    @Override
    public String getRsa2SignKey() {
        return afCoreExtConfig.RSA2_SIGN_KEY;
    }

    @Override
    public String getWechatAppId() {
        return afCoreExtConfig.WECHAT_APPID;
    }

    @Override
    public String getWechatAppSecret() {
        return afCoreExtConfig.WECHAT_APPSECRET;
    }

    @Override
    public String getWebSocketHost() {
        if (afCoreConfig.NORMAL_ROOT_API) {
            FwLogUtil.info("************【线上】SOCKET数据调用************");
            return afCoreExtConfig.WEBSOCKET_HOST_NORMAL;
        }
        FwLogUtil.info("############【线下】SOCKET数据调用############");
        return afCoreExtConfig.WEBSOCKET_HOST_TEST;
    }

    @Override
    public int getWebSocketPort() {
        if (afCoreConfig.NORMAL_ROOT_API) {
            FwLogUtil.info("************【线上】SOCKET端口调用************");
            return afCoreExtConfig.WEBSOCKET_PORT_NORMAL;
        }
        FwLogUtil.info("############【线下】SOCKET端口调用############");
        return afCoreExtConfig.WEBSOCKET_PORT_TEST;
    }

    @Override
    public boolean getJpushLaunch() {
        return afCoreExtConfig.JPUSH_LAUNCH;
    }
}
