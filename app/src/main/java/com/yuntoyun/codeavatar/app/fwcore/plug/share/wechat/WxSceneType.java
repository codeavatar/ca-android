package com.yuntoyun.codeavatar.app.fwcore.plug.share.wechat;

public enum WxSceneType {
    TimeLine, Session, Favorite
}
