package com.yuntoyun.codeavatar.app.fwcore.activity;

import android.app.Activity;
import android.os.Bundle;
import android.os.Message;
import android.view.View;
import android.widget.ImageView;

import com.yuntoyun.codeavatar.app.R;
import com.yuntoyun.fwcore.base.FwBaseFragment;
import com.yuntoyun.fwcore.model.FwResultJsonDataModel;
import com.yuntoyun.fwcore.model.FwResultJsonListDataModel;
import com.yuntoyun.fwcore.plug.image.glide.PlugGlide;
import com.yuntoyun.fwcore.util.FwIntentUtil;

import butterknife.BindView;

/**
 * 预览图
 */
public class PreviewImageFagment extends FwBaseFragment {

    private final String fTag = PreviewImageFagment.class.getSimpleName();

    @BindView(R.id.ivw_thumbnail)
    ImageView ivw_thumbnail;

    private Activity tmpActivity;
    private String imagePath;
    private boolean isClick;
    private boolean clickFinish;
    private String imageJarrStr;

    @Override
    protected boolean isAgainCreateView() {
        return false;
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.fw_layout_preview_imageview;
    }

    @Override
    protected void initView(View layoutView) {

    }

    @Override
    protected void initData() {
        //显示图片
        PlugGlide.init().loadImg(getTmpActivity(), getImagePath(), PlugGlide.GlidePlaceHolderType.RectangleGoodsThumbnail, ivw_thumbnail);
    }

    @Override
    protected void initEvent() {
        if (isClick() || isClickFinish()) {
            ivw_thumbnail.setOnClickListener(this);
        }
    }

    @Override
    protected void initNavigation() {

    }

    @Override
    protected void messageHandler(Message msg) {

    }

    @Override
    protected void messageHandler(FwResultJsonDataModel model) {

    }

    @Override
    protected void messageHandler(FwResultJsonListDataModel model) {

    }

    @Override
    public void onClick(View v) {
        if (null == v) return;

        switch (v.getId()) {
            case R.id.ivw_thumbnail:
                //关闭
                if (isClickFinish()) {
                    getTmpActivity().finish();
                } else {
                    //跳转
                    Bundle extras = new Bundle();
                    extras.putString("ext_imagesJarrStr", getImageJarrStr());
                    extras.putString("ext_currentImage", getImagePath());
                    FwIntentUtil.builder().startActivity(getTmpActivity(), PreviewImageActivity.class, extras);
                }
                break;
        }
    }

    //=========================================================================


    public Activity getTmpActivity() {
        return tmpActivity;
    }

    public void setTmpActivity(Activity tmpActivity) {
        this.tmpActivity = tmpActivity;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public boolean isClick() {
        return isClick;
    }

    public void setClick(boolean click) {
        isClick = click;
    }

    public boolean isClickFinish() {
        return clickFinish;
    }

    public void setClickFinish(boolean clickFinish) {
        this.clickFinish = clickFinish;
    }

    public String getImageJarrStr() {
        return imageJarrStr;
    }

    public void setImageJarrStr(String imageJarrStr) {
        this.imageJarrStr = imageJarrStr;
    }
}
