package com.yuntoyun.codeavatar.app.fwcore.core;

import com.yuntoyun.fwcore.appext.data.AppData;
import com.yuntoyun.fwcore.config.FwAppConfig;
import com.yuntoyun.fwcore.config.FwConstConfig;
import com.yuntoyun.fwcore.factory.sign.FactorySign;
import com.yuntoyun.fwcore.util.FwStringUtil;

import java.util.HashMap;
import java.util.Map;

public final class AppCoreSign {

    public static void addSign(HashMap<String, Object> map) {
        if (null == map)
            return;

        //基础信息
        map.put(FwConstConfig.API_KEY_PLATFORM, AppConfigVal.POST_PLATFORM);
        map.put(FwConstConfig.API_KEY_DEVICE, AppConfigVal.POST_DEVICE);
        map.put(FwConstConfig.API_KEY_VERSION, AppConfigVal.POST_VERSION);
        //随机数
        map.put(FwConstConfig.API_KEY_RNDNUM, FwStringUtil.builder().getRndNum(FwAppConfig
                .RANDOM_NUM_LENGTH));
        map.put(FwConstConfig.API_KEY_RNDSTR, FwStringUtil.builder().getRndStr(FwAppConfig
                .RANDOM_STR_LENGTH));
        //用户登录状态数据
        Map<String, Object> post = AppData.getUserPost();
        if (null != post) {
            map.putAll(post);
        }

        //克隆Nap，用于签名
        Map<String, String> signMap = new HashMap<>();
        for (String key : map.keySet()) {
            signMap.put(key, map.get(key).toString());
        }
        String signStr = FactorySign.builder().addSign(signMap, FwAppConfig.KEY_TYPE, null);
        //签名
        map.put(FwConstConfig.API_KEY_SIGN, signStr);
    }
}
