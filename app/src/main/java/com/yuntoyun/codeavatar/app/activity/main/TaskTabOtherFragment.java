package com.yuntoyun.codeavatar.app.activity.main;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Message;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;

import com.chad.library.adapter.base.BaseViewHolder;
import com.google.gson.JsonObject;
import com.yuntoyun.codeavatar.app.R;
import com.yuntoyun.codeavatar.app.activity.basic.LoginActivity;
import com.yuntoyun.codeavatar.app.fwcore.core.AppCoreSign;
import com.yuntoyun.fwcore.adapter.FwBaseRecyclerViewAdapter;
import com.yuntoyun.fwcore.base.FwBaseRecyclerViewPagerFragment;
import com.yuntoyun.fwcore.model.FwRecyclerViewAdapterModel;
import com.yuntoyun.fwcore.model.FwRecyclerViewPostModel;
import com.yuntoyun.fwcore.model.FwResultJsonDataModel;
import com.yuntoyun.fwcore.model.FwResultJsonListDataModel;
import com.yuntoyun.fwcore.plug.image.glide.PlugGlide;
import com.yuntoyun.fwcore.plug.network.retrofit.FwOkHttp;
import com.yuntoyun.fwcore.tool.login.LoginTool;
import com.yuntoyun.fwcore.util.FwAlertDialogUtil;
import com.yuntoyun.fwcore.util.FwIntentUtil;

import java.util.HashMap;

import io.reactivex.Observable;

public class TaskTabOtherFragment extends FwBaseRecyclerViewPagerFragment implements FwBaseRecyclerViewAdapter.IFwViewHolder<JsonObject> {

    private final int gActionTask = 901;

    private int platformType;
    private int listType; //任务类型：0 其他任务；1 视频任务；
    private int tempTaskId;

    @Override
    protected void initChildView() {

    }

    @Override
    protected void initChildData() {

    }

    @Override
    protected void initChildEvent() {

    }

    @Override
    protected FwRecyclerViewPostModel getRequestModel() {
        HashMap<String, Object> map = new HashMap<>();
        map.put("fwCurrentPage", super.fwCurrentPage);
        map.put("platform", getPlatformType());
        map.put("taskType", getListType());
        AppCoreSign.addSign(map);
        Observable<FwResultJsonListDataModel> observable = FwOkHttp.getFwAPIService().getTaskList(map);
        return new FwRecyclerViewPostModel(map, observable);
    }

    @Override
    protected FwRecyclerViewAdapterModel getAdapterModel() {
        return new FwRecyclerViewAdapterModel(R.layout.fragment_main_task_tab_video_item, R.layout.fw_layout_list_nodata, this);
    }

    @Override
    protected RecyclerView.LayoutManager getLayoutManager() {
        return super.getCustomLayoutManager(LayoutManangerEnum.LinearLayoutVertical, 1);
    }

    @Override
    protected void setResultData(JsonObject data) {

    }

    @Override
    protected void setResultListItem(JsonObject data) {

    }

    @Override
    protected void onItemClickHandler(JsonObject data, View view, int position) {
        if (null == data) return;

    }

    @Override
    protected void onItemChildClickHandler(JsonObject data, View view, int position) {
        if (null == data) return;

        switch (view.getId()) {
            case R.id.tvw_done:
                if (LoginTool.isLogin(getActivity(), LoginActivity.class)) {
                    doFinishTask(fwGsonUtil.getInt(data, "id"));
                }
                break;
        }
    }

    @Override
    protected void childMessageHandler(FwResultJsonListDataModel model) {

    }

    @Override
    protected View getHeaderView() {
        return null;
    }

    @Override
    protected View getFooterView() {
        return null;
    }

    @Override
    protected boolean isAgainCreateView() {
        return false;
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.fw_layout_recyclerview;
    }

    @Override
    protected void initNavigation() {

    }

    @Override
    protected void messageHandler(Message msg) {

    }

    @Override
    protected void messageHandler(FwResultJsonDataModel model) {
        switch (model.getIndex()) {
            case gActionTask:
                this.finisTaskResult(model.getFwData());
                break;
        }
    }

    @Override
    public void onClick(View v) {

    }

    @Override
    public void itemViewHolder(BaseViewHolder helper, JsonObject item, Context context) {
        ImageView ivw_thumbnail = helper.getView(R.id.ivw_thumbnail);
        PlugGlide.init().loadImg(getActivity(), fwGsonUtil.getString(item, "icon"), R.mipmap.fw_icon_empty_square, ivw_thumbnail);
        helper.setText(R.id.tvw_remark, fwGsonUtil.getString(item, "title"));
        helper.setText(R.id.tvw_price, fwGsonUtil.getString(item, "task_price"));
        helper.setText(R.id.tvw_last_num, "余" + fwGsonUtil.getInt(item, "current_number"));
        //点击事件
        helper.addOnClickListener(R.id.tvw_done);

        //逻辑处理
        LinearLayout llt_tag_other = helper.getView(R.id.llt_tag_other);
        LinearLayout llt_tag_follow = helper.getView(R.id.llt_tag_follow);
        LinearLayout llt_tag_praise = helper.getView(R.id.llt_tag_praise);
        LinearLayout llt_tag_comment = helper.getView(R.id.llt_tag_comment);
        LinearLayout llt_tag_share = helper.getView(R.id.llt_tag_share);
        // 	点赞任务：1 是；0 否(其他同理)
        llt_tag_other.setVisibility((1 == fwGsonUtil.getInt(item, "is_other")) ? View.VISIBLE : View.GONE);
        llt_tag_follow.setVisibility((1 == fwGsonUtil.getInt(item, "is_follow")) ? View.VISIBLE : View.GONE);
        llt_tag_praise.setVisibility((1 == fwGsonUtil.getInt(item, "is_praise")) ? View.VISIBLE : View.GONE);
        llt_tag_comment.setVisibility((1 == fwGsonUtil.getInt(item, "is_comment")) ? View.VISIBLE : View.GONE);
        llt_tag_share.setVisibility((1 == fwGsonUtil.getInt(item, "is_share")) ? View.VISIBLE : View.GONE);
    }

    //========================================================================

    public int getPlatformType() {
        return platformType;
    }

    public void setPlatformType(int platformType) {
        this.platformType = platformType;
    }

    public int getListType() {
        return listType;
    }

    public void setListType(int listType) {
        this.listType = listType;
    }

    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    private void finisTaskResult(JsonObject joData) {
        if (null == joData) return;

        int tmpStatus = fwGsonUtil.getInt(joData, "status");
        if (0 == tmpStatus) {
            Bundle bundle = new Bundle();
            bundle.putInt("param_taskId", tempTaskId);
//            FwIntentUtil.builder().startActivity(fwActivity, TaskDoneVideoDetailActivity.class, bundle);
        } else if (1 == tmpStatus) {
//            View viewDialogLayout = fwLayoutInflater.inflate(R.layout.dialog_task_task_tab_tip_info, null);
//            TextView tvw_message = viewDialogLayout.findViewById(R.id.tvw_message);
//            TextView tvw_ok = viewDialogLayout.findViewById(R.id.tvw_ok);
//            tvw_message.setText(fwGsonUtil.getString(joData, "message"));
//            AlertDialog tmpAlertDialog = FwAlertDialogUtil.builder().getCustomAlertDialog(fwActivity, viewDialogLayout);
//            tmpAlertDialog.show();
//            tvw_ok.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    tmpAlertDialog.dismiss();
//                    FwIntentUtil.builder().startActivity(fwActivity, SettingInfoActivity.class);
//                }
//            });
        } else if (2 == tmpStatus) {
            FwAlertDialogUtil.builder().showCustomDialog(fwActivity, "今日次数已满", fwGsonUtil.getString(joData, "message"), "确定", null);
        } else if (3 == tmpStatus) {
            FwAlertDialogUtil.builder().showCustomConfirmDialog(fwActivity, "是否添加账号", fwGsonUtil.getString(joData, "message"), "取消", "去添加",
                    new FwAlertDialogUtil.CustomClickListener() {
                        @Override
                        public void clickPositiveButton(DialogInterface dialog) {
//                            FwIntentUtil.builder().startActivity(fwActivity, SettingThirdActivity.class);
                        }

                        @Override
                        public void clickNegativeButton(DialogInterface dialog) {

                        }
                    });
        } else if (4 == tmpStatus) {
            FwAlertDialogUtil.builder().showCustomDialog(fwActivity, "任务名额已满", fwGsonUtil.getString(joData, "message"), "确定", null);
        }
    }

    private void doFinishTask(int taskId) {
        tempTaskId = taskId;

        HashMap<String, Object> map = new HashMap<>();
        map.put("taskId", taskId);
        AppCoreSign.addSign(map);
        Observable<FwResultJsonDataModel> observable = FwOkHttp.getFwAPIService().getTaskCheck(map);
        super.doPostData(gActionTask, observable, true);
    }

}
