package com.yuntoyun.codeavatar.app.activity.basic;

import android.os.Message;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.yuntoyun.codeavatar.app.R;
import com.yuntoyun.codeavatar.app.fwcore.core.AppCoreSign;
import com.yuntoyun.fwcore.base.FwBaseActivity;
import com.yuntoyun.fwcore.model.FwResultJsonDataModel;
import com.yuntoyun.fwcore.model.FwResultJsonListDataModel;
import com.yuntoyun.fwcore.plug.network.retrofit.FwOkHttp;
import com.yuntoyun.fwcore.plug.tool.FwNavigationTool;
import com.yuntoyun.fwcore.tool.sms.SmsTool;
import com.yuntoyun.fwcore.tool.sms.SmsToolCallback;
import com.yuntoyun.fwcore.util.FwComponentUtil;
import com.yuntoyun.fwcore.util.FwLogUtil;
import com.yuntoyun.fwcore.util.FwSoftInputUtil;
import com.yuntoyun.fwcore.util.FwToastUtil;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.OnClick;
import io.reactivex.Observable;

public class ForgetPwdActivity extends FwBaseActivity {

    private final String fTag = ForgetPwdActivity.class.getSimpleName();

    private final int gActionSms = 901;
    private final int gActionSubmit = 902;

    @BindView(R.id.ett_account)
    EditText ett_account;
    @BindView(R.id.ett_validcode)
    EditText ett_validcode;
    @BindView(R.id.ett_pwd)
    EditText ett_pwd;
    @BindView(R.id.tvw_getcode)
    TextView tvw_getcode;

    //短信工具类
    private SmsTool gSmsTool = null;

    @Override
    protected void beforeLayout() {

    }

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_basic_forget_pwd;
    }

    @Override
    protected void initView() {
        gSmsTool = new SmsTool(this, smsCallBack);
    }

    @Override
    protected void initData() {

    }

    @Override
    protected void initEvent() {

    }

    @Override
    protected void initNavigation() {
        FwNavigationTool.setHeadBar(this, "忘记密码");
    }

    @Override
    protected void messageHandler(Message msg) {

    }

    @Override
    protected void messageHandler(FwResultJsonDataModel model) {
        switch (model.getIndex()) {
            case gActionSms:
                int pCounter = fwGsonUtil.getInt(model.getFwData(), "expireTime");
                this.gSmsTool.setCounter(pCounter);
                this.gSmsTool.initTimer();
                this.gSmsTool.startTimer();
                break;
            case gActionSubmit:
                FwToastUtil.builder().makeText(model.getFwMessage());
                finish();
                break;
        }
    }

    @Override
    protected void messageHandler(FwResultJsonListDataModel model) {

    }

    @OnClick({R.id.btn_submit, R.id.tvw_getcode})
    @Override
    public void onClick(View v) {
        if (null == v) return;

        switch (v.getId()) {
            case R.id.btn_submit:
                this.doSubmit();
                break;
            case R.id.tvw_getcode:
                this.doSendSms();
                break;
            default:
                FwLogUtil.warn(this.fTag, "该组件ID不存在！");
                break;
        }
    }

    @Override
    protected void onDestroy() {
        this.gSmsTool.stopTimer();

        super.onDestroy();
    }

    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    private void doSubmit() {
        //关闭软键盘
        FwSoftInputUtil.builder().closeKeybord(this);

        String valAccount = FwComponentUtil.builder().getViewValue(ett_account);
        if (valAccount.length() != 11) {
            FwToastUtil.builder().makeText("请输入正确手机号");
            return;
        }

        String valCode = FwComponentUtil.builder().getViewValue(ett_validcode);
        if (valCode.length() != 6) {
            FwToastUtil.builder().makeText("请输入正确验证码");
            return;
        }

        String valPwd = FwComponentUtil.builder().getViewValue(ett_pwd);

        if (valPwd.length() < 1) {
            FwToastUtil.builder().makeText("请输入密码");
            return;
        }

        if (valPwd.length() < 6 || valPwd.length() > 20) {
            FwToastUtil.builder().makeText("请输入6~20位的密码");
            return;
        }

        HashMap<String, Object> map = new HashMap<>();
        map.put("account", valAccount);
        map.put("validcode", valCode);
        map.put("userpwd", valPwd);
        AppCoreSign.addSign(map);
        Observable<FwResultJsonDataModel> observable =
                FwOkHttp.getFwAPIService().setResetLoginPwd(map);
        super.doPostData(this.gActionSubmit, observable, true);
    }

    //发送验证码
    private void doSendSms() {
        String valAccount = FwComponentUtil.builder().getViewValue(ett_account);
        if (valAccount.length() != 11) {
            FwToastUtil.builder().makeText("请输入正确手机号");
            return;
        }

        HashMap<String, Object> map = new HashMap<>();
        map.put("mobilephone", valAccount);
        map.put("action", "forgetpwd");
        AppCoreSign.addSign(map);
        Observable<FwResultJsonDataModel> observable = FwOkHttp.getFwAPIService().getSmsSend(map);
        super.doPostData(this.gActionSms, observable, true);
    }

    //短信工具类回调事件
    private SmsToolCallback smsCallBack = new SmsToolCallback() {
        @Override
        public void initTimer() {
//            tvw_getcode.setEnabled(true);
//            tvw_getcode.setBackgroundResource(R.drawable.style_layout_button_main_bgcolor);
//            tvw_getcode.setText("发送验证码");
        }

        @Override
        public void startTimer(int counter) {
            tvw_getcode.setEnabled(false);
            tvw_getcode.setBackgroundResource(R.drawable.style_button_sms_disabled_bgcolor);
            tvw_getcode.setText(String.format("倒计时%d秒", counter));
        }

        @Override
        public void runTimer(int counter) {
            tvw_getcode.setText(String.format("倒计时%d秒", counter));
        }

        @Override
        public void stopTimer() {
            tvw_getcode.setBackgroundResource(R.drawable.style_button_sms_bgcolor);
            tvw_getcode.setText("发送验证码");
            tvw_getcode.setEnabled(true);
        }
    };
}
