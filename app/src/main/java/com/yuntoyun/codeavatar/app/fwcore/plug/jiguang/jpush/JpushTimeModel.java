package com.yuntoyun.codeavatar.app.fwcore.plug.jiguang.jpush;

import java.util.Set;

public class JpushTimeModel {

    private int startime;
    private int endtime;
    private Set<JpushTimeWeek> weekday;

    public int getStartime() {
        return startime;
    }

    public void setStartime(int startime) {
        this.startime = startime;
    }

    public int getEndtime() {
        return endtime;
    }

    public void setEndtime(int endtime) {
        this.endtime = endtime;
    }

    public Set<JpushTimeWeek> getWeekday() {
        return weekday;
    }

    public void setWeekday(Set<JpushTimeWeek> weekday) {
        this.weekday = weekday;
    }

    public JpushTimeModel() {
    }

    public JpushTimeModel(int startime, int endtime, Set<JpushTimeWeek> weekday) {
        this.startime = startime;
        this.endtime = endtime;
        this.weekday = weekday;
    }
}
