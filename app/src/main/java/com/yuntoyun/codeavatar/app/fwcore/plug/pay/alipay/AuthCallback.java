package com.yuntoyun.codeavatar.app.fwcore.plug.pay.alipay;

public interface AuthCallback {
    /**
     * 授权回调
     *
     * @param result 授权结果
     * @param status 授权编码
     */
    void authResult(boolean result, String status);
}
